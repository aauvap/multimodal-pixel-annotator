#include "pixelannotator.h"
#include "ui_pixelannotator.h"

using namespace cv;
using namespace std;

PixelAnnotator::PixelAnnotator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    currentImage = -1;
    numImages = -1;
    changesSaved = true;
    previousPaintPoint = Point(0,0);
    scrollStart = Point(0,0);

    // Settings
    QCoreApplication::setOrganizationName("AAU");
    QCoreApplication::setOrganizationDomain("vap.aau.dk");
    QCoreApplication::setApplicationName("Pixel Annotator");
    this->setWindowTitle("AAU VAP Pixel Annotator");
    this->setWindowIcon(QIcon("://icons/vaplogoBrush.png"));


    QSettings settings;

    settings.setValue("saveAllModalities", saveAllModalities = settings.value("saveAllModalities", true).toBool());
    settings.setValue("borderThickness", borderThickness = settings.value("borderThickness", 4).toInt());
    settings.setValue("overlayColor", overlayColor = settings.value("overlayColor", QColor(0,0,255)).value<QColor>());
    settings.setValue("overlayOpacity", overlayOpacity = settings.value("overlayOpacity", 0.3).toFloat());
    settings.setValue("noiseBlobSize", noiseBlobSize = settings.value("noiseBlobSize", 20).toInt());
    settings.setValue("holeFillSize", holeFillSize = settings.value("holeFillSize", 20).toInt());
    settings.setValue("autoBorderWhenDrawing", autoBorderWhenDrawing = settings.value("autoBorderWhenDrawing", false).toBool());
    settings.setValue("autoBorderWhenSwitching", autoBorderWhenSwitching = settings.value("autoBorderWhenSwitching", false).toBool());
    settings.setValue("rgbPattern", filePatterns[Image::RGB] = settings.value("rgbPattern", "SyncRGB/*.jpg").toString());
    settings.setValue("depthPattern", filePatterns[Image::Depth] = settings.value("depthPattern", "SyncD/*.png").toString());
    settings.setValue("thermalPattern", filePatterns[Image::Thermal] = settings.value("thermalPattern", "SyncT/*.jpg").toString());
    settings.setValue("csvPath", csvPath = settings.value("csvPath", "annotations.csv").toString());
    settings.setValue("rgbMaskFolder", maskFolders[Image::RGB] = settings.value("rgbMaskFolder", "rgbMasks").toString());
    settings.setValue("depthMaskFolder", maskFolders[Image::Depth] = settings.value("depthMaskFolder", "depthMasks").toString());
    settings.setValue("thermalMaskFolder", maskFolders[Image::Thermal] = settings.value("thermalMaskFolder", "thermalMasks").toString());
    settings.setValue("maskNoDontCareFolder", maskFolders[Image::MaskNoDontCare] = settings.value("maskNoDontCareFolder", "maskNoDontCare").toString());
    settings.setValue("maskOnlyDontCareFolder", maskFolders[Image::MaskOnlyDontCare] = settings.value("maskOnlyDontCareFolder", "maskOnlyDontCare").toString());

    settings.setValue("enableRgb", enabledModalities[Image::RGB] = settings.value("enableRgb", true).toBool());
    settings.setValue("enableDepth", enabledModalities[Image::Depth] = settings.value("enableDepth", true).toBool());
    settings.setValue("enableThermal", enabledModalities[Image::Thermal] = settings.value("enableThermal", true).toBool());
    enabledModalities[Image::MaskNoDontCare] = true;
    enabledModalities[Image::MaskOnlyDontCare] = true;

    // Configure overlay color overlay
    ui->overlayColorLabel->setStyleSheet(QString("QLabel {"
        "background-color: rgb(%1, %2, %3);"
        "border-style: inset;"
        "border-width: 0px;"
        "}"
        ).arg(overlayColor.red()).arg(overlayColor.green()).arg(overlayColor.blue()));

    ui->overlayColorSlider->blockSignals(true);
    ui->overlayColorSlider->setSliderPosition(overlayColor.hue());
    ui->overlayColorSlider->blockSignals(false);
   

    // Load meta data settings
    QFile file("metaDataList.txt");
    QStringList lines;
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        while (!file.atEnd()) {
            QString line = QString(file.readLine()).trimmed();
            if(line.length() == 0) {
                continue;
            }
            lines << line;
        }
        file.close();
    }
    setMetaDataOptions(lines);
    ui->annotationProperties->setRowCount(3+metaDataNames.size());
    ui->annotationProperties->setColumnWidth(0, 127);
    ui->annotationProperties->setColumnWidth(1, 127);

	QFile tagFile("suggestedTags.txt");
	if (tagFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		while (!tagFile.atEnd()) {
			QString line = QString(tagFile.readLine()).trimmed();

			if (line.length() == 0) {
				continue;
			}

			suggestedTags << line;
		}
		tagFile.close();
	}

    // Assets
    addCursor = new QCursor(QPixmap(":/cursors/paint-brush--plus.png"), 0, 16);
    subtractCursor = new QCursor(QPixmap(":/cursors/paint-brush--minus.png"), 0, 16);
    addGrabCutCursor = new QCursor(QPixmap(":/cursors/highlighter--plus.png"), 0, 16);
    subtractGrabCutCursor = new QCursor(QPixmap(":/cursors/highlighter--minus.png"), 0, 16);
    handCursor = new QCursor(QPixmap(":/cursors/hand.png"), 0, 16);
	addPolygonCursor = new QCursor(QPixmap(":/cursors/pencil--plus.png"), 0, 16);
	subtractPolygonCursor = new QCursor(QPixmap(":/cursors/pencil--minus.png"), 0, 16);
	movePolygonCursor = new QCursor(QPixmap(":/cursors/pencil--arrow.png"), 0, 16);

    initModalityWindows(); // Initialize window and mouse handling
    
    KeyPressEater *keyPressEater = new KeyPressEater();
    ui->annotationList->installEventFilter(keyPressEater);

    // Tool states
    state = State::none;
    tool = Tool::none;
    overlayMaskEnabled = true;

    // Define shortcuts
    QHash<int, QKeySequence> functionShortcuts;
    functionShortcuts[Function::newAnnotation] = QKeySequence::New;
    functionShortcuts[Function::save] = QKeySequence::Save;
    functionShortcuts[Function::undoAll] = QKeySequence::Undo;
    functionShortcuts[Function::deleteAnnotations] = QKeySequence::Delete;
    functionShortcuts[Function::autoBorder] = QKeySequence("Ctrl+r");
    functionShortcuts[Function::removeNoise] = QKeySequence("Ctrl+b");
    functionShortcuts[Function::fillHoles] = QKeySequence("Ctrl+f");
    functionShortcuts[Function::zoomIn] = QKeySequence::ZoomIn;
    functionShortcuts[Function::zoomInExtra] = QKeySequence(Qt::Key_Plus);
    functionShortcuts[Function::zoomOut] = QKeySequence::ZoomOut;
    functionShortcuts[Function::zoomOutExtra] = QKeySequence(Qt::Key_Minus);
    functionShortcuts[Function::zoomFit] = QKeySequence("Ctrl+9");
    functionShortcuts[Function::zoomActual] = QKeySequence("Ctrl+0");
	functionShortcuts[Function::zoomCenter] = QKeySequence(Qt::CTRL + Qt::Key_8);
    functionShortcuts[Function::previousImage] = QKeySequence(Qt::CTRL + Qt::Key_Left);
    functionShortcuts[Function::nextImage] = QKeySequence(Qt::CTRL + Qt::Key_Right);
    functionShortcuts[Function::jumpToImage] = QKeySequence("Ctrl+j");
	functionShortcuts[Function::retainPreviousAnnotation] = QKeySequence(Qt::Key_F3);
	functionShortcuts[Function::retainNextAnnotation] = QKeySequence(Qt::Key_F4);
	functionShortcuts[Function::displayTag] = QKeySequence(Qt::Key_F1);
    functionShortcuts[Function::moveMaskDown] = QKeySequence("S");
    functionShortcuts[Function::moveMaskUp] = QKeySequence("W");
    functionShortcuts[Function::moveMaskLeft] = QKeySequence("A");
    functionShortcuts[Function::moveMaskRight] = QKeySequence("D");

    QHash<int, QKeySequence> toolShortcuts;
    toolShortcuts[Tool::selector] = QKeySequence("Y");
    toolShortcuts[Tool::grabCut] = QKeySequence("Ctrl+g");
    toolShortcuts[Tool::addToGrabCut] = QKeySequence("h");
    toolShortcuts[Tool::subtractFromGrabCut] = QKeySequence("j");
    toolShortcuts[Tool::addToMask] = QKeySequence("b");
    toolShortcuts[Tool::subtractFromMask] = QKeySequence("n");
	toolShortcuts[Tool::addToPolygon] = QKeySequence("u");
	toolShortcuts[Tool::subtractFromPolygon] = QKeySequence("i");
	toolShortcuts[Tool::movePointInPolygon] = QKeySequence("o");

    // Build widgets for the toolbar
    brushSizeSelector = new QComboBox(this);
    brushSizeSelector->addItem("0");
    brushSizeSelector->addItem("1");
    brushSizeSelector->addItem("3");
    brushSizeSelector->addItem("5");
    brushSizeSelector->addItem("7");
	brushSizeSelector->addItem("10");
	brushSizeSelector->addItem("50");


    connect(brushSizeSelector, SIGNAL(currentIndexChanged(QString)), this, SLOT(setBrushSize(QString)));
    brushSizeSelector->setCurrentIndex(1);
    imageSelector = new QSpinBox(this);
    imageSelector->setMinimum(1);
    imageSelector->setMaximum(1);
    imageSelector->setSingleStep(1);
    imageSelector->setValue(0);

    // Build toolbar
    functionActions[Function::newAnnotation] = this->ui->mainToolBar->addAction(QIcon(":/icons/tag-hash.png"), QString("New annotation (%1)").arg(functionShortcuts[Function::newAnnotation].toString()), this, SLOT(newAnnotation()));
    functionActions[Function::newAnnotation]->setShortcut(functionShortcuts[Function::newAnnotation]);
    functionActions[Function::save] = this->ui->mainToolBar->addAction(QIcon(":/icons/disk-black.png"), QString("Save (%1)").arg(functionShortcuts[Function::save].toString()), this, SLOT(saveAnnotations()));
    functionActions[Function::save]->setShortcut(functionShortcuts[Function::save]);
    functionActions[Function::save]->setEnabled(false);
    functionActions[Function::undoAll] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-curve-180-left.png"), QString("Undo all changes to this frame (%1)").arg(functionShortcuts[Function::undoAll].toString()), this, SLOT(undoAll()));
    functionActions[Function::undoAll]->setShortcut(functionShortcuts[Function::undoAll]);
    functionActions[Function::undoAll]->setEnabled(false);
    functionActions[Function::deleteAnnotations] = this->ui->mainToolBar->addAction(QIcon(":/icons/cross-script.png"), QString("Delete selected annotation(s) (%1)").arg(functionShortcuts[Function::deleteAnnotations].toString()), this, SLOT(deleteSelectedAnnotations()));
    functionActions[Function::deleteAnnotations]->setShortcut(functionShortcuts[Function::deleteAnnotations]);
    functionActions[Function::deleteAnnotations]->setEnabled(false);
    functionActions[Function::autoBorder] = this->ui->mainToolBar->addAction(QIcon(":/icons/layer-resize.png"), QString("Add border to mask (%1)").arg(functionShortcuts[Function::autoBorder].toString()), this, SLOT(autoBorder()));
    functionActions[Function::autoBorder]->setShortcut(functionShortcuts[Function::autoBorder]);
    functionActions[Function::autoBorder]->setEnabled(false);
    functionActions[Function::removeNoise] = this->ui->mainToolBar->addAction(QIcon(":/icons/broom.png"), QString("Remove noise from mask (%1)").arg(functionShortcuts[Function::removeNoise].toString()), this, SLOT(removeNoise()));
    functionActions[Function::removeNoise]->setShortcut(functionShortcuts[Function::removeNoise]);
    functionActions[Function::removeNoise]->setEnabled(false);
    functionActions[Function::fillHoles] = this->ui->mainToolBar->addAction(QIcon(":/icons/paint-can.png"), QString("Fill holes in mask (%1)").arg(functionShortcuts[Function::fillHoles].toString()), this, SLOT(fillHoles()));
    functionActions[Function::fillHoles]->setShortcut(functionShortcuts[Function::fillHoles]);
    functionActions[Function::fillHoles]->setEnabled(false);
    this->ui->mainToolBar->addSeparator();
    toolActions[Tool::selector] = this->ui->mainToolBar->addAction(QIcon(":/icons/cursor.png"), QString("Select annotations (%1)").arg(toolShortcuts[Tool::selector].toString()), this, SLOT(enableSelector()));
    toolActions[Tool::selector]->setCheckable(true);
    toolActions[Tool::selector]->setShortcut(toolShortcuts[Tool::selector]);
    toolActions[Tool::grabCut] = this->ui->mainToolBar->addAction(QIcon(":/icons/selection.png"), QString("GrabCut (%1)").arg(toolShortcuts[Tool::grabCut].toString()), this, SLOT(enableGrabCut()));
    toolActions[Tool::grabCut]->setCheckable(true);
    toolActions[Tool::grabCut]->setShortcut(toolShortcuts[Tool::grabCut]);
    toolActions[Tool::addToGrabCut] = this->ui->mainToolBar->addAction(QIcon(":/icons/highlighter--plus.png"), QString("Add to GrabCut (%1)").arg(toolShortcuts[Tool::addToGrabCut].toString()), this, SLOT(enableAddToGrabCut()));
    toolActions[Tool::addToGrabCut]->setCheckable(true);
    toolActions[Tool::addToGrabCut]->setShortcut(toolShortcuts[Tool::addToGrabCut]);
    toolActions[Tool::subtractFromGrabCut] = this->ui->mainToolBar->addAction(QIcon(":/icons/highlighter--minus.png"), QString("Subtract from GrabCut (%1)").arg(toolShortcuts[Tool::subtractFromGrabCut].toString()), this, SLOT(enableSubtractFromGrabCut()));
    toolActions[Tool::subtractFromGrabCut]->setCheckable(true);
    toolActions[Tool::subtractFromGrabCut]->setShortcut(toolShortcuts[Tool::subtractFromGrabCut]);
    toolActions[Tool::addToMask] = this->ui->mainToolBar->addAction(QIcon(":/icons/paint-brush--plus.png"), QString("Add to mask (%1)").arg(toolShortcuts[Tool::addToMask].toString()), this, SLOT(enableAddToMask()));
    toolActions[Tool::addToMask]->setCheckable(true);
    toolActions[Tool::addToMask]->setShortcut(toolShortcuts[Tool::addToMask]);
    toolActions[Tool::subtractFromMask] = this->ui->mainToolBar->addAction(QIcon(":/icons/paint-brush--minus.png"), QString("Subtract from mask (%1)").arg(toolShortcuts[Tool::subtractFromMask].toString()), this, SLOT(enableSubtractFromMask()));
    toolActions[Tool::subtractFromMask]->setCheckable(true);
    toolActions[Tool::subtractFromMask]->setShortcut(toolShortcuts[Tool::subtractFromMask]);
    this->ui->mainToolBar->addWidget(new QLabel("   Brush size: ", this));
    this->ui->mainToolBar->addWidget(brushSizeSelector);
	toolActions[Tool::addToPolygon] = this->ui->mainToolBar->addAction(QIcon(":/icons/pencil--plus.png"), QString("Add point to polygon (%1)").arg(toolShortcuts[Tool::addToPolygon].toString()), this, SLOT(enableAddToPolygon()));
	toolActions[Tool::addToPolygon]->setCheckable(true);
	toolActions[Tool::addToPolygon]->setShortcut(toolShortcuts[Tool::addToPolygon]);
	toolActions[Tool::subtractFromPolygon] = this->ui->mainToolBar->addAction(QIcon(":/icons/pencil--minus.png"), QString("Subtract point from polygon (%1)").arg(toolShortcuts[Tool::subtractFromPolygon].toString()), this, SLOT(enableSubtractFromPolygon()));
	toolActions[Tool::subtractFromPolygon]->setCheckable(true);
	toolActions[Tool::subtractFromPolygon]->setShortcut(toolShortcuts[Tool::subtractFromPolygon]);
	toolActions[Tool::movePointInPolygon] = this->ui->mainToolBar->addAction(QIcon(":/icons/pencil--arrow.png"), QString("Move point in polygon (%1)").arg(toolShortcuts[Tool::movePointInPolygon].toString()), this, SLOT(enableMovePointInPolygon()));
	toolActions[Tool::movePointInPolygon]->setCheckable(true);
	toolActions[Tool::movePointInPolygon]->setShortcut(toolShortcuts[Tool::movePointInPolygon]);
    this->ui->mainToolBar->addSeparator();
    functionActions[Function::moveMaskUp] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-090.png"), QString("Move mask up (%1)").arg(functionShortcuts[Function::moveMaskUp].toString()), this, SLOT(moveMaskUp()));
    functionActions[Function::moveMaskUp]->setShortcut(functionShortcuts[Function::moveMaskUp]);
    functionActions[Function::moveMaskUp]->setEnabled(false);
    functionActions[Function::moveMaskDown] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-270.png"), QString("Move mask down (%1)").arg(functionShortcuts[Function::moveMaskDown].toString()), this, SLOT(moveMaskDown()));
    functionActions[Function::moveMaskDown]->setShortcut(functionShortcuts[Function::moveMaskDown]);
    functionActions[Function::moveMaskDown]->setEnabled(false);
    functionActions[Function::moveMaskLeft] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-180.png"), QString("Move mask left (%1)").arg(functionShortcuts[Function::moveMaskLeft].toString()), this, SLOT(moveMaskLeft()));
    functionActions[Function::moveMaskLeft]->setShortcut(functionShortcuts[Function::moveMaskLeft]);
    functionActions[Function::moveMaskLeft]->setEnabled(false);
    functionActions[Function::moveMaskRight] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow.png"), QString("Move mask right (%1)").arg(functionShortcuts[Function::moveMaskRight].toString()), this, SLOT(moveMaskRight()));
    functionActions[Function::moveMaskRight]->setShortcut(functionShortcuts[Function::moveMaskRight]);
    functionActions[Function::moveMaskRight]->setEnabled(false);
	this->ui->mainToolBar->addSeparator();
	functionActions[Function::retainPreviousAnnotation] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-return-270-left.png"), QObject::tr("Retain annotation boxes when loading previous (%1)").arg(functionShortcuts[Function::retainPreviousAnnotation].toString()), this, SLOT(on_retainBoxes_clicked()));
	functionActions[Function::retainPreviousAnnotation]->setCheckable(true);
	functionActions[Function::retainPreviousAnnotation]->setShortcut(functionShortcuts[Function::retainPreviousAnnotation]);
	functionActions[Function::retainNextAnnotation] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-return-270.png"), QObject::tr("Retain annotation boxes when loading next (%1)").arg(functionShortcuts[Function::retainNextAnnotation].toString()), this, SLOT(on_retainBoxes_clicked()));
	functionActions[Function::retainNextAnnotation]->setCheckable(true);
	functionActions[Function::retainNextAnnotation]->setShortcut(functionShortcuts[Function::retainNextAnnotation]);
    
    functionActions[Function::zoomIn] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-in.png"), QString("Zoom in (%1)").arg(functionShortcuts[Function::zoomIn].toString()), this, SLOT(zoomIn()));
    QList<QKeySequence> shortcuts;
    shortcuts.append(functionShortcuts[Function::zoomIn]);
    shortcuts.append(functionShortcuts[Function::zoomInExtra]);
    functionActions[Function::zoomIn]->setShortcuts(shortcuts);
    functionActions[Function::zoomOut] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-out.png"), QString("Zoom out (%1)").arg(functionShortcuts[Function::zoomOut].toString()), this, SLOT(zoomOut()));
    shortcuts.clear();
    shortcuts.append(functionShortcuts[Function::zoomOut]);
    shortcuts.append(functionShortcuts[Function::zoomOutExtra]);
    functionActions[Function::zoomOut]->setShortcuts(shortcuts);
    functionActions[Function::zoomFit] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-fit.png"), QString("Zoom fit (%1)").arg(functionShortcuts[Function::zoomFit].toString()), this, SLOT(zoomFit()));
    functionActions[Function::zoomFit]->setShortcut(functionShortcuts[Function::zoomFit]);
    functionActions[Function::zoomActual] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-actual.png"), QString("Zoom 1:1 (%1)").arg(functionShortcuts[Function::zoomActual].toString()), this, SLOT(zoomActual()));
    functionActions[Function::zoomActual]->setShortcut(functionShortcuts[Function::zoomActual]);
	functionActions[Function::zoomCenter] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-actual-equal.png"), QString("Center on the current annotation(s)  (%1)").arg(functionShortcuts[Function::zoomCenter].toString()), this, SLOT(centerCurrentAnnotation()));
	functionActions[Function::zoomCenter]->setShortcut(functionShortcuts[Function::zoomCenter]);	
    this->ui->utilityToolBar->addSeparator();
    functionActions[Function::previousImage] = this->ui->utilityToolBar->addAction(QIcon(":/icons/control-180.png"), QString("Previous image (%1)").arg(functionShortcuts[Function::previousImage].toString()), this, SLOT(previousImage()));
    functionActions[Function::previousImage]->setShortcut(functionShortcuts[Function::previousImage]);
    functionActions[Function::nextImage] = this->ui->utilityToolBar->addAction(QIcon(":/icons/control.png"), QString("Next image (%1)").arg(functionShortcuts[Function::nextImage].toString()), this, SLOT(nextImage()));
    functionActions[Function::nextImage]->setShortcut(functionShortcuts[Function::nextImage]);
    this->ui->utilityToolBar->addWidget(new QLabel("   Jump to image: ", this));
    this->ui->utilityToolBar->addWidget(imageSelector);
    functionActions[Function::jumpToImage] = this->ui->utilityToolBar->addAction(QIcon(":/icons/control-skip.png"), QString("Jump to image (%1)").arg(functionShortcuts[Function::jumpToImage].toString()), this, SLOT(jumpToImage()));
    functionActions[Function::jumpToImage]->setShortcut(functionShortcuts[Function::jumpToImage]);
    this->ui->utilityToolBar->addSeparator();
	functionActions[Function::showDontCareMask] = this->ui->utilityToolBar->addAction(QIcon(":/icons/layer-mask.png"), QObject::tr("Show don't care mask"), this, SLOT(drawAndUpdate()));
	functionActions[Function::showDontCareMask]->setCheckable(settings.value("useMask").toBool());
	if (settings.value("useMask").toBool())
	{
		functionActions[Function::showDontCareMask]->setChecked(true);
	}
	functionActions[Function::displayTag] = this->ui->utilityToolBar->addAction(QIcon(":/icons/tag.png"), QObject::tr("Display tag (%1)").arg(functionShortcuts[Function::displayTag].toString()), this, SLOT(drawAndUpdate()));
	functionActions[Function::displayTag]->setCheckable(true);
	functionActions[Function::displayTag]->setChecked(true);
	functionActions[Function::displayTag]->setShortcut(functionShortcuts[Function::displayTag]);

    toolGroup = new QActionGroup(this);
    for(int i = Tool::selector; i <= Tool::movePointInPolygon; i++) {
        toolGroup->addAction(toolActions[i]);
    }

    connect(imageSelector, SIGNAL(editingFinished()), this, SLOT(jumpToImage()));

    // Insert indicators to the status bar
    fileNumber = new QLabel();
    fileNumber->setText("Image: 0/0");
    ui->statusBar->addWidget(fileNumber);

    // Final initializations
    updateCurrentAnnotationsPane();
    updateControlButtons();

	// Save default window settings
	writeSettings("default");

	// Read window settings
	readSettings("");

}

PixelAnnotator::~PixelAnnotator()
{
	outFile.close();

	// Remember the window settings
	writeSettings("");

	delete ui;
}

void PixelAnnotator::on_actionReset_layout_triggered()
{
	readSettings("default");
}

void PixelAnnotator::writeSettings(QString prefix)
{
	QSettings settings;
	settings.beginGroup("mainWindow");
	settings.setValue(prefix + "geometry", saveGeometry());
	settings.setValue(prefix + "windowState", saveState());
	settings.endGroup();
}


void PixelAnnotator::readSettings(QString prefix)
{
	QSettings settings;
	settings.beginGroup("mainWindow");
	restoreGeometry(settings.value(prefix + "geometry").toByteArray());
	restoreState(settings.value(prefix + "windowState").toByteArray());
	settings.endGroup();
}

void PixelAnnotator::initModalityWindows()
{
    images.clear();
    displayImages.clear();

    Mat empty = Mat::zeros(1, 1, CV_8UC3);
    images << empty << empty << empty << empty;
    for (int i = 0; i < images.size(); i++) {
        displayImages << images[i].clone();
        dontCareMasks << empty;
    }

    // Add the last matrix to the images vector - this belongs to the "maskNoDontCare" image 
    // which has no corresponding displayImage 
    images << empty;

    QStringList windowNames;
    for (int i = Image::RGB; i <= Image::MaskNoDontCare; ++i)
    {
        if (enabledModalities[i])
        {
            windowNames << getModalityName(i);
        }
    }

    viewers.clear();
    
    scrollAreas.clear();

    // Clear the existing subwindows
    for (int i = 0; i < workWindows.size(); ++i)
    {
        ui->mainArea->removeSubWindow(workWindows[i]);
    }
    workWindows.clear();
    
    for (int i = 0; i < windowNames.size(); i++) {
        QMdiSubWindow* subwindow = new QMdiSubWindow(this);
        subwindow->setAttribute(Qt::WA_DeleteOnClose);
        subwindow->setWindowTitle(windowNames[i]);
        subwindow->installEventFilter(this);
        QScrollArea* scrollArea = new QScrollArea();
        scrollArea->setBackgroundRole(QPalette::Dark);
        scrollArea->setAlignment(Qt::AlignCenter);
        scrollArea->setWidgetResizable(true);
        scrollArea->installEventFilter(this);
        subwindow->setWidget(scrollArea);
        OpenCVViewer *viewer = new OpenCVViewer();
        viewer->setImage(images[i]);
        //viewer->setMouseTracking(true);
        viewer->installEventFilter(this);
        scrollArea->setWidget(viewer);
        connect(viewer, SIGNAL(wheelScrolled(QWheelEvent*)), this, SLOT(scrollHandler(QWheelEvent*)));
        this->ui->mainArea->addSubWindow(subwindow);
        viewers.append(viewer);
        workWindows.append(subwindow);
        scrollAreas.append(scrollArea);
        subwindow->show();
        // This automatic connection code is replace with the below connections to provide Qt5 compatibility.
        //connect(viewer, SIGNAL(mouseClicked(QMouseEvent*)), this, qFlagLocation(QString("1%1MouseHandler(QMouseEvent*)\0" __FILE__ ":" QTOSTRING(__LINE__)).arg(windowNames[i]).toUtf8().constData()));
        //connect(viewer, SIGNAL(mouseMoved(QMouseEvent*)), this, qFlagLocation(QString("1%1MouseHandler(QMouseEvent*)\0" __FILE__ ":" QTOSTRING(__LINE__)).arg(windowNames[i]).toUtf8().constData()));
    }
    this->ui->mainArea->tileSubWindows();

    // Link scroll between viewports
    for (int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
        if (enabledModalities[i]) {
            for (int j = 0; j <= Image::MaskNoDontCare; j++) {
                if (j != i && enabledModalities[j]) {
                    connect((QObject*)scrollAreas[getModalityIndex(i)]->horizontalScrollBar(), SIGNAL(valueChanged(int)), (QObject*)scrollAreas[getModalityIndex(j)]->horizontalScrollBar(), SLOT(setValue(int)));
                    connect((QObject*)scrollAreas[getModalityIndex(i)]->verticalScrollBar(), SIGNAL(valueChanged(int)), (QObject*)scrollAreas[getModalityIndex(j)]->verticalScrollBar(), SLOT(setValue(int)));
                }
            }
        }
    }

    // Mouse handling - TODO; find a way to unify the mouse handlers to clean up the switch statement
    for (int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
        if (enabledModalities[i]) {
            switch (i)
            {
            case Image::RGB:
            {
                connect(viewers[getModalityIndex(Image::RGB)], SIGNAL(mouseClicked(QMouseEvent*)), this, SLOT(RGBMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::RGB)], SIGNAL(mouseMoved(QMouseEvent*)), this, SLOT(RGBMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::RGB)], SIGNAL(mouseReleased(QMouseEvent*)), this, SLOT(RGBMouseHandler(QMouseEvent*)));
                break;
            }
            case Image::Depth:
            {
                connect(viewers[getModalityIndex(Image::Depth)], SIGNAL(mouseClicked(QMouseEvent*)), this, SLOT(DepthMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::Depth)], SIGNAL(mouseMoved(QMouseEvent*)), this, SLOT(DepthMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::Depth)], SIGNAL(mouseReleased(QMouseEvent*)), this, SLOT(DepthMouseHandler(QMouseEvent*)));
                break;
            }
            case Image::Thermal:
            {
                connect(viewers[getModalityIndex(Image::Thermal)], SIGNAL(mouseClicked(QMouseEvent*)), this, SLOT(ThermalMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::Thermal)], SIGNAL(mouseMoved(QMouseEvent*)), this, SLOT(ThermalMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::Thermal)], SIGNAL(mouseReleased(QMouseEvent*)), this, SLOT(ThermalMouseHandler(QMouseEvent*)));
                break;
            }
            case Image::MaskNoDontCare:
            {
                connect(viewers[getModalityIndex(Image::MaskNoDontCare)], SIGNAL(mouseClicked(QMouseEvent*)), this, SLOT(MaskMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::MaskNoDontCare)], SIGNAL(mouseMoved(QMouseEvent*)), this, SLOT(MaskMouseHandler(QMouseEvent*)));
                break;
            }
            default:
                break;
            }
        }
    }

    baseDataNames.clear();
    // Define base data names
    for (int i = Image::RGB; i <= Image::Thermal; i++) {
        if (enabledModalities[i] && i != Image::MaskNoDontCare) {
            QString baseNames = getModalityName(i) + " mask file";
            baseDataNames << baseNames;
        }
    }

    for (int i = Image::RGB; i <= Image::Thermal; i++) {
        if (enabledModalities[i] && i != Image::MaskNoDontCare) {
            QString baseNames = getModalityName(i) + " file";
            baseDataNames << baseNames;
        }
    }
    baseDataNames << "Object ID" << "Annotation tag";
}

bool PixelAnnotator::eventFilter(QObject *object, QEvent *event)
{
    if(event->type() == QEvent::Close) {
        event->ignore();
        return true;
    } else if(event->type() == QEvent::FocusIn || event->type() == QEvent::FocusOut) {
        ui->annotationList->setFocus();
    }
    return QObject::eventFilter(object,event);
}

void PixelAnnotator::keyPressEvent(QKeyEvent *e)
{
    if(e->modifiers() == Qt::ControlModifier && e->key() == Qt::Key_A) {
        qDebug() << "CTRL + A pressed. Key: " << e->key();
        ui->annotationList->selectAll();
        ui->annotationList->setFocus();
        return;
    }

    switch(e->key()) {
	case Qt::Key_Home:
		if (polygonMask)
		{
			polygonMask->changeCurrentPoint(-1);
			drawAndUpdate();
		}
		break;
	case Qt::Key_End:
		if (polygonMask)
		{
			polygonMask->changeCurrentPoint(1);
			drawAndUpdate();
		}
		break;
	case Qt::Key_F2:
		if (currentAnnotations.size() == 1) {
			int current = currentAnnotations.values()[0];
			changeTag(current);
		}
		break;
    case Qt::Key_Control:
        switchAddSubtract();
		break;
    }


}

void PixelAnnotator::keyReleaseEvent(QKeyEvent *e)
{
    qDebug() << "Hello from keyReleaseEvent. Key: " << e->key();

    if (QGuiApplication::queryKeyboardModifiers().testFlag(Qt::ShiftModifier)) {
        qDebug() << "Shift is pressed, Key:" << e->key() << "; A=" << Qt::Key_A;

        switch (e->key()) {
        case Qt::Key_W:
        {
            moveMask(0, -10);
            break;
        }
        case Qt::Key_S:
        {
            moveMask(0, 10);
            break;
        }
        case Qt::Key_A:
        {
            moveMask(-10, 0);
            break;
        }
        case Qt::Key_D:
            moveMask(10, 0);
            break;
		case Qt::Key_I:
			if (polygonMask)
			{
				polygonMask->removeCurrentPoint();
				drawAndUpdate();
			}
			break;
        }
        return;
	}
	else if (QGuiApplication::queryKeyboardModifiers().testFlag(Qt::KeyboardModifier::AltModifier)) {
		qDebug() << "Alt is pressed, Key:" << e->key() << "; A=" << Qt::Key_A;

		switch (e->key()) {
		case Qt::Key_W:
		{
			moveMask(0, -1, true);
			break;
		}
		case Qt::Key_S:
		{
			moveMask(0, 1, true);
			break;
		}
		case Qt::Key_A:
		{
			moveMask(-1, 0, true);
			break;
		}
		case Qt::Key_D:
			moveMask(1, 0, true);
			break;
		case Qt::Key_I:
			if (polygonMask)
			{
				polygonMask->removeCurrentPoint();
				drawAndUpdate();
			}
			break;
		}
		e->accept();
		return;
	}

    switch(e->key()) {
    case Qt::Key_0:
        brushSizeSelector->setCurrentIndex(0);
        return;
    case Qt::Key_1:
        brushSizeSelector->setCurrentIndex(1);
        return;
    case Qt::Key_3:
        brushSizeSelector->setCurrentIndex(2);
        return;
    case Qt::Key_5:
        brushSizeSelector->setCurrentIndex(3);
        return;
    case Qt::Key_7:
        brushSizeSelector->setCurrentIndex(4);
        return;
    case Qt::Key_Control:
        switchAddSubtract();
        break;
    }
}

void PixelAnnotator::drawAndUpdate()
{
    QSettings settings;
	bool showDontCareMask = settings.value("useMask").toBool() && functionActions[Function::showDontCareMask]->isChecked();
	QColor dontCareOverlayColor = settings.value("dontCareOverlayColor", QColor(255, 255, 255)).value<QColor>();
    double dontCareOpacity = settings.value("dontCareOverlayOpacity", 0.3).toDouble();

    for(int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
        if (images[i].depth() == CV_16U) {
            // Image is 16-bit. Convert to 8-bit
            images[i].convertTo(displayImages[i], CV_8UC1, 0.0116, -140); // Convert depth values from [12000 34000] to [0 255]
            
            // Create 3 channels
            Mat tmpIn[] = { displayImages[i], displayImages[i], displayImages[i] };
            merge(tmpIn, 3, displayImages[i]);
        }
        else {
            displayImages[i] = images[i].clone();
        }

        // Overlay the global don't care mask if appropriate
        if (showDontCareMask && (displayImages[i].rows == dontCareMasks[i].rows) && (displayImages[i].cols == dontCareMasks[i].cols)) {
            displayImages[i] = cvUtils::colorOverlay(Scalar(dontCareOverlayColor.blue(), 
                dontCareOverlayColor.green(), dontCareOverlayColor.red()), 
                dontCareOpacity, displayImages[i], dontCareMasks[i]);
        }

        if (i != Image::MaskNoDontCare && enabledModalities[i] && (displayImages[i].rows <= 1 || displayImages[i].cols <= 1)) {
            return; // Empty image of a modality. Return before anything goes wrong
        }
    }

    // When just one mask is selected, we use the mask that is defined in images[Image::Mask] because
    // the current edit is only saved when the selection changes. This code becomes irrelevant when
    // several are selected, as images[Image::Mask] will be black.
    Mat maskOnlyDontCare = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());

    //displayImages[Image::Mask] = (images[Image::Mask] == GC_FGD) + (images[Image::Mask] == GC_PR_FGD);

    //    registrator.setImages(displayImages.toVector().toStdVector());

    std::vector<Scalar> colors = cvUtils::distinctColors(currentAnnotations.size(), overlayColor.hue());

    QSetIterator<int> i(currentAnnotations);
    int counter = 0;
    while (i.hasNext()) {
        int current = i.next();
		bool showPolygonOutline = false;

        Mat maskNoDontCare; Mat currentMaskOnlyDontCare;
        if(currentAnnotations.size() == 1) {
            maskNoDontCare = images[Image::MaskNoDontCare]; // If only one mask is selected it is not saved till the selection changes, and we show the working mask to show a drawing in progress.
            currentMaskOnlyDontCare = images[Image::MaskOnlyDontCare];

			if (polygonMask)
			{
				maskNoDontCare = polygonMask->getMask(GC_FGD);
				showPolygonOutline = true;
			}

        } else {
            maskNoDontCare = annotations[current].getMaskNoDontCare(); // However, when multiple masks are selected, they have all been saved and we use the saved masks from annotations[].
            currentMaskOnlyDontCare = annotations[current].getMaskOnlyDontCare();
        }
        if(maskNoDontCare.empty()) {
            continue;
        }

        Mat filteredMaskNoDontCare;
        Mat(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type(), Scalar(GC_FGD)).copyTo(filteredMaskNoDontCare, ((maskNoDontCare == GC_FGD) + (maskNoDontCare == GC_PR_FGD))); // Filter the mask to only contain positive and probable foreground
        Mat(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type(), Scalar(annotations[current].id())).copyTo(displayImages[Image::MaskNoDontCare], filteredMaskNoDontCare); // Draw the mask in the mask-image with a value corresponding to its id.

        if (!currentMaskOnlyDontCare.empty()) {
            maskOnlyDontCare = maskOnlyDontCare + currentMaskOnlyDontCare;
        }

        if(overlayMaskEnabled) {
            if(saveAllModalities && (enabledModalities[Image::Depth] || enabledModalities[Image::Thermal])) {
                // Calculate registered contours:
                Mat dContour = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());
                Mat tContour = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());

                Mat tmpDepthImg = images[Image::Depth];

                if (tmpDepthImg.rows < 10) {
                    tmpDepthImg = Mat();
                }

                registrator.drawRegisteredContours(filteredMaskNoDontCare, dContour, tContour, tmpDepthImg, MAP_FROM_RGB, true);

                // Paint registered overlays:
                if (enabledModalities[Image::Depth]) {
                    displayImages[Image::Depth] = cvUtils::colorOverlay(colors[counter], overlayOpacity, displayImages[Image::Depth], dContour);
                }

                if (enabledModalities[Image::Thermal]) {
                    displayImages[Image::Thermal] = cvUtils::colorOverlay(colors[counter], overlayOpacity, displayImages[Image::Thermal], tContour);
                }
            }
            displayImages[Image::RGB] = cvUtils::colorOverlay(colors[counter], overlayOpacity, displayImages[Image::RGB], filteredMaskNoDontCare);

			if (showPolygonOutline && polygonMask)
			{
				Mat outlineMask = polygonMask->getOutline(Scalar(255), 1);
				displayImages[Image::RGB] = cvUtils::colorOverlay(cvUtils::distinctColors(2, overlayColor.hue())[1], overlayOpacity, displayImages[Image::RGB], outlineMask);

				polygonMask->drawPoints(displayImages[Image::RGB], cvUtils::distinctColors(2, overlayColor.hue())[1], 2, 1);
			}
        }

		// Add text if a tag is set
		if (annotations[current].getTag().length() > 0 && functionActions[Function::displayTag]->isChecked()) {
			Point rgbUpperLeftCorner = annotations[current].getUpperLeftCorner();

			putText(displayImages[Image::RGB], annotations[current].getTag().toStdString(), rgbUpperLeftCorner, FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 255), 1);

			vector<Point2f> depthUpperLeftCorner;

			if (enabledModalities[Image::Depth]) {
				registrator.computeCorrespondingDepthPointFromRgb(std::vector<Point2f>({ Point2f(rgbUpperLeftCorner.x, rgbUpperLeftCorner.y) }), depthUpperLeftCorner);

				if (!depthUpperLeftCorner.empty())
				{
					putText(displayImages[Image::Depth], annotations[current].getTag().toStdString(), Point(depthUpperLeftCorner[0].x, depthUpperLeftCorner[0].y),
						FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255, 255, 255), 1);
				}
			}

			if (enabledModalities[Image::Thermal]) {
				vector<Point2f> thermalUpperLeftCorner;
				registrator.computeCorrespondingThermalPointFromRgb(std::vector<Point2f>({ Point2f(rgbUpperLeftCorner.x, rgbUpperLeftCorner.y) }), 
					thermalUpperLeftCorner, depthUpperLeftCorner);

				if (!thermalUpperLeftCorner.empty())
				{
					putText(displayImages[Image::Thermal], annotations[current].getTag().toStdString(), Point(thermalUpperLeftCorner[0].x, thermalUpperLeftCorner[0].y),
						FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255, 255, 255), 1);
				}
			}
		}

        counter++;
    }

    if(!saveAllModalities) {
        // Used for inspection of external masks (so it will load and display them, but not change them).
        if(!externalMasks[Image::Depth].empty()) {
            //imshow("Test", externalMasks[Image::Depth]);
            displayImages[Image::Depth] = cvUtils::colorOverlay(Scalar(overlayColor.blue(), overlayColor.green(), overlayColor.red()), overlayOpacity, displayImages[Image::Depth], externalMasks[Image::Depth]);
        }
        if(!externalMasks[Image::Thermal].empty()) {
            displayImages[Image::Thermal] = cvUtils::colorOverlay(Scalar(overlayColor.blue(), overlayColor.green(), overlayColor.red()), overlayOpacity, displayImages[Image::Thermal], externalMasks[Image::Thermal]);
        }
    }


    if (showDontCareMask) {
        Mat(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type(), Scalar(100)).copyTo(displayImages[Image::MaskNoDontCare], maskOnlyDontCare);

        if (saveAllModalities && (enabledModalities[Image::Depth] || enabledModalities[Image::Thermal])) {
            // Calculate registered contours:
            Mat dContour = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());
            Mat tContour = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());

            registrator.drawRegisteredContours(maskOnlyDontCare, dContour, tContour, images[Image::Depth], MAP_FROM_RGB, true);

            // Paint registered overlays:
            if (enabledModalities[Image::Depth]) {
                displayImages[Image::Depth] = cvUtils::colorOverlay(Scalar(255,255,255), overlayOpacity + 0.1, displayImages[Image::Depth], dContour);
            }

            if (enabledModalities[Image::Thermal]) {
                displayImages[Image::Thermal] = cvUtils::colorOverlay(Scalar(255, 255, 255), overlayOpacity + 0.1, displayImages[Image::Thermal], tContour);
            }
        }
        displayImages[Image::RGB] = cvUtils::colorOverlay(Scalar(255, 255, 255), overlayOpacity + 0.1, displayImages[Image::RGB], maskOnlyDontCare);
    }

    if(currentBox.box().width > 0 && currentBox.box().height > 0) {
        // If the current box of the GrabCut is selected, draw it in any enabled modality
        rectangle(displayImages[Image::RGB], currentBox.box(), Scalar(0,255,0)); // RGB is enabled by default

        Rect boxRgb = currentBox.box();
        vector<Point2f> rgbPoints;
        rgbPoints.push_back(Point2f(boxRgb.x, boxRgb.y));
        rgbPoints.push_back(Point2f(boxRgb.x + boxRgb.width, boxRgb.y + boxRgb.height));    

        if (enabledModalities[Image::Depth]) {
            vector<Point2f> depthPoints;
            registrator.computeCorrespondingDepthPointFromRgb(rgbPoints, depthPoints);
            if (depthPoints.size() == 2) {
                rectangle(displayImages[Image::Depth], Rect(depthPoints[0], depthPoints[1]), Scalar(255));
            }
        }

        if (enabledModalities[Image::Thermal]) {
            vector<Point2f> tPoints, depthPoints;
            registrator.computeCorrespondingThermalPointFromRgb(rgbPoints, tPoints, depthPoints);

            if (tPoints.size() == 2) {
                rectangle(displayImages[Image::Thermal], Rect(tPoints[0], tPoints[1]), Scalar(0, 255, 0));
            }
        }

    }

    for(int i = 0; i < displayImages.size(); i++) {
        viewers[getModalityIndex(i)]->setImage(displayImages[i]);
    }
}

void PixelAnnotator::updateControlButtons()
{
    if(currentImage < numImages-1) {
        functionActions[Function::nextImage]->setEnabled(true);
    } else {
        functionActions[Function::nextImage]->setEnabled(false);
    }
    if(currentImage > 0) {
        functionActions[Function::previousImage]->setEnabled(true);
    } else {
        functionActions[Function::previousImage]->setEnabled(false);
    }
    if(numImages > -1) {
        functionActions[Function::newAnnotation]->setEnabled(true);
        functionActions[Function::jumpToImage]->setEnabled(true);
        imageSelector->setEnabled(true);
    } else {
        functionActions[Function::newAnnotation]->setEnabled(false);
        functionActions[Function::jumpToImage]->setEnabled(false);
        /*for(int i = Tool::selector; i <= Tool::subtractFromMask; i++) {
            toolActions[i]->setEnabled(false);
        }*/
        imageSelector->setEnabled(false);
    }
    if(changesSaved) {
        functionActions[Function::save]->setEnabled(false);
        functionActions[Function::undoAll]->setEnabled(false);
    } else {
        functionActions[Function::save]->setEnabled(true);
        functionActions[Function::undoAll]->setEnabled(true);
    }

    // Enable/disable tools depending on whether something is selected:
    bool annotationMarked = (currentAnnotations.size() == 1);
    for(int i = Tool::grabCut; i <= Tool::movePointInPolygon; i++) {
        toolActions[i]->setEnabled(annotationMarked);
    }
    if(!annotationMarked) {
        toolActions[Tool::selector]->trigger();
        for(int i = Function::autoBorder; i <= Function::fillHoles; i++) {
            functionActions[i]->setEnabled(false);
        }
    } else {
        for(int i = Function::autoBorder; i <= Function::fillHoles; i++) {
            functionActions[i]->setEnabled(true);
        }
    }

    functionActions[Function::deleteAnnotations]->setEnabled(currentAnnotations.size() > 0);
}

void PixelAnnotator::updateCurrentAnnotationsPane()
{
    // Update annotation list
    ui->annotationList->blockSignals(true);

	if (ui->annotationList->rowCount() != annotations.size())
	{
		ui->annotationList->clearContents();
		ui->annotationList->setRowCount(annotations.size());
	}

    // Add new annotations:
    for(int i = 0; i < annotations.size(); i++) {
		QTableWidgetItem * id = new QTableWidgetItem(QString("%1").arg(annotations[i].id()));
		QTableWidgetItem * tag = new QTableWidgetItem(annotations[i].getTag());

		ui->annotationList->setItem(i, 0, id);
		ui->annotationList->setItem(i, 1, tag);
    }
	
    ui->annotationList->blockSignals(false);
}

void PixelAnnotator::updateCurrentAnnotationsPropertiesPane()
{
	// Update properties table
	ui->annotationProperties->blockSignals(true);
	ui->annotationProperties->clearContents();
	ui->annotationProperties->setRowCount(3 + metaDataNames.size());

	QTableWidgetItem* currentItem = new QTableWidgetItem("Tag:");
	currentItem->setFlags(Qt::ItemIsEnabled);
	ui->annotationProperties->setItem(0, 0, currentItem);

	currentItem = new QTableWidgetItem("ID:");
	currentItem->setFlags(Qt::ItemIsEnabled);
	ui->annotationProperties->setItem(1, 0, currentItem);
	for (int i = 0; i < (int)metaDataNames.size(); i++) {
		currentItem = new QTableWidgetItem(QString("%1:").arg(metaDataNames[i]));
		currentItem->setFlags(Qt::ItemIsEnabled);
		ui->annotationProperties->setItem(i + 2, 0, currentItem);
	}

	currentItem = new QTableWidgetItem("Status:");
	currentItem->setFlags(Qt::ItemIsEnabled);
	ui->annotationProperties->setItem(metaDataNames.size() + 2, 0, currentItem);


	if (currentAnnotations.size() == 1) {
		int current = currentAnnotations.values()[0];

		currentItem = new QTableWidgetItem(annotations[current].getTag());
		currentItem->setFlags(Qt::ItemIsEnabled);
		ui->annotationProperties->setItem(0, 1, currentItem);

		currentItem = new QTableWidgetItem(QString::number(annotations[current].id()));
		ui->annotationProperties->setItem(1, 1, currentItem);

		for (int i = 0; i < annotations[current].getMetaDataSize(); i++) {
			if (annotations[current].getMetaDataType(i) == "bool") {
				if (annotations[current].getMetaData(i) == "True") {
					currentItem = new QTableWidgetItem("True");
				}
				else {
					currentItem = new QTableWidgetItem("False");
				}
				currentItem->setFlags(Qt::ItemIsEnabled);
			}
			else {
				currentItem = new QTableWidgetItem(annotations[current].getMetaData(i));
			}
			ui->annotationProperties->setItem(i + 2, 1, currentItem);
		}

		QString statusText;
		if (annotations[current].isFinished()) {
			statusText = QObject::tr("Last frame reached");
		}
		else {
			statusText = QObject::tr("Active");
		}
		currentItem = new QTableWidgetItem(statusText);
		currentItem->setFlags(Qt::ItemIsEnabled);
		ui->annotationProperties->setItem(metaDataNames.size() + 2, 1, currentItem);

		ui->annotationProperties->setEnabled(true);
	}
	else {
		ui->annotationProperties->setEnabled(false);
	}
	ui->annotationProperties->verticalHeader()->setVisible(false);
	ui->annotationProperties->blockSignals(false);
}

void PixelAnnotator::on_annotationProperties_cellDoubleClicked(int row, int column)
{
    if(column == 0) {
        return; // We're never interested in detecting changes for the first column, since the user cannot change them.
    }
    if(currentAnnotations.size() != 1) {
        return;
    }
    int current = currentAnnotations.values()[0];

	if (row == 0) {
		changeTag(current);
		changesSaved = false;
	}
	else if (row == 1) {
		// Change the annotation ID
		return;
	}
    else if(row >= 1 && row < annotations[current].getMetaDataSize() + 2) {
        if(row > 0) {
            annotations[current].toggleMetaData(row-2);
            changesSaved = false;
        }
    }
	else if (row >= 1 && row == annotations[current].getMetaDataSize() + 2) {
		toggleIsFinishedState();
		changesSaved = false;
	}

	updateCurrentAnnotationsPropertiesPane();
	updateCurrentAnnotationsPane();
}

void PixelAnnotator::centerCurrentAnnotation()
{
	// Set the viewpoint such that the selected annotations are in focus

	if (currentAnnotations.size() >= 1) {
		float minX = 99999;
		float minY = 99999;
		float maxX = 0;
		float maxY = 0;

		for (auto& annotationIndex : currentAnnotations)
		{
			if (annotationIndex < 0 || annotationIndex >= annotations.size())
			{
				continue;
			}

			auto bb = annotations[annotationIndex].getBoundingBox();

			if (bb.area() == 0)
			{
				QString compactPolygon = annotations[annotationIndex].getCompactPolygon();

				if (!compactPolygon.isEmpty())
				{
					bb = PolygonMask::getBoundingBox(compactPolygon);
				}
			}


			if (bb.x < minX)
			{
				minX = bb.x;
			}

			if (bb.y < minY)
			{
				minY = bb.y;
			}

			if ((bb.x + bb.width) > maxX)
			{
				maxX = bb.x + bb.width;
			}

			if ((bb.y + bb.height) > maxY)
			{
				maxY = bb.y + bb.height;
			}
		}

		QRectF minMaxBB(QPointF(minX, minY), QPointF(maxX, maxY));

		/// Map from scene to view coordinates to set the proper centre of the scene
		QRect viewBox = viewers[getModalityIndex(Image::RGB)]->mapFromScene(minMaxBB).boundingRect();


		//// Set scroll to the same spot. The ensureVisible just guarantees minimum visibility and does not put the object in the center, so here 
		//// we fix this by requiring maximum margins around the point of interest
		scrollAreas[getModalityIndex(Image::RGB)]->ensureVisible(viewBox.center().x(), viewBox.center().y(), 
			scrollAreas[getModalityIndex(Image::RGB)]->viewport()->width() / 2, scrollAreas[getModalityIndex(Image::RGB)]->viewport()->height() / 2);		
	}
}

void PixelAnnotator::addPolygonMask()
{
	if (!polygonMask)
	{
		int current = currentAnnotations.values()[0];

		QString compactPolygon = annotations[current].getCompactPolygon();

		if (!compactPolygon.isEmpty())
		{
			polygonMask = std::make_shared<PolygonMask>(images[Image::MaskNoDontCare], compactPolygon, 5);
		}
		else {
			polygonMask = std::make_shared<PolygonMask>(images[Image::MaskNoDontCare], 5);
		}
	}

}

void PixelAnnotator::toggleIsFinishedState()
{
	int current = currentAnnotations.values()[0];

	if (current >= 0 && current < annotations.size()) {
		annotations[current].setIsFinished(!annotations[current].isFinished());
	}
}

void PixelAnnotator::on_annotationProperties_cellChanged(int row, int column)
{
    if(currentAnnotations.size() != 1) {
        return;
    }
    int current = currentAnnotations.values()[0];
    int proposedId = ui->annotationProperties->item(row, column)->text().toInt();
    int oldId = annotations[current].id();


    if(row == 1 && column == 1) {
        bool overwriting = false;
        for(int i = 0; i < annotations.size(); i++) {
            if(annotations[i].id() == proposedId) {
                overwriting = true;
                break;
            }
        }

        if(overwriting) {
            if(QMessageBox::No == QMessageBox::question(this, "Merge with existing annotation?", "You are about to change the ID of an annotation to another ID which exists already. This will cause the two annotations to be merged, and may lead to lost data. Are you sure you want to continue?", QMessageBox::No, QMessageBox::Yes)) {
                return;
            }
        }

        // If the id does not overwrite an existing id, check if the id resides within the allowed range
        if (!overwriting && idInList(proposedId, annotations)) {
            // If idInList returns true, the id is not within the allowed range. Suggest another id to the user
            int newProposedId = 200;
            bool success = getNewId(newProposedId);

            if (success) {
                QMessageBox::information(this, "ID not within range", tr("The ID %1 is not within the allowed range of [11-169], [171-255].\nPlease rename the annotation using an ID within the allowed range. \n\nSuggested ID within the allowed range: %2")
                    .arg(proposedId).arg(newProposedId), QMessageBox::Ok, QMessageBox::Ok);
            }
            else {
                QMessageBox::information(this, "ID not within range", tr("The ID %1 is not within the allowed range of [11-169], [171-255] and there are no available ID's left. \nOnly 244 unique annotation ID's are supported. ") +
                    tr("Please merge existing annotations or expand an existing annotation to embrace the current object.")
                    .arg(proposedId).arg(newProposedId), QMessageBox::Ok, QMessageBox::Ok);
            }

            // Restore original ID as the new ID has been rendered invalid
            ui->annotationProperties->blockSignals(true);
            ui->annotationProperties->item(row, column)->setText(QString::number(oldId));
            ui->annotationProperties->blockSignals(false);
            return;
        }


        annotations[current].id(ui->annotationProperties->item(row, column)->text().toInt());
        deleteById(oldId); // Remove the old annotation.

        changesSaved = false;
    } else if(row > 0 && row < annotations[current].getMetaDataSize()+1 && column == 1) {
        annotations[current].setMetaData(row-1, ui->annotationProperties->item(row, column)->text());
        changesSaved = false;
    }

	updateCurrentAnnotationsPropertiesPane();
	updateCurrentAnnotationsPane();
}

void PixelAnnotator::on_annotationList_itemSelectionChanged()
{
    //qDebug() << "Selection changed.";
  //  if(currentAnnotations.size() == 1) {
  //      int current = currentAnnotations.values()[0];
		//if (annotations.size() > current)
		//{
		//	annotations[current].setMask(images[Image::MaskNoDontCare].clone(), autoBorderWhenSwitching, borderThickness, Color::unknown);
		//}
  //  }

    QSet<int> currentSet;

    for(int i = 0; i < ui->annotationList->selectedItems().size(); i++) {
        for(int j = 0; j < annotations.size(); j++) {
            if(annotations[j].id() == ui->annotationList->selectedItems()[i]->text().toInt()) {
                currentSet.insert(j);
                break;
            }
        }
    }

    setCurrentAnnotations(currentSet);    
}

void PixelAnnotator::setCurrentAnnotations(QSet<int> set, bool save)
{
    // Save the soon-to-be previous mask:
    if(currentAnnotations.size() == 1 && save) {
        int current = currentAnnotations.values()[0];

        if (annotations.size() > current) {

			if (polygonMask)
			{
				images[Image::MaskNoDontCare] = polygonMask->getMask(Scalar(GC_FGD));
			}

            annotations[current].setMask(images[Image::MaskNoDontCare].clone(), autoBorderWhenSwitching, borderThickness, Color::unknown);

			if (polygonMask)
			{
				qDebug() << "Compact polygon: " << polygonMask->getCompactPolygon();
				annotations[current].setCompactPolygon(polygonMask->getCompactPolygon());
			}

            // If the settings enableBorders is turned off, update the dont care mask manually
            if (!autoBorderWhenSwitching) {
                annotations[current].setDontCareMask(images[Image::MaskOnlyDontCare].clone());
            }
        }
    }

	polygonMask.reset();

    if(annotations.size() == 0) {
        currentAnnotations.clear();
        images[Image::MaskNoDontCare] = Mat::zeros(images[Image::RGB].size(), CV_8UC1);
        images[Image::MaskOnlyDontCare] = Mat::zeros(images[Image::RGB].size(), CV_8UC1);
    } else {
        currentAnnotations = set;

        if(currentAnnotations.size() == 1) {
            int current = currentAnnotations.values()[0];

            // Show the mask from the newly selected annotation (if it is there):
            if(!annotations[current].getMaskNoDontCare().empty()) {
                images[Image::MaskNoDontCare] = annotations[current].getMaskNoDontCare().clone();
                images[Image::MaskOnlyDontCare] = annotations[current].getMaskOnlyDontCare().clone();
            } else {
                images[Image::MaskNoDontCare] = Mat::zeros(images[Image::RGB].size(), CV_8UC1);
                images[Image::MaskOnlyDontCare] = Mat::zeros(images[Image::RGB].size(), CV_8UC1);
            }

			if (!annotations[current].getCompactPolygon().isEmpty())
			{
				addPolygonMask();
			}
        }
    }

    drawAndUpdate();
    updateControlButtons();
	updateCurrentAnnotationsPropertiesPane();
}

void PixelAnnotator::drawMask(QList<PixelAnnotation> annotations, cv::Mat& combinedMask, cv::Mat& maskNoDontCare, cv::Mat& maskOnlyDontCare)
{
    for(int i = 0; i < annotations.size(); i++) {
        Mat currentMaskNoDontCare = annotations[i].getMaskNoDontCare();
        Mat currentMaskOnlyDontCare = annotations[i].getMaskOnlyDontCare();

        if(currentMaskNoDontCare.empty()) {
            continue;
        }

        Mat(currentMaskNoDontCare.size(), currentMaskNoDontCare.type(), Scalar(annotations[i].id())).copyTo(maskNoDontCare, ((currentMaskNoDontCare == GC_FGD) + (currentMaskNoDontCare == GC_PR_FGD)));

        if (!currentMaskOnlyDontCare.empty()) {
            Mat(currentMaskOnlyDontCare.size(), currentMaskOnlyDontCare.type(), Scalar(annotations[i].id())).copyTo(maskOnlyDontCare, currentMaskOnlyDontCare);
        }
    }

    combinedMask = maskNoDontCare.clone();

    if (!combinedMask.empty() && !maskOnlyDontCare.empty()) {
        Mat(maskOnlyDontCare.size(), maskOnlyDontCare.type(), Scalar(Color::unknown)).copyTo(combinedMask, maskOnlyDontCare);
    }
}

void PixelAnnotator::imageMouseHandler(int image, QMouseEvent* event)
{
    Point pt = viewers[getModalityIndex(image)]->mapPoint(Point(event->x(), event->y()));

    if (image == Image::Thermal) {
        // If we have marked something in the thermal image, we need to map the coordinates to the RGB domain
        vector<Point2f> vecRgbCoord, vecTCoord;
        vecTCoord.push_back(pt);

        registrator.computeCorrespondingRgbPointFromThermal(vecTCoord, vecRgbCoord);
        
        if (!vecRgbCoord.empty()) {
            pt = vecRgbCoord[0];
        }
    }

    if (state == State::busy) {
        // We are in the process of computing a GrabCut. Return
        return;
    }

	bool resetPolygon = false;

    if(event->button() == Qt::LeftButton && (event->buttons() & Qt::LeftButton) == Qt::LeftButton) { // Left click event.
        if(event->modifiers() == Qt::ShiftModifier) {
            scrollStart = pt;
        } else {
            switch(tool) {
            case Tool::none:

                break;
            case Tool::selector:

                break;
            case Tool::grabCut:
                if(state != State::boxInProgress && image != Image::MaskNoDontCare) {
                    currentBox = BoxAnnotation();
                    currentBox.setA(pt);
                    state = State::boxInProgress;
					resetPolygon = true;
                }
                break;
            case Tool::addToGrabCut:
                if(image != Image::MaskNoDontCare) {
                    circle(images[Image::MaskNoDontCare], pt, brushSize, Scalar(GC_FGD), -1);
                    circle(images[Image::MaskOnlyDontCare], pt, brushSize, Scalar(0), -1); // Remove from don't care mask
                    previousPaintPoint = pt;
                    state = State::drawingInProgress;
					resetPolygon = true;
                }
                break;
            case Tool::subtractFromGrabCut:
                if(image != Image::MaskNoDontCare) {
                    circle(images[Image::MaskNoDontCare], pt, brushSize, Scalar(GC_BGD), -1);
                    circle(images[Image::MaskOnlyDontCare], pt, brushSize, Scalar(0), -1);
                    previousPaintPoint = pt;
                    state = State::drawingInProgress;
					resetPolygon = true;
                }
                break;
            case Tool::addToMask:
                circle(images[Image::MaskNoDontCare], pt, brushSize, Scalar(GC_FGD), -1);
                circle(images[Image::MaskOnlyDontCare], pt, brushSize, Scalar(0), -1);
                previousPaintPoint = pt;
                state = State::drawingInProgress;
				resetPolygon = true;
                break;
            case Tool::subtractFromMask:
                circle(images[Image::MaskNoDontCare], pt, brushSize, Scalar(GC_BGD), -1);
                circle(images[Image::MaskOnlyDontCare], pt, brushSize, Scalar(0), -1);
                previousPaintPoint = pt;
                state = State::drawingInProgress;
				resetPolygon = true;
                break;
			case Tool::addToPolygon:
				if (polygonMask)
				{
					polygonMask->addPoint(pt);
				}
				break;
			case Tool::subtractFromPolygon:
				if (polygonMask)
				{
					polygonMask->removePoint(pt);
				}
				break;
			case Tool::movePointInPolygon:
				if (polygonMask)
				{
					polygonMask->movePoint(pt);
				}
				break;
            }
        }
    } else if(event->button() == Qt::NoButton && (event->buttons() & Qt::LeftButton) == true) { // Move event, left button down
        if(event->modifiers() == Qt::ShiftModifier) {
            if(scrollStart != Point(0,0)) {
                scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->setValue(scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->value()+scrollStart.x-pt.x);
                scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->setValue(scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->value()+scrollStart.y-pt.y);
            }
        } else {

            switch(tool) {
            case Tool::none:

                break;
            case Tool::selector:

                break;
            case Tool::grabCut:
                if(state == State::boxInProgress) {
                    currentBox.setB(pt);
                }
                break;
            case Tool::addToGrabCut:
                if(image != Image::MaskNoDontCare) {
                    if(previousPaintPoint != Point(0,0)) {
                        line(images[Image::MaskNoDontCare], previousPaintPoint, pt, Scalar(GC_FGD), brushSize*2);
                        line(images[Image::MaskOnlyDontCare], previousPaintPoint, pt, Scalar(0), brushSize * 2);
                    } else {
                        circle(images[Image::MaskNoDontCare], pt, brushSize, Scalar(GC_FGD), -1);
                        circle(images[Image::MaskOnlyDontCare], pt, brushSize, Scalar(0), -1);

                    }
                    previousPaintPoint = pt;
                }
                break;
            case Tool::subtractFromGrabCut:
                if(image != Image::MaskNoDontCare) {
                    if(previousPaintPoint != Point(0,0)) {
                        line(images[Image::MaskNoDontCare], previousPaintPoint, pt, Scalar(GC_BGD), brushSize*2);
                        line(images[Image::MaskOnlyDontCare], previousPaintPoint, pt, Scalar(0), brushSize * 2);

                    } else {
                        circle(images[Image::MaskNoDontCare], pt, brushSize, Scalar(GC_BGD), -1);
                        circle(images[Image::MaskOnlyDontCare], pt, brushSize, Scalar(0), -1);
                    }
                    previousPaintPoint = pt;
                }
                break;
            case Tool::addToMask:
                if(previousPaintPoint != Point(0,0)) {
                    line(images[Image::MaskNoDontCare], previousPaintPoint, pt, Scalar(GC_FGD), brushSize*2);
                    line(images[Image::MaskOnlyDontCare], previousPaintPoint, pt, Scalar(0), brushSize * 2);

                } else {
                    circle(images[Image::MaskNoDontCare], pt, brushSize, Scalar(GC_FGD), -1);
                    circle(images[Image::MaskOnlyDontCare], pt, brushSize, Scalar(0), -1);
                }
                previousPaintPoint = pt;
                break;
            case Tool::subtractFromMask:
                if(previousPaintPoint != Point(0,0)) {
                    line(images[Image::MaskNoDontCare], previousPaintPoint, pt, Scalar(GC_BGD), brushSize*2);
                    line(images[Image::MaskOnlyDontCare], previousPaintPoint, pt, Scalar(0), brushSize * 2);
                } else {
                    circle(images[Image::MaskNoDontCare], pt, brushSize, Scalar(GC_BGD), -1);
                    circle(images[Image::MaskOnlyDontCare], pt, brushSize, Scalar(0), -1);
                }
                previousPaintPoint = pt;
                break;
			case Tool::addToPolygon:
				if (polygonMask)
				{
					polygonMask->removeCurrentPoint();
					polygonMask->addPoint(pt);
				}
				break;
			case Tool::subtractFromPolygon:
				// Nothing happens. Only capable of removing one point at a time
				break;
			case Tool::movePointInPolygon:
				if (polygonMask)
				{
					polygonMask->movePoint(pt);
				}
				break;
            }
            changesSaved = false;
        }
    } else if(event->button() == Qt::LeftButton && (event->buttons() & Qt::LeftButton) == 0) { // Release left button
        
        Mat tmpGrabCutImg = images[Image::MaskNoDontCare].clone();
        bool grabCutToggled = false;

        if (state == State::boxInProgress || tool == Tool::addToGrabCut || tool == Tool::subtractFromGrabCut) {
            grabCutToggled = true;
        }

        if (grabCutToggled) {
            // If we are drawing the GrabCut in another modality than RGB, we need to map the image RGB domain into that modality

            if (image == Image::Depth || image == Image::Thermal) {
                tmpGrabCutImg = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());

                for (int i = GC_BGD; i <= GC_PR_FGD; ++i) {
                    // Run through all the GrabCut foreground/background classes and register them seperately to other modalities
                    Mat dMask = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());
                    Mat tMask = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());
                    Mat emptyMask;
                    Mat filteredDontCareMask = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());
                    images[Image::MaskNoDontCare].copyTo(filteredDontCareMask, images[Image::MaskNoDontCare] == i);

                    registrator.drawRegisteredContours(filteredDontCareMask, dMask, tMask, emptyMask, MAP_FROM_RGB, true);

                    if (image == Image::Depth) {
                        dMask.copyTo(tmpGrabCutImg, dMask);
                    }
                    else if (image == Image::Thermal) {
                        tMask.copyTo(tmpGrabCutImg, tMask);
                    }
                }
            }
        }

        Mat tmpDontCareImg = tmpGrabCutImg.clone();

		if (polygonMask)
		{
			polygonMask->stopMovePoint();
		}

        if (state == State::boxInProgress) {
            // End box drawing: Run GrabCut.
            state = State::busy;
            currentBox.setB(pt);
            Mat bgdModel;
            Mat fgdModel;
            if (currentBox.box().width > 5 && currentBox.box().height > 5) {
                grabCut(images[image], tmpGrabCutImg, currentBox.box(), bgdModel, fgdModel, 3, GC_INIT_WITH_RECT);
            }
            
            state = State::none;
            currentBox = BoxAnnotation();
        }
        else if (state == State::drawingInProgress) {
            if (tool == Tool::addToGrabCut || tool == Tool::subtractFromGrabCut) {
                // Update GrabCut
                Mat bgdModel;
                Mat fgdModel;
                grabCut(images[image], tmpGrabCutImg, Rect(), bgdModel, fgdModel, 1);
            }
            state = State::none;
            previousPaintPoint = Point(0, 0);
        }

        if (grabCutToggled) {
            // Map the grabCutImg back to the RGB modality
            if (image == Image::RGB || image == Image::MaskNoDontCare) {
                images[Image::MaskNoDontCare] = tmpGrabCutImg;
            }
            else if (image == Image::Depth || image == Image::Thermal) {
                Mat diffImage = tmpGrabCutImg - tmpDontCareImg; // Only transfer the differences in order to mitigate mapping errors
                Mat mappedImage = Mat::zeros(tmpGrabCutImg.size(), tmpGrabCutImg.type());
                tmpGrabCutImg.copyTo(mappedImage, diffImage);

                for (int i = GC_BGD; i <= GC_PR_FGD; ++i) {
                    Mat rgbMask = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());
                    Mat tMask, dMask, emptyMask;

                    Mat filteredDontCareMask = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());
                    mappedImage.copyTo(filteredDontCareMask, mappedImage == i);

                    if (image == Image::Depth) {
                        tMask = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());
                        registrator.drawRegisteredContours(rgbMask, filteredDontCareMask, tMask, emptyMask, MAP_FROM_DEPTH, true);
                        rgbMask.copyTo(images[Image::MaskNoDontCare], rgbMask);
                    }
                    else if (image == Image::Thermal) {
                        dMask = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());
                        registrator.drawRegisteredContours(rgbMask, dMask, filteredDontCareMask, emptyMask, MAP_FROM_THERMAL, true);
                        rgbMask.copyTo(images[Image::MaskNoDontCare], rgbMask);
                    }
                }
            }

        }

		if (!currentAnnotations.empty())
		{
			foreach(const int value, currentAnnotations)
			{
				if (annotations[value].getTag().isEmpty())
				{
					changeTag(value);
				}
			}
		}

        changesSaved = false;
        updateControlButtons();
    }

	if (resetPolygon)
	{
		if (currentAnnotations.size() == 1) {
			int current = currentAnnotations.values()[0];

			if (annotations.size() > current) {
				// We have drawn something with a tool that is not 
				// polygon-based. Thus, the polygon is not a correct 
				// representation of the current mask
				qDebug() << "Resetting polygon for annotation with ID " << annotations[current].id();
				annotations[current].resetCompactPolygon();
			}
		}
	}

	if (polygonMask)
	{
		images[Image::MaskNoDontCare] = polygonMask->getMask(Scalar(GC_FGD));
	}

    if (autoBorderWhenDrawing) {
        autoBorder();
    }
    else {
        drawAndUpdate();
    }
}

void PixelAnnotator::RGBMouseHandler(QMouseEvent* event)
{
    imageMouseHandler(Image::RGB, event);
}

void PixelAnnotator::DepthMouseHandler(QMouseEvent* event)
{
    imageMouseHandler(Image::Depth, event);
}

void PixelAnnotator::ThermalMouseHandler(QMouseEvent* event)
{
    imageMouseHandler(Image::Thermal, event);
}

void PixelAnnotator::MaskMouseHandler(QMouseEvent* event)
{
    imageMouseHandler(Image::MaskNoDontCare, event);
}

void PixelAnnotator::scrollHandler(QWheelEvent* event)
{
    if(event->orientation() == Qt::Vertical && QApplication::keyboardModifiers() == Qt::ControlModifier) {
        if(event->delta() > 0) {
            zoom(1);
        } else {
            zoom(-1);
        }

		// No further processing of event
		event->ignore();
    } else {
        event->ignore();
    }
}

void PixelAnnotator::setBrushSize(QString text) {
    brushSize = text.toInt();
}

bool PixelAnnotator::changeTag(int annotationIndex)
{
	if (annotationIndex < 0 || annotationIndex >= annotations.size()) {
		return false;
	}

	// Select the item to change tag
	ui->annotationList->selectRow(annotationIndex);

	QString currentTag = annotations[annotationIndex].getTag();

	QSettings settings;
	bool limitToSugggestedTags = settings.value("limitTagsToSuggested", 0).toBool();

	if (limitToSugggestedTags) {
		// If we limit the tags to strictly use the suggested list, use a QInputDialog instead
		// of the AutoCompleteDialog

		QString newTag = QInputDialog::getItem(this, QObject::tr("Change tag"), QObject::tr("New tag"), suggestedTags, suggestedTags.indexOf(currentTag), false);

		if (!newTag.isEmpty() && (newTag != currentTag)) {
			QString oldTag = annotations[annotationIndex].getTag();
			annotations[annotationIndex].setTag(newTag);

			// Search for annotations in adjacent frames which contains the same annotation id, tag
			std::map<int, PixelAnnotation> sameIdTagAnnotations;

			// Go forward
			for (auto i = currentImage + 1; i < numImages; ++i) {
				QStringList existingAnnotations = getExistingAnnotations(maskFolders[Image::RGB], maskFilename(i, Image::RGB), annotations[annotationIndex].id());

				if (existingAnnotations.size() == 1) {
					PixelAnnotation ann = PixelAnnotation::csvToAnnotation(existingAnnotations[0], csvMetaDataColumnIndex, metaDataNames, metaDataTypes);

					if (ann.getTag() == oldTag) {
						sameIdTagAnnotations.insert(std::pair<int, PixelAnnotation>(i, ann));
						continue;
					}
				}

				// If we have not found any annotations with the same id, tag in frame i, do not look further
				// as we only need the annotations that are temporally connected
				break;
			}

			// Go backward
			if (currentImage > 0)
			{
				for (auto i = currentImage - 1; i >= 0; --i) {
					QStringList existingAnnotations = getExistingAnnotations(maskFolders[Image::RGB], maskFilename(i, Image::RGB), annotations[annotationIndex].id());

					if (existingAnnotations.size() == 1) {
						PixelAnnotation ann = PixelAnnotation::csvToAnnotation(existingAnnotations[0], csvMetaDataColumnIndex, metaDataNames, metaDataTypes);

						if (ann.getTag() == oldTag) {
							sameIdTagAnnotations.insert(std::pair<int, PixelAnnotation>(i, ann));
							continue;
						}
					}

					// If we have not found any annotations with the same id, tag in frame i, do not look further
					// as we only need the annotations that are temporally connected
					break;
				}
			}

			if (!sameIdTagAnnotations.empty()) {
				// Create a list of adjacent annotations for the user to choose if to change the tag of these annotations
				QString replacedAnnotations;
				int framesShown = 0;
				auto it = sameIdTagAnnotations.begin();

				// Show the first five frames that contains the annotations
				while (it != sameIdTagAnnotations.end()) {
					replacedAnnotations += QObject::tr("Frame %1: Id: %2, tag: %3\n").arg(it->first + 1).arg(it->second.id()).arg(it->second.getTag());

					framesShown++;

					if (framesShown > 4) {
						break;
					}
					it++;
				}

				// And the last annotations if appropropiate
				if (sameIdTagAnnotations.size() > 6) {
					replacedAnnotations += QObject::tr("...\n");

					// Show the last frame that contains the annotations
					auto backIt = sameIdTagAnnotations.rbegin();

					if (backIt != sameIdTagAnnotations.rend()) {
						replacedAnnotations += QObject::tr("Frame %1: Id: %2, tag: %3\n").arg(backIt->first + 1).arg(backIt->second.id()).arg(backIt->second.getTag());
					}
				}

				QString message = QObject::tr("There are annotations in adjacent frames which share the same id and tag as the current annotation. \n\n"
					"Do you want to change the tag of these annotations, too?\n\nDetails:\n%1").arg(replacedAnnotations);

				int ret = QMessageBox::question(this, QObject::tr("Replace tag in adjacent frames"), message, QMessageBox::Yes, QMessageBox::No);

				if (ret == QMessageBox::Yes) {
					for (auto ann : sameIdTagAnnotations) {
						ann.second.setTag(annotations[annotationIndex].getTag());

						insertAnnotationInList(ann.first, ann.second); // Replace annotations with new tags and overwrite annotations with old tag
					}
				}
			}
		}

	}
	else {
		AutoCompleteDialog d(this, suggestedTags, QString("Annotation tag"));
		if (d.exec() == QDialog::Accepted) {
			annotations[annotationIndex].setTag(d.getWord());
		
			if (!suggestedTags.contains(d.getWord())) {
				suggestedTags << d.getWord();
			}

			if (noReplace.indexOf(d.getWord()) == -1 && !currentTag.isEmpty()) {
				int ret = QMessageBox::question(this, "Replace all occurrences?",
					QString("Do you wish to replace all instances of %1 with %2 in this track?").arg(currentTag).arg(d.getWord()),
					QMessageBox::Yes | QMessageBox::No,
					QMessageBox::Yes);
				if (ret == QMessageBox::Yes) {
					replaceInTrack.append(currentTag);
					replaceInTrack.append(d.getWord());
				}
				else {
					noReplace.append(d.getWord());
				}
			}
		}
		else {
			return false;
		}
	}

	updateCurrentAnnotationsPane();
	updateCurrentAnnotationsPropertiesPane();
}

QStringList PixelAnnotator::getExistingAnnotations(QString maskDir, QString imageFileName, int id)
{
	int enabledModalitiesCount = 0;
	
	for (int i = 0; i <= Image::Thermal; ++i) {
		if (enabledModalities[i]) {
			++enabledModalitiesCount;
		}
	}
	
	int regExCount = 1 + ((enabledModalitiesCount - 1) * 2);

	qDebug() << "regExp: " << QString("./%1/%2").arg(maskDir).arg(imageFileName) + "(;[^;]*){" + QString("%1").arg(regExCount) + "};" + QString("%1").arg(id) + "*";

	// Legacy string without tags
	QStringList existingAnnotations = outFileLines.filter(QRegExp(QString("./%1/%2").arg(maskDir).arg(imageFileName) + "(;[^;]*){" + QString("%1").arg(regExCount) + "};" + QString("%1").arg(id)));

	if (existingAnnotations.isEmpty())
	{
		// New line with tags
		existingAnnotations = outFileLines.filter(QRegExp(QString("./%1/%2").arg(maskDir).arg(imageFileName) + "(;[^;]*){" + QString("%1").arg(regExCount) + "};" + QString("%1").arg(id) + ";*"));
	}

	return existingAnnotations;
}

void PixelAnnotator::newAnnotation()
{
	if (polygonMask)
	{
		polygonMask.reset();
	}

    int id = 200;

    bool success = getNewId(id);

    if (success) {
        annotations.append(PixelAnnotation(id, QString(), metaDataNames, metaDataTypes));

        QSet<int> currentSet;
        currentSet.insert(annotations.size() - 1);
        setCurrentAnnotations(currentSet);

        changesSaved = false;

        updateCurrentAnnotationsPane();
        updateControlButtons();
    }
    else {
        QMessageBox::information(this, tr("Out of ID's"), tr("Cannot create a new, unique annotation. \nOnly 156 unique annotation ID's are supported. ") +
            tr("Please merge existing annotations or expand an existing annotation to include the current object."), QMessageBox::Ok, QMessageBox::Ok);
    }
}

void PixelAnnotator::deleteSelectedAnnotations()
{
    if(ui->annotationList->selectedItems().size() < 1) {
        return;
    }

    QList<int> deleteIds;
	QModelIndexList selectedRows = ui->annotationList->selectionModel()->selectedRows();

    for(int i = 0; i < selectedRows.count(); i++) {
		int id = ui->annotationList->item(selectedRows.at(i).row(), 0)->text().toInt();
		deleteIds.append(id);
    }

    deleteAnnotations(deleteIds);
}


void PixelAnnotator::deleteAnnotations(QList<int> deleteIds)
{
    QSet<int> currentSet = currentAnnotations;

    for(int i = 0; i < deleteIds.size(); i++) {
         for(int j = 0; j < annotations.size(); j++) {
             if(annotations[j].id() == deleteIds[i]) {
                deleteById(annotations[j].id());

                annotations.removeAt(j);

                currentSet.remove(j);
                break; // Break out of the inner for loop
            }

        }
    }

    if(annotations.size() == 0) {
        functionActions[Function::deleteAnnotations]->setEnabled(false);
    }

    changesSaved = false;
    currentSet.clear();
    setCurrentAnnotations(currentSet, false);
    updateCurrentAnnotationsPane();
    drawAndUpdate();
}

void PixelAnnotator::deleteById(int id)
{
    int enabledModalitiesCount = 0;

    for (int i = 0; i <= Image::Thermal; ++i) {
        if (enabledModalities[i]) {
            ++enabledModalitiesCount;
        }
    }

    // Design the regular expression depending on the number of modalities enabled
    int regExCount = 1 + ((enabledModalitiesCount - 1) * 2);

    int idx = outFileLines.indexOf(QRegExp(QString("./%1/%2").arg(maskFolders[Image::RGB]).arg(maskFilename(currentImage, Image::RGB)) + "(;[^;]*){" + QString::number(regExCount) + "};" + QString("%1.*").arg(id))); // Remove the deleted annotations from outFileLines
    if(idx >= 0) {
        outFileLines.removeAt(idx);
    }
}

void PixelAnnotator::autoBorder()
{
    if(currentAnnotations.size() != 1) {
        return;
    }
    if(images[Image::MaskNoDontCare].empty()) {
        return;
    }

    // Draw the border of the current annotation
    int current = currentAnnotations.values()[0];
    annotations[current].setMask(images[Image::MaskNoDontCare].clone(), true, borderThickness, Color::unknown);
    
    // Fetch the annotation mask and dont care border
    images[Image::MaskNoDontCare] = annotations[current].getMaskNoDontCare().clone();
    images[Image::MaskOnlyDontCare] = annotations[current].getMaskOnlyDontCare().clone();


    changesSaved = false;
    drawAndUpdate();
}

void PixelAnnotator::removeNoise()
{
    if(currentAnnotations.size() == 0) {
        return;
    }
    if(images[Image::MaskNoDontCare].empty()) {
        return;
    }

    vector<vector<Point> > contours;
    Mat input = (images[Image::MaskNoDontCare].clone() == GC_FGD) + (images[Image::MaskNoDontCare].clone() == GC_PR_FGD);
    if(countNonZero(input) == 0) {
        return;
    }
    findContours(input, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );
    for(unsigned int i = 0; i < contours.size(); i++) {
        if(contourArea(contours[i]) <= noiseBlobSize) {
            drawContours(images[Image::MaskNoDontCare], contours, i, Scalar::all(0), CV_FILLED, 8);
        }
    }

    changesSaved = false;
    drawAndUpdate();
}

void PixelAnnotator::fillHoles()
{
    if(currentAnnotations.size() == 0) {
        return;
    }
    if(images[Image::MaskNoDontCare].empty()) {
        return;
    }

    Mat holes = (images[Image::MaskNoDontCare].clone() == GC_FGD) + (images[Image::MaskNoDontCare].clone() == GC_PR_FGD);
    floodFill(holes, Point(0,0), Scalar(1));
    holes = (holes == 0);

    vector<vector<Point> > contours;
    if(countNonZero(holes) == 0) {
        return;
    }
    findContours(holes, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE );
    for(unsigned int i = 0; i < contours.size(); i++) {
        if(contourArea(contours[i]) <= holeFillSize) {
            drawContours(images[Image::MaskNoDontCare], contours, i, Scalar::all(GC_FGD), CV_FILLED, 8);
        }
    }


    changesSaved = false;
    drawAndUpdate();
}

void PixelAnnotator::enableSelector()
{
    tool = Tool::selector;
    for(int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
        viewers[getModalityIndex(i)]->setCursor(Qt::ArrowCursor);
    }
}

void PixelAnnotator::enableGrabCut()
{
    tool = Tool::grabCut;
    for(int i = Image::RGB; i <= Image::Thermal; i++) {
        viewers[getModalityIndex(i)]->setCursor(Qt::CrossCursor);
    }
    viewers[getModalityIndex(Image::MaskNoDontCare)]->setCursor(Qt::ArrowCursor);

	polygonMask.reset();
	drawAndUpdate();
}


void PixelAnnotator::enableAddToGrabCut()
{
    tool = Tool::addToGrabCut;
    for(int i = Image::RGB; i <= Image::Thermal; i++) {
        viewers[getModalityIndex(i)]->setCursor(*addGrabCutCursor);
    }
    viewers[getModalityIndex(Image::MaskNoDontCare)]->setCursor(Qt::ArrowCursor);

	polygonMask.reset();
	drawAndUpdate();
}

void PixelAnnotator::enableSubtractFromGrabCut()
{
    tool = Tool::subtractFromGrabCut;
    for(int i = Image::RGB; i <= Image::Thermal; i++) {
        viewers[getModalityIndex(i)]->setCursor(*subtractGrabCutCursor);
    }

	polygonMask.reset();
	drawAndUpdate();
}

void PixelAnnotator::enableAddToMask()
{
    tool = Tool::addToMask;
    for(int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
        viewers[getModalityIndex(i)]->setCursor(*addCursor);
    }

	polygonMask.reset();
	drawAndUpdate();
}

void PixelAnnotator::enableSubtractFromMask()
{
    tool = Tool::subtractFromMask;
    for(int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
        viewers[getModalityIndex(i)]->setCursor(*subtractCursor);
    }

	polygonMask.reset();
	drawAndUpdate();
}

void PixelAnnotator::enableAddToPolygon()
{
	addPolygonMask();
	
	tool = Tool::addToPolygon;

	for (int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
		viewers[getModalityIndex(i)]->setCursor(*addPolygonCursor);
	}

	drawAndUpdate();
}

void PixelAnnotator::enableSubtractFromPolygon()
{
	addPolygonMask();

	for (int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
		viewers[getModalityIndex(i)]->setCursor(*subtractPolygonCursor);
	}
	
	tool = Tool::subtractFromPolygon;

	drawAndUpdate();
}

void PixelAnnotator::enableMovePointInPolygon()
{
	addPolygonMask();

	for (int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
		viewers[getModalityIndex(i)]->setCursor(*movePolygonCursor);
	}

	tool = Tool::movePointInPolygon;

	drawAndUpdate();
}


void PixelAnnotator::zoom(int direction, float magnitude)
{
    int dirMultiplier;
    if(direction >= 0) {
        dirMultiplier = 1;
    } else {
        dirMultiplier = -1;
    }

    QSize newSize = viewers[getModalityIndex(Image::RGB)]->size()+viewers[getModalityIndex(Image::RGB)]->size()*magnitude*dirMultiplier;

    if(newSize.width() > 50) {
        float hScroll = 0.5, vScroll = 0.5;
        // Find the current scoll position:
        if(scrollAreas[Image::RGB]->horizontalScrollBar()->maximum() > 0) {
            hScroll = (float)scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->value() / (scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->maximum() - scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->minimum());
            vScroll = (float)scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->value() / (scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->maximum()-scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->minimum());
        }

        // Resize view:
        for(int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
            if (enabledModalities[i]) {
                scrollAreas[getModalityIndex(i)]->setWidgetResizable(false);
                viewers[getModalityIndex(i)]->resize(newSize);
            }
        }

        // Set scroll to the same spot:
        if(scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->maximum() > 0) {
            scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->setValue((scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->maximum()-scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->minimum())*hScroll);
            scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->setValue((scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->maximum()-scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->minimum())*vScroll);
        }
    }
}

void PixelAnnotator::zoomIn()
{
    zoom(1);
}

void PixelAnnotator::zoomOut()
{
    zoom(-1);
}

void PixelAnnotator::zoomFit()
{
    for(int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
        if (enabledModalities[i]) {
            scrollAreas[getModalityIndex(i)]->setWidgetResizable(true);
        }
    }
}

void PixelAnnotator::zoomActual()
{
    for(int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
        if (enabledModalities[i]) {
            scrollAreas[getModalityIndex(i)]->setWidgetResizable(false);
            viewers[getModalityIndex(i)]->resize(QSize(displayImages[i].cols, displayImages[i].rows));
        }
    }
}

void PixelAnnotator::previousImage()
{
    loadImage(currentImage-1);
}

void PixelAnnotator::nextImage()
{
    loadImage(currentImage+1);
}

void PixelAnnotator::jumpToImage()
{
    loadImage(imageSelector->value()-1);
}

void PixelAnnotator::clearWindows()
{

}

void PixelAnnotator::loadImage(int index, bool saveChanges, bool showAnnotations)
{
    if(index < 0 || index >= numImages) {
        return;
    }

	imageSelector->blockSignals(true);
	imageSelector->setValue(index + 1);
	imageSelector->blockSignals(false);

	QSet<int> oldIds;

	for (auto& currentAnn : currentAnnotations)
	{
		if (currentAnn >= 0 && currentAnn < annotations.size()) {
			oldIds.insert(annotations[currentAnn].id());
		}
	}

    setCurrentAnnotations(QSet<int>());

    /*if(currentAnnotations.size() == 1) {
        int current = currentAnnotations.values()[0];
        annotations[current].setMask(images[Image::Mask].clone());
    }

    currentAnnotations.clear();*/

    if(saveChanges) {
        saveAnnotations();
    }



    QList<PixelAnnotation> oldAnnotations = annotations;
    annotations.clear();

    QString maskFile = maskFilename(index, Image::RGB);
    if(maskFile.length() > 0) {
        QDir maskNoDontCareDir = baseDir;
        QDir maskOnlyDontCareDir = baseDir;
        maskNoDontCareDir.cd(maskFolders[Image::RGB]);
        maskOnlyDontCareDir.cd(maskFolders[Image::RGB]);
        maskNoDontCareDir.cd(maskFolders[Image::MaskNoDontCare]);
        maskOnlyDontCareDir.cd(maskFolders[Image::MaskOnlyDontCare]);

        Mat maskNoDontCare = imread(maskNoDontCareDir.absoluteFilePath(maskFile).toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
        Mat maskOnlyDontCare = imread(maskOnlyDontCareDir.absoluteFilePath(maskFile).toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
        
        QStringList existingAnnotations = outFileLines.filter(maskFile);
        for(int i = 0; i < existingAnnotations.size(); i++) {
            annotations.append(PixelAnnotation::csvToAnnotation(existingAnnotations[i], csvMetaDataColumnIndex, metaDataNames, metaDataTypes));

            if (maskNoDontCare.empty()) {
                 continue;
            }

            Mat annotationOnly, dontCareOnly;
            Mat(maskNoDontCare.size(), maskNoDontCare.type(), Scalar(GC_FGD)).copyTo(annotationOnly, maskNoDontCare == annotations.last().id());

            annotations.last().setMask(annotationOnly, false); // Don't draw don't care zone here

            if (!maskOnlyDontCare.empty()) {
                Mat(maskOnlyDontCare.size(), maskOnlyDontCare.type(), Scalar(Color::unknown)).copyTo(dontCareOnly, maskOnlyDontCare == annotations.last().id());
                annotations.last().setDontCareMask(dontCareOnly); // Include it explicitly from the saved mask
            }
        }
    }
    if(!saveAllModalities) { // Used for inspection of external masks (so it will load and display them, but not change them).
        for(int clr = Image::Depth; clr <= Image::Thermal; clr++) {
            maskFile = maskFilename(index, clr);
            if(maskFile.length() > 0) {
                QDir maskDir = baseDir;
                maskDir.cd(maskFolders[clr]);
                externalMasks[clr] = imread(maskDir.absoluteFilePath(maskFile).toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
                
            }
        }
    }

    QString readableFileInfo;

    for(int i = Image::RGB; i <= Image::Thermal; i++) {
        if (enabledModalities[i]) {
            int flags = 1;

            if (i == Image::Depth)
            {
                flags = CV_LOAD_IMAGE_ANYDEPTH;
            }

            images[i] = imread(fileLists[i][index].toStdString(), flags);

            if (i == Image::Thermal) {
                // If the image is thermal, adjust the contrast and brightness
                QSettings settings;
                double alpha = settings.value("thermalContrast", 1).toDouble();
                double beta = settings.value("thermalBrightness", 0).toDouble();

                if (alpha != 1 || beta != 0) {
                    for (auto y = 0; y < images[i].rows; ++y) {
                        for (auto x = 0; x < images[i].cols; ++x) {
                            int newVal = cv::saturate_cast<uchar>(alpha*(images[i].at<cv::Vec3b>(y, x)[0]) + beta);
                            images[i].at<cv::Vec3b>(y, x)[0] = newVal;
                            images[i].at<cv::Vec3b>(y, x)[1] = newVal;
                            images[i].at<cv::Vec3b>(y, x)[2] = newVal;
                        }
                    }
                }
            }

            readableFileInfo.append("\t\t" + getModalityName(i) + ": ");
            QFileInfo fileInfo(fileLists[i][index]);
            readableFileInfo.append(fileInfo.baseName());

        }
    }


    images[Image::MaskNoDontCare] = Mat::zeros(images[Image::RGB].size(), CV_8UC1);
    images[Image::MaskOnlyDontCare] = Mat::zeros(images[Image::RGB].size(), CV_8UC1);


	bool retainAnnotations = false;

	if (index == (currentImage + 1) && functionActions[Function::retainNextAnnotation]->isChecked()) {
		retainAnnotations = true;
	}
	if (index == (currentImage - 1) && functionActions[Function::retainPreviousAnnotation]->isChecked()) {
		retainAnnotations = true;
	}

    // Try to automatically keep the old annotations
    if(retainAnnotations) {
        for(int i = 0; i < oldAnnotations.size(); i++) {
            if(!idInList(oldAnnotations[i].id(), annotations)) {
				// Check if the isFinished flag has been set in the previous annotation with the same id
				// If so, we should not append this annotation

				if (!oldAnnotations[i].isFinished())
				{
					annotations.append(oldAnnotations[i]);
				}
            }
        }
    }

	currentImage = index;
    fileNumber->setText(QString("Image: %1/%2 %3").arg(currentImage+1).arg(numImages).arg(readableFileInfo));
    
    updateCurrentAnnotationsPane();

	// Try to find the id of the old annotation in the current annotations and, if possible
	// set this to the current annotation

	if (showAnnotations) {

		bool idFound = false;
		ui->annotationList->blockSignals(true);

		for (auto i = 0; i < annotations.size(); ++i)
		{
            auto finder = oldIds.find(annotations[i].id());

			if (finder != oldIds.end())
			{
				idFound = true;
				currentAnnotations.insert(i);
				for (auto column = 0; column < ui->annotationList->columnCount(); ++column)
				{
					ui->annotationList->item(i, column)->setSelected(true);
				}
			}
		}

		if (!idFound)
		{
			ui->annotationList->selectAll();

			for (auto i = 0; i < annotations.size(); ++i)
			{
				currentAnnotations.insert(i);
			}
		}

		ui->annotationList->blockSignals(false);
		setCurrentAnnotations(currentAnnotations, false);
    }

    updateControlButtons();
}


void PixelAnnotator::saveCSVFile()
{
    // Clear and rewrite the file
    outFile.resize(0);
    QTextStream outStream(&outFile);
	outStream << baseDataNames.join(";") << ";";
	outStream << metaDataNames.join(',');
    outStream << '\n';
    for(QStringList::Iterator it = outFileLines.begin(); it != outFileLines.end(); ++it) {
        outStream << *it << '\n';
    }
    outStream.flush();
}

void PixelAnnotator::saveAnnotations() {
    if(currentImage >= 0) {
        QHash<int, QString> maskFiles;
        for(int i = Image::RGB; i <= Image::MaskOnlyDontCare; i++) {
            maskFiles[i] = maskFilename(currentImage, i);
        }

        for(int i = 0; i < annotations.size(); i++) {
            if(annotations[i].getMask().empty()) { // Don't save annotations without a mask.
                continue;
            }

			if (annotations[i].getTag().length() < 1) {
				changeTag(i);
				updateCurrentAnnotationsPane();
				updateCurrentAnnotationsPropertiesPane();
			}

			QString imageFileName = maskFilename(currentImage, Image::RGB);
			int id = annotations[i].id();

			QString line = annotationToCsv(currentImage, annotations[i]);
			QStringList existingAnnotations = getExistingAnnotations(maskFolders[Image::RGB], imageFileName, id);

            if(existingAnnotations.size() > 0) {
                outFileLines[outFileLines.indexOf(existingAnnotations[0])] = line;
            } else {
                outFileLines << line;
            }
        }

        // Generate registered masks here:
        QHash<int, Mat> masks;
        drawMask(annotations, masks[Image::RGB], masks[Image::MaskNoDontCare], masks[Image::MaskOnlyDontCare]);

        if(masks[Image::RGB].empty()) {
            masks[Image::RGB] = Mat::zeros(images[Image::RGB].size(), CV_8UC1);
        }
        if (masks[Image::MaskNoDontCare].empty()) {
            masks[Image::MaskNoDontCare] = Mat::zeros(images[Image::RGB].size(), CV_8UC1);
        }
        if (masks[Image::MaskOnlyDontCare].empty()) {
            masks[Image::MaskOnlyDontCare] = Mat::zeros(images[Image::RGB].size(), CV_8UC1);
        }

        if(saveAllModalities && (enabledModalities[Image::Depth] || enabledModalities[Image::Thermal])) {
            // Calculate registered contours. We need to register the noDontCare-mask and then add the don't-care layer
            // seperately for each modality
            QHash<int, Mat> areaOfInterest, maskOnlyDontCare;

            if (enabledModalities[Image::Depth]) {
                masks[Image::Depth] = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());
                areaOfInterest[Image::Depth] = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());
                maskOnlyDontCare[Image::Depth] = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());
            }

            if (enabledModalities[Image::Thermal]) {
                masks[Image::Thermal] = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());
                areaOfInterest[Image::Thermal] = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());
                maskOnlyDontCare[Image::Thermal] = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());
            }

            // Register the noDontCareMask/areaOfInterest seperately
            registrator.drawRegisteredContours(masks[Image::MaskNoDontCare], areaOfInterest[Image::Depth], areaOfInterest[Image::Thermal], areaOfInterest[Image::Depth], MAP_FROM_RGB, true);         
          
            // Get the don't care mask, and combine the two masks
            if (enabledModalities[Image::Depth]) {
                masks[Image::Depth] = areaOfInterest[Image::Depth].clone();
                
                // Check if auto-drawing of don't care zones is turned on. If not, we do not add don't care border in depth or thermal.
                // This might be behaviour, that one wants to change in the future
                if (autoBorderWhenDrawing || autoBorderWhenSwitching) {
                    PixelAnnotation::generateDontCareFromMask(areaOfInterest[Image::Depth], maskOnlyDontCare[Image::Depth], borderThickness, Color::unknown);
                    Mat(maskOnlyDontCare[Image::Depth].size(), maskOnlyDontCare[Image::Depth].type(), Scalar(Color::unknown)).copyTo(masks[Image::Depth], maskOnlyDontCare[Image::Depth]);
                }
            }

            // Get the don't care mask, and combine the two masks
            if (enabledModalities[Image::Thermal]) {
                masks[Image::Thermal] = areaOfInterest[Image::Thermal].clone();

                // Check if auto-drawing of don't care zones is turned on. If not, we do not add don't care border in depth or thermal.
                // This might be behaviour, that one wants to change in the future
                if (autoBorderWhenDrawing || autoBorderWhenSwitching) {
                    PixelAnnotation::generateDontCareFromMask(areaOfInterest[Image::Thermal], maskOnlyDontCare[Image::Thermal], borderThickness, Color::unknown);
                    Mat(maskOnlyDontCare[Image::Thermal].size(), maskOnlyDontCare[Image::Thermal].type(), Scalar(Color::unknown)).copyTo(masks[Image::Thermal], maskOnlyDontCare[Image::Thermal]);
                }
            }
        }

        for (int i = Image::RGB; i <= Image::MaskOnlyDontCare; i++) {
            if (enabledModalities[i]) {
                QDir maskDir = baseDir;

                if (i == Image::MaskNoDontCare || i == Image::MaskOnlyDontCare) {
                    maskDir.cd(maskFolders[Image::RGB]); // The mask folders are located inside the RGB folder.
                }
                maskDir.cd(maskFolders[i]);

                if (!saveAllModalities && i > Image::RGB) {
                    break;
                }
                qDebug() << "Writing to..." << maskDir.absoluteFilePath(maskFiles[i]);
                imwrite(maskDir.absoluteFilePath(maskFiles[i]).toStdString(), masks[i]);
            }
        }

        saveCSVFile();

        changesSaved = true;
    }
}

void PixelAnnotator::undoAll()
{
    loadImage(currentImage, false);
}

void PixelAnnotator::moveMaskUp()
{
    moveMask(0, -1);
}

void PixelAnnotator::moveMaskDown()
{
    moveMask(0, 1);
}

void PixelAnnotator::moveMaskLeft()
{
    moveMask(-1, 0);
}

void PixelAnnotator::moveMaskRight()
{
    moveMask(1, 0);
}

void PixelAnnotator::moveMask(int xShift, int yShift, bool moveSinglePoint)
{
    if (currentAnnotations.size() != 1) {
        return;
    }
    if (images[Image::MaskNoDontCare].empty()) {
        return;
    }

	if (polygonMask)
	{
		if (moveSinglePoint)
		{
			polygonMask->moveCurrentPoint(yShift, xShift);
			drawAndUpdate();
			return;
		} else
		{
			polygonMask->moveEntirePolygon(yShift, xShift);
			drawAndUpdate();
			return;
		}
	}

    Rect source = Rect(max(0, -xShift), max(0, -yShift), images[Image::MaskNoDontCare].cols -
        abs(xShift), images[Image::MaskNoDontCare].rows - abs(yShift));
    Rect target = Rect(max(0, xShift), max(0, yShift), images[Image::MaskNoDontCare].cols -
        abs(xShift), images[Image::MaskNoDontCare].rows - abs(yShift));

    Mat tmpTarget = Mat::zeros(images[Image::MaskNoDontCare].size(), images[Image::MaskNoDontCare].type());
    images[Image::MaskNoDontCare](source).copyTo(tmpTarget(target));
    images[Image::MaskNoDontCare] = tmpTarget.clone();

    drawAndUpdate();
}

void PixelAnnotator::insertAnnotationInList(int frameNumber, const PixelAnnotation & annotation)
{
	QString line = annotationToCsv(frameNumber, annotation);

	// Search for existing annotations with the same frame name and object id
	qDebug() << QString("%1;%2").arg(maskFilename(currentImage, Image::RGB)).arg(QString("%1").arg(annotation.id()) + "(;[^;]*){1,9};");
	if (!outFileLines.empty()) {
		qDebug() << outFileLines[0];
	}

	QStringList existingAnnotations = getExistingAnnotations(maskFolders[Image::RGB], maskFilename(frameNumber, Image::RGB), annotation.id());//outFileLines.filter(QRegExp(QString("%1;%2").arg(maskFilename(currentImage, Image::RGB)).arg(QString("%1").arg(annotations[i].id()) + "(;[^;]*){1,9};")));
	if (existingAnnotations.size() > 0) {
		outFileLines[outFileLines.indexOf(existingAnnotations[0])] = line;
	}
	else {
		outFileLines << line;
	}
}

void PixelAnnotator::repairRegistration()
{

    if (currentAnnotations.size() != 1) {
        return;
    }
    if (images[Image::MaskNoDontCare].empty()) {
        return;
    }

    if (!oldRegistrator.getIsInitialized()) {

        QString filename = QFileDialog::getOpenFileName(this, tr("Open old registration file"), "", "*.yml");

        if (filename.isEmpty()) {
            return;
        }

        oldRegistrator.loadMinCalibrationVars(filename.toStdString());

        if (!images.empty() && !images[Image::RGB].empty()) {
            oldRegistrator.setImageWidth(images[Image::RGB].cols);
            oldRegistrator.setImageHeight(images[Image::RGB].rows);
        }
    }
    
   
    Mat maskNoDontCare = images[Image::MaskNoDontCare].clone(); // If only one mask is selected it is not saved till the selection changes, and we show the working mask to show a drawing in progress.

    if (saveAllModalities && (enabledModalities[Image::Depth] || enabledModalities[Image::Thermal])) {
        // Calculate registered contours:
        Mat dContour = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());
        Mat tContour = Mat::zeros(displayImages[Image::MaskNoDontCare].size(), displayImages[Image::MaskNoDontCare].type());

        Mat tmpDepthImg = images[Image::Depth];

        if (tmpDepthImg.rows < 10) {
            tmpDepthImg = Mat();
        }


        vector< vector<Point> > contourPointsRgb, newContourPointsRgb;
        vector<Vec4i> hierarchy;
        vector<Point2f> contourPointsD;

        findContours(maskNoDontCare, contourPointsRgb, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);


        for (auto i = 0; i < contourPointsRgb.size(); ++i) {
            vector<Point2f> tmpContourPointsT;
            vector<Point2f> convertedRgbPoints;
            vector<Point> tmpRgbPoints;

            for (auto j = 0; j < contourPointsRgb[i].size(); ++j) {
                convertedRgbPoints.push_back(Point2f(contourPointsRgb[i][j].x, contourPointsRgb[i][j].y));

            }

            // Map from RGB to thermal using the old registration algorithm
            oldRegistrator.computeCorrespondingThermalPointFromRgb(convertedRgbPoints, tmpContourPointsT, contourPointsD);
            convertedRgbPoints.clear();

            // Map from thermal to RGB using new registration algorithm
            registrator.computeCorrespondingRgbPointFromThermal(tmpContourPointsT, convertedRgbPoints);

            for (auto j = 0; j < convertedRgbPoints.size(); ++j) {
                tmpRgbPoints.push_back(Point(convertedRgbPoints[j].x+0.5f, convertedRgbPoints[j].y+0.5f));
            }

            newContourPointsRgb.push_back(tmpRgbPoints);

        }

        Mat repairedMask = Mat::zeros(maskNoDontCare.size(), maskNoDontCare.type());
        Scalar color = Scalar::all(images[Image::MaskNoDontCare].at<uchar>(contourPointsRgb[0][0]));

        drawContours(repairedMask, newContourPointsRgb, 0, color, CV_FILLED);
        
        repairedMask.copyTo(images[Image::MaskNoDontCare]);
    }
        

    drawAndUpdate();
}

void PixelAnnotator::on_actionExport_to_bounding_box_annotations_triggered()
{
	QStringList convertedBBAnnotations;

	QProgressDialog progress(tr("Converting annotations..."), tr("Cancel"), 0, numImages, this);

	for (auto imageIdx = 0; imageIdx < numImages; ++imageIdx)
	{
		QString maskFile = maskFilename(imageIdx, Image::RGB);
		if (maskFile.length() > 0) {
			QDir maskNoDontCareDir = baseDir;
			QDir maskOnlyDontCareDir = baseDir;
			maskNoDontCareDir.cd(maskFolders[Image::RGB]);
			maskOnlyDontCareDir.cd(maskFolders[Image::RGB]);
			maskNoDontCareDir.cd(maskFolders[Image::MaskNoDontCare]);
			maskOnlyDontCareDir.cd(maskFolders[Image::MaskOnlyDontCare]);

			Mat maskNoDontCare = imread(maskNoDontCareDir.absoluteFilePath(maskFile).toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
			/*cv::imshow("Mask for image" + maskFile.toStdString(), maskNoDontCare);*/

			QStringList existingAnnotations = outFileLines.filter(maskFile);
			for (int i = 0; i < existingAnnotations.size(); i++) {
				PixelAnnotation a = PixelAnnotation::csvToAnnotation(existingAnnotations[i], csvMetaDataColumnIndex, metaDataNames, metaDataTypes);

				if (maskNoDontCare.empty()) {
					break;
				}

				Mat annotationOnly, dontCareOnly;
				Mat(maskNoDontCare.size(), maskNoDontCare.type(), Scalar(150)).copyTo(annotationOnly, maskNoDontCare == a.id());
				/*cv::imshow("Mask for ID:" + QString::number(a.id()).toStdString(), annotationOnly);
				cv::waitKey(0);*/
				a.setMask(annotationOnly, false); // Don't draw don't care zone here

				// Get annotations mask
				cv::Rect bb = a.getBoundingBox();

				if (bb.area() != 0)
				{

					// Get lower right corner X
					int lRCX = bb.x + bb.width;

					// Get lower right corner Y
					int lRCY = bb.y + bb.height;

					// Converted annotion format:
					// Filename (only RGB);Object ID;Filename;Object ID;Annotation tag;Upper left corner X;Upper left corner Y;Lower right corner X;Lower right corner Y;
					QString convertedLine = maskFile + ";" + QString::number(a.id()) + ";"+ a.getTag() + ";" + QString::number(bb.x) + ";" + QString::number(bb.y) + ";" + QString::number(lRCX) + ";" + QString::number(lRCY) + ";";
					convertedBBAnnotations.append(convertedLine);
				}
			}
		}

		progress.setValue(imageIdx);
	}

	convertedBBAnnotations.sort();

	QFile bbFile(baseDir.absoluteFilePath("convertedBBannotations.csv"));

	bbFile.open(QFile::WriteOnly);
	QTextStream stream;
	stream.setDevice(&bbFile);

	stream << "Filename;Object ID;Annotation tag;Upper left corner X;Upper left corner Y;Lower right corner X;Lower right corner Y;\n";

	for (auto& line : convertedBBAnnotations)
	{
		stream << line << "\n";
	}

	stream.flush();
	bbFile.close();

	QMessageBox::information(this, tr("Finished exporting"), tr("Successfully converted annotations to bounding box format into the file at:\n")
		+ baseDir.absoluteFilePath("convertedBBannotations.csv"));
}

void PixelAnnotator::on_actionEdit_suggested_tags_triggered()
{
	MetaDataDialog d(this, suggestedTags, suggestedTags,
		QObject::tr("Edit suggested tags"),
		QObject::tr("Enter the suggested tag names here, one per line"));
	if (d.exec()) {
		suggestedTags = d.getNames();
	}

	// Write the suggested tags to disk
	QFile tagFile("suggestedTags.txt");
	if (tagFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QTextStream tagStream(&tagFile);

		for (auto line : suggestedTags) {
			tagStream << line << '\n';
		}

		tagStream.flush();
	}

	tagFile.close();
}


void PixelAnnotator::placeholderSlot()
{
    qDebug() << "Not implemented.";
}

void PixelAnnotator::switchAddSubtract()
{
    switch(tool) {
    case Tool::addToMask:
        toolActions[Tool::subtractFromMask]->trigger();
        break;
    case Tool::subtractFromMask:
        toolActions[Tool::addToMask]->trigger();
        break;
    case Tool::addToGrabCut:
        toolActions[Tool::subtractFromGrabCut]->trigger();
        break;
    case Tool::subtractFromGrabCut:
        toolActions[Tool::addToGrabCut]->trigger();
        break;
    }
}

void PixelAnnotator::setMetaDataOptions(QStringList lines)
{
    metaDataNames.clear();
    metaDataTypes.clear();

    for(int i = 0; i < lines.size(); i++) {
        QStringList segments = lines[i].split(":", QString::SkipEmptyParts);

		if (!segments.empty())
		{
			metaDataNames.append(segments[0]);
			metaDataTypes.append("bool");
		}
    }
}

void PixelAnnotator::on_action_Edit_meta_data_fields_triggered()
{
    if(numImages > 0) {
        if(QMessageBox::No == QMessageBox::warning(this, "Annotation is in progress", "Annotation is currently in progress. If you change the meta data fields now, the previous annotations and the csv-header will only contain the original fields. All new annotations will contain the new field set. Are you sure you wish to change the meta data fields?", QMessageBox::Yes | QMessageBox::No)) {
            return;
        }
    }
	
    MetaDataDialog d(this, metaDataNames, metaDataTypes,
		QObject::tr("Edit meta data fields"),
		QObject::tr("Enter the meta data names here, one per line."));
    if(d.exec()) {
        QStringList lines = d.getNames();
        setMetaDataOptions(lines);

        QFile file("metaDataList.txt");
        if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream out(&file);
            for(int i = 0; i < lines.size(); i++) {
                out << lines[i] << "\n";
            }
            file.close();
        }
        updateCurrentAnnotationsPane();
    }
}

void PixelAnnotator::on_action_Settings_triggered()
{
    SettingsDialog d(this, images[Image::Thermal].clone());
    bool modalitiesChanged = false;

    if(d.exec()) {
        QSettings settings;
        saveAllModalities = settings.value("saveAllModalities", true).toBool();
        borderThickness = settings.value("borderThickness", 4).toInt();
        overlayColor = settings.value("overlayColor", QColor(0,0,255)).value<QColor>();
        overlayOpacity = settings.value("overlayOpacity", 0.3).toFloat();
        noiseBlobSize = settings.value("noiseBlobSize", 20).toInt();
        holeFillSize = settings.value("holeFillSize", 20).toInt();
        autoBorderWhenDrawing = settings.value("autoBorderWhenDrawing", false).toBool();
        autoBorderWhenSwitching = settings.value("autoBorderWhenSwitching", false).toBool();

        filePatterns[Image::RGB] = settings.value("rgbPattern", "rgb/*.png").toString();
        filePatterns[Image::Depth] = settings.value("depthPattern", "depth/*.png").toString();
        filePatterns[Image::Thermal] = settings.value("thermalPattern", "thermal/*.png").toString();
        csvPath = settings.value("csvPath", "annotations.csv").toString();
        maskFolders[Image::RGB] = settings.value("rgbMaskFolder", "rgbMasks").toString();
        maskFolders[Image::Depth] = settings.value("depthMaskFolder", "depthMasks").toString();
        maskFolders[Image::Thermal] = settings.value("thermalMaskFolder", "thermalMasks").toString();
        
        if (enabledModalities[Image::RGB] != settings.value("enableRgb", true).toBool()) {
            modalitiesChanged = true;
        }
        else if (enabledModalities[Image::Depth] != settings.value("enableDepth", true).toBool()){
            modalitiesChanged = true;
        }
        else if (enabledModalities[Image::Thermal] != settings.value("enableThermal", true).toBool()){
            modalitiesChanged = true;
        }

        enabledModalities[Image::RGB] = settings.value("enableRgb", true).toBool();
        enabledModalities[Image::Depth] = settings.value("enableDepth", true).toBool();
        enabledModalities[Image::Thermal] = settings.value("enableThermal", true).toBool();
        
        if (modalitiesChanged) {
            // Modalities have changed. Reset ui
            fileLists.clear();
            annotations.clear();
            updateCurrentAnnotationsPane();
            initModalityWindows();
            return;
        }

        // Configure overlay color overlay
        ui->overlayColorLabel->setStyleSheet(QString("QLabel {"
            "background-color: rgb(%1, %2, %3);"
            "border-style: inset;"
            "border-width: 0px;"
            "}"
        ).arg(overlayColor.red()).arg(overlayColor.green()).arg(overlayColor.blue()));

        ui->overlayColorSlider->blockSignals(true);
        ui->overlayColorSlider->setSliderPosition(overlayColor.hue());
        ui->overlayColorSlider->blockSignals(false);

        loadImage(currentImage);
    }
}

void PixelAnnotator::on_action_Open_folder_triggered()
{
    openConfiguration();
}

void PixelAnnotator::openConfiguration(QString dir, QString calibPath, QString maskPath)
{
    QCoreApplication::setOrganizationName("AAU");
    QCoreApplication::setOrganizationDomain("vap.aau.dk");
    QCoreApplication::setApplicationName("Pixel Annotator");
    QSettings settings;
    settings.setValue("saveAllModalities", saveAllModalities = settings.value("saveAllModalities", true).toBool());


    if (dir.isEmpty()) {
        dir = settings.value("baseFolder").toString();

        dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), dir, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    }

    if (dir.length() > 0) {

         settings.setValue("baseFolder", dir);
         baseDir.setPath(dir);

         // Load registration parameters if other modalities than RGB is present
         if (enabledModalities[Image::Depth] || enabledModalities[Image::Thermal])
         {
             if (settings.value("useMultipleHom").toBool()) {
                 // Use advanced, multiple homographies to map between points. Depth is supported.
				 QDir topLevelDir = baseDir;
				 topLevelDir.cdUp();

                 if (calibPath.isEmpty()) {
                     if (baseDir.exists("calibVars.yml")) {
                         calibPath = baseDir.absoluteFilePath("calibVars.yml");
                     }
					 else if (topLevelDir.exists("calibVars.yml")) {
						 calibPath = topLevelDir.absoluteFilePath("calibVars.yml");
					 }
                     else {
                         calibPath = QFileDialog::getOpenFileName(this, tr("Open multi-modal calibration file"), topLevelDir.absolutePath(), "*.yml");
                     }
                 }

                 if (!calibPath.isEmpty()) {
                     registrator.loadMinCalibrationVars(calibPath.toStdString());
                     this->calibPath = calibPath;

                 }
                 else {
                     QMessageBox::critical(this, tr("No registration file present"),
                         QString(tr("A file named calibVars.yml containing the image registration parameters must be present in the directory. Cancelling operation.")),
                         QMessageBox::Ok);
                     return;
                 }
             }
             else if (settings.value("usePlanarHom").toBool()) {
                 // Use a single, planar homography to map between points. Depth is not supported

                 if (enabledModalities[Image::Depth]) {
                     QMessageBox::critical(this, tr("Depth is not supported"),
                         tr("The depth modality is not supported for the registration by a single, planar homography. Use a different registration method to"
                             " enable the depth modality. \n Cancelling operation"), QMessageBox::Ok);
                     return;
                 }

                 QString calibPath;

                 if (baseDir.exists(".yml")) {
                     calibPath = baseDir.absoluteFilePath("*.yml");
                 }
                 else {
                     QString calibDir = settings.value("calibBaseFolder").toString();

                     if (calibDir.isEmpty()) {
                         calibDir = dir;
                     }

                     calibPath = QFileDialog::getOpenFileName(this, tr("Open homography file"), calibDir, "*.yml");

                     if (!calibPath.isEmpty()) {
                         registrator.loadHomography(calibPath.toStdString());
                         settings.setValue("calibBaseFolder", calibPath);
                     }
                     else {
                         QMessageBox::critical(this, tr("No registration file present"),
                             QString(tr("A .yml-file containing two registration matrices; homRgbToT and homTToRgb must be present in the directory. Cancelling operation.")),
                             QMessageBox::Ok);
                         return;
                     }
                 }
             }
         } 
         

         baseDir.setFilter(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
         baseDir.setSorting(QDir::Name);

         bool missingFiles = false;
         bool inqeualNumberOfFiles = false;

         for(int i = Image::RGB; i <= Image::Thermal; i++) {
             QDir currentDir = baseDir;
             currentDir.cd(filePatterns[i].left(filePatterns[i].lastIndexOf(QRegExp("[\\\\|/]")))); // If the pattern contains folders, cd into the proper folder (the regex matches backslashes or forward slashes. Lots of escaping involved.).
             fileLists[i] = currentDir.entryList(QStringList(filePatterns[i].section(QRegExp("[\\\\|/]"), -1)));
             for(int j = 0; j < fileLists[i].size(); j++) {
                 fileLists[i][j] = currentDir.absoluteFilePath(fileLists[i][j]);
             }

             if (enabledModalities[i] && fileLists[i].empty()) {
                 missingFiles = true;
             }
             
             if (enabledModalities[i]) {
                 if (i == Image::RGB) {
                     numImages = fileLists[i].size();
                 }
                 else if (numImages > fileLists[i].size()) {
                     numImages = fileLists[i].size();  // Set the number of images to the lowest common denominator.
                     inqeualNumberOfFiles = true;
                 }
             }
         }

         if(missingFiles) {
             QString errorMessage = "There must be at least 1 image for each modality.\n\n";

             for (int i = Image::RGB; i <= Image::Thermal; ++i) {
                 if (enabledModalities[i]) {
                     errorMessage.append(QString(getModalityName(i) + " has %1 images.\n").arg(fileLists[i].size()));
                 }
             }
             errorMessage.append("\nCheck the file patterns in Settings.");

             QMessageBox::critical(this, "Missing image files", errorMessage, QMessageBox::Ok);
             fileLists.clear();
             return;
         }

         if(inqeualNumberOfFiles) {
             QString warningMessage = "Beware that there is not an equal number of images for all modalities.\n\n";

             for (int i = Image::RGB; i <= Image::Thermal; ++i) {
                 if (enabledModalities[i]) {
                     warningMessage.append(getModalityName(i) + QString(" has %1 images.\n").arg(fileLists[i].count()));
                 }
             }
             warningMessage.append(QString("\nOnly the first %1 images will be annotated").arg(numImages));

             QMessageBox::warning(this, "Unequal number of image files", warningMessage, QMessageBox::Ok);
         }

         imageSelector->setMaximum(numImages);
         setCurrentAnnotations(QSet<int>());
         annotations.clear();
         outFileLines.clear();

         outFile.setFileName(baseDir.absoluteFilePath(csvPath));
         if(baseDir.exists(csvPath)) { // Read existing annotations
             // Open existing annotation file
             outFile.open(QFile::ReadOnly);
             QTextStream stream;
             stream.setDevice(&outFile);
			 csvTagColumnIndex = -1;

             if(!stream.atEnd()) {
                 QStringList topEntry = stream.readLine().split(";", QString::SkipEmptyParts);

				 int idx = 0;

				 if (topEntry.contains("Annotation tag"))
				 {
					 // If the annotation file supports tags, the meta data are written hereafter
					 idx = topEntry.indexOf("Annotation tag");
					 csvTagColumnIndex = idx;
				 } else {
					 // Otherwise, find the entries after "Object ID"
					 idx = topEntry.indexOf("Object ID");
				 }
                 
                 QStringList metaData = topEntry.mid(idx + 1).join(',').split(',');

                 csvMetaDataColumnIndex = idx + 1;

                 setMetaDataOptions(metaData);
             }
             while(!stream.atEnd()) {
                 outFileLines << stream.readLine();

				 QStringList split = outFileLines.back().split(";", QString::SkipEmptyParts);

				 if (split.size() > csvTagColumnIndex && csvTagColumnIndex >= 0) {
					 //Populate the suggested tags
					 if (!suggestedTags.empty() && !suggestedTags.contains(split[csvTagColumnIndex])) {
						 suggestedTags << split[csvTagColumnIndex];
					 }
					 else if (suggestedTags.empty())
					 {
						 suggestedTags << split[csvTagColumnIndex];
					 }
				 }
             }
             outFile.close();
         }
         outFile.open(QFile::WriteOnly);

         // Create mask folders (if either exists already nothing happens)
         for (int i = Image::RGB; i <= Image::MaskOnlyDontCare; i++) {

             if (i == Image::MaskNoDontCare || i == Image::MaskOnlyDontCare) {
                 QDir rgbDir = baseDir;
                 rgbDir.cd(maskFolders[Image::RGB]);
                 rgbDir.mkpath(maskFolders[i]);
             }
             else {
                 baseDir.mkpath(maskFolders[i]);
             }
         }
         
         saveCSVFile();

         // Proceed to load the image to obtain image dimensions needed for the registrator module. 
         // We won't show the annotations in this pass as we need the registrator module to be 
         // configured appropirately first

         loadImage(0, false, false);

         // Set the image dimensions in the registrator module
         if (images.size() > 3 && !images[Image::Thermal].empty()) {
             registrator.setImageWidth(images[Image::Thermal].cols);
             registrator.setImageHeight(images[Image::Thermal].rows);
         }
         else if (!images.empty() && !images[Image::RGB].empty()) {
             registrator.setImageWidth(images[Image::RGB].cols);
             registrator.setImageHeight(images[Image::RGB].rows);
         }

         // If we have enabled the don't care mask, prompt the user to load it
         QSettings settings;
         bool showMask = settings.value("useMask").toBool();

         if (showMask || !maskPath.isEmpty()) {

			 QDir topLevelDir = baseDir;
			 QString currentDirMask = baseDir.dirName() + "-mask.png";
			 topLevelDir.cdUp();

			 if (maskPath.isEmpty())
			 {
				 if (baseDir.exists("mask.png")) {
					 maskPath = baseDir.absoluteFilePath("mask.png");
				 }
				 else if (topLevelDir.exists("mask.png")) {
					 maskPath = topLevelDir.absoluteFilePath("mask.png");
				 }
				 else if (baseDir.exists(currentDirMask)) {
					 maskPath = baseDir.absoluteFilePath(currentDirMask);
				 }
				 else if (topLevelDir.exists(currentDirMask)) {
					 maskPath = topLevelDir.absoluteFilePath(currentDirMask);
				 }
				 else {
					 maskPath = settings.value("maskFolder").toString();
					 maskPath = QFileDialog::getOpenFileName(this, tr("Open don't care mask"), topLevelDir.absolutePath(), "*.png");
				 }
			 }

             if (!maskPath.isEmpty()) {
                 settings.setValue("maskFolder", maskPath);
                 this->maskPath = maskPath;

                 Mat invDontCareMaskRgb = imread(maskPath.toStdString(), CV_LOAD_IMAGE_GRAYSCALE);

                 if (enabledModalities[Image::Depth] || enabledModalities[Image::Thermal]) {
                     // If the modality is thermal and/or depth, we need to map the mask to the these domains
                     Mat invDontCareMaskThermal = Mat::zeros(invDontCareMaskRgb.size(), invDontCareMaskRgb.type());
                     Mat invDontCareMaskDepth = Mat::zeros(invDontCareMaskRgb.size(), invDontCareMaskRgb.type()), emptyMask;

                     registrator.drawRegisteredContours(invDontCareMaskRgb, invDontCareMaskDepth, invDontCareMaskThermal, emptyMask, MAP_FROM_RGB);

                     QFileInfo maskFileInfo = QFileInfo(maskPath);

                     if (enabledModalities[Image::Depth]) {
                         imwrite(maskFileInfo.absolutePath().toStdString() + maskFileInfo.baseName().toStdString() + "-depth.png", 
                             invDontCareMaskDepth);
                     }

                     if (enabledModalities[Image::Thermal]) {
                         imwrite(maskFileInfo.absolutePath().toStdString() + "/" + maskFileInfo.baseName().toStdString() + "-thermal.png",
                             invDontCareMaskThermal);
                     }

                     // Invert the don't care masks
                     cv::bitwise_not(invDontCareMaskThermal, dontCareMasks[Image::Thermal]);
                     cv::bitwise_not(invDontCareMaskDepth, dontCareMasks[Image::Depth]);
                 }

                 cv::bitwise_not(invDontCareMaskRgb, dontCareMasks[Image::RGB]);
             }
             else {
                 settings.setValue("useMask", false);
             }
         }
         
         // Proceed to load the image
         loadImage(0, false);

         for(int i = Image::RGB; i <= Image::MaskNoDontCare; i++) {
             viewers[getModalityIndex(i)]->resize(QSize(viewers[getModalityIndex(i)]->size().width()-1, viewers[getModalityIndex(i)]->size().height()-1));
         }

         ui->actionSave_configuration->setEnabled(true);
    }
}

void PixelAnnotator::on_actionTile_triggered()
{
    this->ui->mainArea->tileSubWindows();
}

void PixelAnnotator::on_action_Quit_triggered()
{
    QCoreApplication::exit();
}

void PixelAnnotator::on_action_About_triggered()
{
    QString message = tr("Created by Andreas Møgelmose and Chris Holmberg Bahnsen, Aalborg University.\n\n"
        "Please visit https://bitbucket.org/aauvap/multimodal-pixel-annotator for the newest version of the program.\n\n\n"
        "This program is licensed under the MIT License:\n"
        "Copyright(c) 2016 Aalborg University\n\n"
        "Permission is hereby granted, free of charge, to any person obtaining a copy "
        "of this software and associated documentation files(the \"Software\"), to deal "
        "in the Software without restriction, including without limitation the rights "
        "to use, copy, modify, merge, publish, distribute, sublicense, and / or sell "
        "copies of the Software, and to permit persons to whom the Software is "
        "furnished to do so, subject to the following conditions : \n\n"
        "The above copyright notice and this permission notice shall be included in all "
        "copies or substantial portions of the Software.\n\n"
        "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR "
        "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, "
        "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE "
        "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER "
        "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, "
        "OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
        "\n\n-----------------------------------------\n"
        "\Some icons by Yusuke Kamiyamane. All rights reserved. Icons licensed under a Creative Commons Attribution 3.0 License.\n"
        "-----------------------------------------\n"
        "Created using the QT Framework under the LGPL License. For more information about the QT framework regarding this program, please refer to cb@create.aau.dk");
                        
    QMessageBox::about(this, "AAU VAP Pixel Annotator", message);
}

void PixelAnnotator::on_actionReport_bug_triggered()
{
    QString description("Report bugs or enhancements at:<br>");
    QString hyperlinkText("https://bitbucket.org/aauvap/multimodal-pixel-annotator/issues?status=new&status=open");

    QMessageBox::about(this, "AAU VAP Pixel Annotator", QString("<span>%1&nbsp;&nbsp;</span><a href=\"%2\">%2</a>").arg(description).arg(hyperlinkText));
}

bool PixelAnnotator::getNewId(int& newId)
{
    // Returns true if we are able to get a new id that is not 
    // already used and lies within the id number restrictions.
    // Returns false if no id is available

    // Try to get a new id in the preferred range from [200 255]
    for (auto i = 200; i <= 255; ++i) {
        if (!idInList(i, annotations)) {
			// Check if this ID is used in future frames. If so, it might be beneficial to use 
			// another one as we might overwrite them

			std::map<int, int> sameIdAnnotationsTable;
			findFutureAnnotationsWithSameId(sameIdAnnotationsTable, i, 20);

			if (sameIdAnnotationsTable.empty())
			{
				newId = i;
				return true;
			}
        }
    }

    // If that is not available, try in the range [100 - 199]
    for (auto i = 100; i < 199; ++i) {
        if (!idInList(i, annotations)) {
            newId = i;
            return true;
        }
    }

    // No id is available :| Return false
    return false;
}

bool PixelAnnotator::idInList(int id, QList<PixelAnnotation> list)
{
    // Check if the id is in the range [0 10] [170]. If it is, 
    // the id is by definition not allowed, and we thus treat it as it already
    // exists - and thus, the id is not valid. Furthermore, the id must be 
    // within the range [1 - 255] as we are working with 8-bit images


    if (id >= 0 && id <= 10) {
        return true;
    }
    else if (id == 170) {
        return true;
    }
    else if (id < 0 || id > 255)
    {
        return true;
    }

    for(int i = 0; i < list.size(); i++) {
        if(id == list[i].id()) {
            return true;
        }
    }
    return false;
}


QString PixelAnnotator::annotationToCsv(int frameNumber, const PixelAnnotation & annotation)
{
	QStringList line;

	for (int i = 0; i <= Image::Thermal; ++i) {
		if (enabledModalities[i]) {
			line << QString("./%1/%2").arg(maskFolders[i]).arg(maskFilename(frameNumber, i));
		}
	}

	int enabledModalitiesCount = 0;

	for (int i = 0; i <= Image::Thermal; ++i) {
		if (enabledModalities[i]) {
			line << baseDir.relativeFilePath(fileLists[i][frameNumber]);
			++enabledModalitiesCount;
		}
	}

	qDebug() << "maskFolders: " << maskFolders[Image::RGB];
	qDebug() << "maskFilename: " << maskFilename(currentImage, Image::RGB);

	return PixelAnnotation::annotationToCsv(annotation, line);
}

QString PixelAnnotator::maskFilename(int index, int modality)
{
    assert(index >= 0 && index < numImages);
    assert(modality >= Image::RGB && modality <= Image::MaskOnlyDontCare);

    QString maskFile;

    if (modality == Image::MaskNoDontCare || modality == Image::MaskOnlyDontCare) {
        // The "modality" belongs to the RGB domain. Treat it as such
        modality = Image::RGB;
    }

    if (enabledModalities[modality]) {
        QRegExp extractFilename("([^/]+)\\.[^\\.]+$");
        extractFilename.indexIn(fileLists[modality][index]);
        //maskFile = QFileInfo(fileLists[modality][index]).fileName();
		maskFile = extractFilename.cap(1);
        maskFile += ".png";
    }
    return maskFile;
}

void PixelAnnotator::on_actionExport_annotations_triggered()
{
	if (!images.empty() && !images[Image::RGB].empty())
	{
		AnnotationExporter annExporter(this, outFileLines, 
			images[Image::RGB].cols, 
			images[Image::RGB].rows,
			baseDir.path(),
			csvMetaDataColumnIndex,
			metaDataNames,
			metaDataTypes,
			fileLists,
			maskFolders[Image::MaskNoDontCare],
			numImages);
		annExporter.exec();
	}
}

QString PixelAnnotator::getModalityName(int modality)
{
    QString outputModality;

    switch (modality)
    {
    case Image::RGB:
    {
        outputModality = "RGB";
        break;
    }
    case Image::Thermal:
    {
        outputModality = "Thermal";
        break;
    }
    case Image::Depth:
    {
        outputModality = "Depth";
        break;
    }
    case Image::MaskNoDontCare:
    {
        outputModality = "Mask";
        break;
    }
    default:
        outputModality = "ErrorModality";

    }

    return outputModality;

}

int PixelAnnotator::getModalityIndex(int modality)
{
    int index = 0;

    for (int i = Image::RGB; i <= Image::MaskOnlyDontCare; ++i)
    {
        if (i == modality)
        {
            return index; 
        }
        
        if (enabledModalities[i])
        {
            ++index;
        }
    }

    // Not supported - return -1
    return -1;
}

void PixelAnnotator::on_actionLoad_configuration_triggered()
{
    QSettings settings;

    QString dir = settings.value("baseFolder").toString();
    QString filename = QFileDialog::getOpenFileName(this, tr("Load configuration"), dir, "*.yml");

    if (filename.isEmpty()) {
        return;
    }

    FileStorage fs(filename.toStdString(), FileStorage::READ);

    if (fs.isOpened()) {
        settings.setValue("baseFolder", QFileInfo(filename).absolutePath());

        std::string directory;
        fs["directory"] >> directory;

        std::string calibVars;
        fs["calibVars"] >> calibVars;

        std::string mask;
        fs["mask"] >> mask;

        openConfiguration(QString::fromStdString(directory), QString::fromStdString(calibVars), QString::fromStdString(mask));

        fs.release();
    }
}

void PixelAnnotator::on_actionSave_configuration_triggered()
{
    QSettings settings;

    QString dir = settings.value("baseFolder").toString();

    QString filename = QFileDialog::getSaveFileName(this, "Save configuration", dir, "*.yml");

    if (!filename.isEmpty()) {
        FileStorage fs(filename.toStdString(), FileStorage::WRITE);

        fs << "directory" << baseDir.absolutePath().toStdString();
        fs << "calibVars" << calibPath.toStdString();
        fs << "mask" << maskPath.toStdString();

        fs.release();
    }
}

void PixelAnnotator::on_overlayColorSlider_valueChanged()
{
    overlayColor.setHsv(ui->overlayColorSlider->value(), 255, 255);

    ui->overlayColorLabel->setStyleSheet(QString("QLabel {"
        "background-color: rgb(%1, %2, %3);"
        "border-style: inset;"
        "border-width: 0px;"
        "}"
        ).arg(overlayColor.red()).arg(overlayColor.green()).arg(overlayColor.blue()));

    QSettings settings;
    settings.setValue("overlayColor", overlayColor);

    drawAndUpdate();
}

bool KeyPressEater::eventFilter(QObject *obj, QEvent *event)
{
    QKeyEvent *e = static_cast<QKeyEvent *>(event);

    if (event->type() == QEvent::KeyPress) {
        switch (e->key()) {
        case Qt::Key_0:
        case Qt::Key_1:
        case Qt::Key_3:
        case Qt::Key_5:
		case Qt::Key_Alt:
        case Qt::Key_7:
            qDebug() << "Hello from KeyPressEater. Key: " << e->key();
            return false;
        }
    }

    // No shortkey - standard event processing
    return QObject::eventFilter(obj, event);
    return true;
}

void PixelAnnotator::findFutureAnnotationsWithSameId(std::map<int, int>& samedIdAnnotationsTable, int annotationId, int futureRange)
{
	int enabledModalitiesCount = 0;

	for (int i = 0; i <= Image::Thermal; ++i) {
		if (enabledModalities[i]) {
			++enabledModalitiesCount;
		}
	}

	samedIdAnnotationsTable.clear();

	int regExCount = 1 + ((enabledModalitiesCount - 1) * 2);
	QRegExp rx(QString("./*/*") + "(;[^;]*){" + QString("%1").arg(regExCount) + "};" + QString("%1").arg(annotationId) + ";*");


	if (!fileLists[Image::RGB].empty())
	{
		// Get the base directory of the image frames
		QString baseDir = QFileInfo(fileLists[Image::RGB].first()).absolutePath();

		for (auto& line : outFileLines)
		{
			if (rx.indexIn(line) != -1)
			{
				// We found an entry with the same ID and tag. Now check if it is in the future frames
				QStringList entries = line.split(";");

				if (entries.size() > enabledModalitiesCount)
				{
					QString rgbFile = entries[enabledModalitiesCount];
					QString search = QDir(baseDir).absoluteFilePath(rgbFile);
					QRegularExpression extractFilename(search);
					int idx = fileLists[Image::RGB].indexOf(extractFilename);

					if (idx >= 0 && idx > currentImage && idx <= currentImage + futureRange)
					{
						PixelAnnotation ann = PixelAnnotation::csvToAnnotation(line, csvMetaDataColumnIndex, metaDataNames, metaDataTypes);

						if (ann.id() > 0)
						{
							samedIdAnnotationsTable[idx] = ann.id();
						}
					}
				}
			}
		}
	}
}
