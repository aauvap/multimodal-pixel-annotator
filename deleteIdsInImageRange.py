# MIT License
# 
# Copyright(c) 2018 Aalborg University
# Chris H. Bahnsen, June 2018
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions :
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import csv
import os


def deleteIdInImageRange(annotationFile, id, startImageNumber, endImageNumber):
    """
    Deletes the specific ID in the range from [startImageNumber, endImageNumber]
    The annotationFile should originate from either the AAU VAP Multimodal Pixel Annotator
    or the AAU VAP Bounding Box Annotator

    """

    annDir = os.path.dirname(annotationFile)

    files = [f for f in os.listdir(annDir) if os.path.isfile(os.path.join(annDir, f))]

    # Build a list of files and their corresponding file numbers
    sFiles = sorted(files)
    counter = 0

    fileNameIdLookup = dict()

    for f in sFiles:
        if '.png' in f or '.jpg' in f:
            counter += 1
            fileNameIdLookup[f] = counter
    
    rowsToKeep = []

    with open(annotationFile, newline='') as csvfile:
        annotationReader = csv.reader(csvfile, delimiter=';')

        for row in annotationReader:
            toBeRemoved = False

            if len(row) > 2:
                imageFile = row[1]

                if imageFile in fileNameIdLookup:
                    fileNumber =  fileNameIdLookup[imageFile]

                    if fileNumber >= startImageNumber and fileNumber < endImageNumber:
                        # To be removed from list if the ID is correct
                        for column in row:
                            if column == str(id):
                                # We have a match
                                toBeRemoved = True
                                break

                        if toBeRemoved:
                            print('Removing ID ' + str(id) + ' from frame ' + str(fileNumber) + ', ' + imageFile)


            if not toBeRemoved:
                rowsToKeep.append(row)

    modifiedName = annotationFile.replace('.csv', '-modified.csv')

    with open(modifiedName, 'w', newline='') as csvfile:
        print('Writing the remaining annotations to: ' + modifiedName)
        annotationWriter = csv.writer(csvfile, delimiter=';')

        for row in rowsToKeep:
            annotationWriter.writerow(row)



if __name__ == "__main__":
    ap = argparse.ArgumentParser(
            description = "    Deletes the specific ID in the range from [startImageNumber, endImageNumber].  The annotationFile should originate from either the AAU VAP Multimodal Pixel Annotator or the AAU VAP Bounding Box Annotator")
    ap.add_argument("-f", "--annotationFile", type=str, required = True,
                    help="The annotation file to be manipulated", default='annotations.csv')
    ap.add_argument("-s", "--startImageNumber", type=str, required = True,
                    help="Starting from this frame (inclusive), remote the specified ID")
    ap.add_argument("-e", "--endImageNumber", type=str, required = True,
                    help="Ending with this frame (exclusive), remote the specified ID")
    ap.add_argument("-i", "--id", type=str, required = True,
                    help="ID of the annotation to be removed")
    
    args = vars(ap.parse_args())
    
    deleteIdInImageRange(args['annotationFile'], 
                         int(args['id']), 
                         int(args['startImageNumber']),
                         int(args['endImageNumber']))