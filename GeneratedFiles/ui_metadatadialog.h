/********************************************************************************
** Form generated from reading UI file 'metadatadialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_METADATADIALOG_H
#define UI_METADATADIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_MetaDataDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QPlainTextEdit *metaDataNames;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *MetaDataDialog)
    {
        if (MetaDataDialog->objectName().isEmpty())
            MetaDataDialog->setObjectName(QStringLiteral("MetaDataDialog"));
        MetaDataDialog->resize(400, 300);
        verticalLayout = new QVBoxLayout(MetaDataDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(MetaDataDialog);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        metaDataNames = new QPlainTextEdit(MetaDataDialog);
        metaDataNames->setObjectName(QStringLiteral("metaDataNames"));

        verticalLayout->addWidget(metaDataNames);

        buttonBox = new QDialogButtonBox(MetaDataDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Save);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(MetaDataDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), MetaDataDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), MetaDataDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(MetaDataDialog);
    } // setupUi

    void retranslateUi(QDialog *MetaDataDialog)
    {
        MetaDataDialog->setWindowTitle(QApplication::translate("MetaDataDialog", "Edit meta data fields", 0));
        label->setText(QApplication::translate("MetaDataDialog", "Enter the meta data names here, one per line.", 0));
    } // retranslateUi

};

namespace Ui {
    class MetaDataDialog: public Ui_MetaDataDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_METADATADIALOG_H
