/********************************************************************************
** Form generated from reading UI file 'adjustpicturedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADJUSTPICTUREDIALOG_H
#define UI_ADJUSTPICTUREDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSlider>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "opencvviewer.h"

QT_BEGIN_NAMESPACE

class Ui_AdjustPictureDialog
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    OpenCVViewer *openCVView;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_2;
    QSlider *brightnessSlider;
    QDoubleSpinBox *brightnessSpinBox;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout;
    QSlider *contrastSlider;
    QDoubleSpinBox *contrastSpinBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QWidget *AdjustPictureDialog)
    {
        if (AdjustPictureDialog->objectName().isEmpty())
            AdjustPictureDialog->setObjectName(QStringLiteral("AdjustPictureDialog"));
        AdjustPictureDialog->resize(446, 458);
        verticalLayout_2 = new QVBoxLayout(AdjustPictureDialog);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        groupBox = new QGroupBox(AdjustPictureDialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        openCVView = new OpenCVViewer(groupBox);
        openCVView->setObjectName(QStringLiteral("openCVView"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(openCVView->sizePolicy().hasHeightForWidth());
        openCVView->setSizePolicy(sizePolicy);
        openCVView->setMinimumSize(QSize(200, 200));

        verticalLayout->addWidget(openCVView);

        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        horizontalLayout_2 = new QHBoxLayout(groupBox_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        brightnessSlider = new QSlider(groupBox_2);
        brightnessSlider->setObjectName(QStringLiteral("brightnessSlider"));
        brightnessSlider->setMinimum(-150);
        brightnessSlider->setMaximum(150);
        brightnessSlider->setPageStep(25);
        brightnessSlider->setValue(0);
        brightnessSlider->setOrientation(Qt::Horizontal);
        brightnessSlider->setTickPosition(QSlider::TicksAbove);

        horizontalLayout_2->addWidget(brightnessSlider);

        brightnessSpinBox = new QDoubleSpinBox(groupBox_2);
        brightnessSpinBox->setObjectName(QStringLiteral("brightnessSpinBox"));
        brightnessSpinBox->setEnabled(true);
        brightnessSpinBox->setDecimals(0);
        brightnessSpinBox->setMinimum(-150);
        brightnessSpinBox->setMaximum(150);

        horizontalLayout_2->addWidget(brightnessSpinBox);


        verticalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout = new QHBoxLayout(groupBox_3);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        contrastSlider = new QSlider(groupBox_3);
        contrastSlider->setObjectName(QStringLiteral("contrastSlider"));
        contrastSlider->setMinimum(10);
        contrastSlider->setMaximum(80);
        contrastSlider->setSingleStep(0);
        contrastSlider->setPageStep(10);
        contrastSlider->setValue(30);
        contrastSlider->setOrientation(Qt::Horizontal);
        contrastSlider->setTickPosition(QSlider::TicksAbove);

        horizontalLayout->addWidget(contrastSlider);

        contrastSpinBox = new QDoubleSpinBox(groupBox_3);
        contrastSpinBox->setObjectName(QStringLiteral("contrastSpinBox"));
        contrastSpinBox->setEnabled(true);
        contrastSpinBox->setMaximum(3.5);

        horizontalLayout->addWidget(contrastSpinBox);


        verticalLayout->addWidget(groupBox_3);


        verticalLayout_2->addWidget(groupBox);

        buttonBox = new QDialogButtonBox(AdjustPictureDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Save);

        verticalLayout_2->addWidget(buttonBox);


        retranslateUi(AdjustPictureDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), AdjustPictureDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AdjustPictureDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(AdjustPictureDialog);
    } // setupUi

    void retranslateUi(QWidget *AdjustPictureDialog)
    {
        AdjustPictureDialog->setWindowTitle(QApplication::translate("AdjustPictureDialog", "Adjust Thermal Image", 0));
        groupBox->setTitle(QApplication::translate("AdjustPictureDialog", "Thermal image", 0));
        groupBox_2->setTitle(QApplication::translate("AdjustPictureDialog", "Brightness", 0));
        groupBox_3->setTitle(QApplication::translate("AdjustPictureDialog", "Contrast", 0));
    } // retranslateUi

};

namespace Ui {
    class AdjustPictureDialog: public Ui_AdjustPictureDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADJUSTPICTUREDIALOG_H
