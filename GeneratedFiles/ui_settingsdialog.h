/********************************************************************************
** Form generated from reading UI file 'settingsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SettingsDialog
{
public:
    QVBoxLayout *verticalLayout_6;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox_4;
    QFormLayout *formLayout_4;
    QCheckBox *enableRgbCheckBox;
    QCheckBox *enableDepthCheckBox;
    QCheckBox *enableThermalCheckBox;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout;
    QRadioButton *usePlanarHomRadioButton;
    QRadioButton *useMultipleHomRadioButton;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout;
    QLabel *label_16;
    QCheckBox *useDontCareMask;
    QLabel *label_17;
    QPushButton *btnDontCareColor;
    QLabel *label_18;
    QDoubleSpinBox *dontCareOverlayOpacitySpinBox;
    QGroupBox *imageColorAdjustmentsGroupBox;
    QVBoxLayout *verticalLayout_2;
    QPushButton *adjustThermalImagePushButton;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_7;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QLabel *label_13;
    QCheckBox *saveAllModalities;
    QLabel *label;
    QSpinBox *borderThickness;
    QLabel *label_7;
    QPushButton *btnColor;
    QLabel *label_14;
    QDoubleSpinBox *overlayOpacitySpinBox;
    QLabel *label_8;
    QSpinBox *noiseBlobSize;
    QLabel *label_9;
    QSpinBox *holeFillSize;
    QLabel *label_12;
    QCheckBox *autoBorderWhenDrawing;
    QLabel *label_15;
    QCheckBox *autoBorderWhenSwitching;
    QGroupBox *groupBox_8;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *limitTagsCheckBox;
    QSpacerItem *verticalSpacer;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_2;
    QFormLayout *formLayout_2;
    QLabel *label_2;
    QLineEdit *rgbPattern;
    QLabel *label_3;
    QLineEdit *depthPattern;
    QLabel *label_4;
    QLineEdit *thermalPattern;
    QGroupBox *groupBox_3;
    QFormLayout *formLayout_3;
    QLabel *label_5;
    QLineEdit *csvPath;
    QLabel *label_6;
    QLineEdit *rgbMaskFolder;
    QLabel *label_10;
    QLineEdit *depthMaskFolder;
    QLabel *label_11;
    QLineEdit *thermalMaskFolder;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SettingsDialog)
    {
        if (SettingsDialog->objectName().isEmpty())
            SettingsDialog->setObjectName(QStringLiteral("SettingsDialog"));
        SettingsDialog->resize(426, 448);
        verticalLayout_6 = new QVBoxLayout(SettingsDialog);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        tabWidget = new QTabWidget(SettingsDialog);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_4 = new QVBoxLayout(tab);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        groupBox_4 = new QGroupBox(tab);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        formLayout_4 = new QFormLayout(groupBox_4);
        formLayout_4->setObjectName(QStringLiteral("formLayout_4"));
        enableRgbCheckBox = new QCheckBox(groupBox_4);
        enableRgbCheckBox->setObjectName(QStringLiteral("enableRgbCheckBox"));
        enableRgbCheckBox->setEnabled(false);
        enableRgbCheckBox->setCheckable(true);
        enableRgbCheckBox->setChecked(true);

        formLayout_4->setWidget(0, QFormLayout::LabelRole, enableRgbCheckBox);

        enableDepthCheckBox = new QCheckBox(groupBox_4);
        enableDepthCheckBox->setObjectName(QStringLiteral("enableDepthCheckBox"));

        formLayout_4->setWidget(1, QFormLayout::LabelRole, enableDepthCheckBox);

        enableThermalCheckBox = new QCheckBox(groupBox_4);
        enableThermalCheckBox->setObjectName(QStringLiteral("enableThermalCheckBox"));

        formLayout_4->setWidget(2, QFormLayout::LabelRole, enableThermalCheckBox);


        verticalLayout_4->addWidget(groupBox_4);

        groupBox_5 = new QGroupBox(tab);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        verticalLayout = new QVBoxLayout(groupBox_5);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        usePlanarHomRadioButton = new QRadioButton(groupBox_5);
        usePlanarHomRadioButton->setObjectName(QStringLiteral("usePlanarHomRadioButton"));

        verticalLayout->addWidget(usePlanarHomRadioButton);

        useMultipleHomRadioButton = new QRadioButton(groupBox_5);
        useMultipleHomRadioButton->setObjectName(QStringLiteral("useMultipleHomRadioButton"));

        verticalLayout->addWidget(useMultipleHomRadioButton);


        verticalLayout_4->addWidget(groupBox_5);

        groupBox_6 = new QGroupBox(tab);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        gridLayout = new QGridLayout(groupBox_6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_16 = new QLabel(groupBox_6);
        label_16->setObjectName(QStringLiteral("label_16"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_16->sizePolicy().hasHeightForWidth());
        label_16->setSizePolicy(sizePolicy);

        gridLayout->addWidget(label_16, 0, 0, 1, 1);

        useDontCareMask = new QCheckBox(groupBox_6);
        useDontCareMask->setObjectName(QStringLiteral("useDontCareMask"));
        useDontCareMask->setEnabled(true);
        useDontCareMask->setChecked(false);

        gridLayout->addWidget(useDontCareMask, 0, 1, 1, 1);

        label_17 = new QLabel(groupBox_6);
        label_17->setObjectName(QStringLiteral("label_17"));
        sizePolicy.setHeightForWidth(label_17->sizePolicy().hasHeightForWidth());
        label_17->setSizePolicy(sizePolicy);

        gridLayout->addWidget(label_17, 1, 0, 1, 1);

        btnDontCareColor = new QPushButton(groupBox_6);
        btnDontCareColor->setObjectName(QStringLiteral("btnDontCareColor"));

        gridLayout->addWidget(btnDontCareColor, 1, 1, 1, 1);

        label_18 = new QLabel(groupBox_6);
        label_18->setObjectName(QStringLiteral("label_18"));
        sizePolicy.setHeightForWidth(label_18->sizePolicy().hasHeightForWidth());
        label_18->setSizePolicy(sizePolicy);

        gridLayout->addWidget(label_18, 2, 0, 1, 1);

        dontCareOverlayOpacitySpinBox = new QDoubleSpinBox(groupBox_6);
        dontCareOverlayOpacitySpinBox->setObjectName(QStringLiteral("dontCareOverlayOpacitySpinBox"));
        dontCareOverlayOpacitySpinBox->setDecimals(0);
        dontCareOverlayOpacitySpinBox->setMaximum(100);
        dontCareOverlayOpacitySpinBox->setSingleStep(5);
        dontCareOverlayOpacitySpinBox->setValue(30);

        gridLayout->addWidget(dontCareOverlayOpacitySpinBox, 2, 1, 1, 1);


        verticalLayout_4->addWidget(groupBox_6);

        imageColorAdjustmentsGroupBox = new QGroupBox(tab);
        imageColorAdjustmentsGroupBox->setObjectName(QStringLiteral("imageColorAdjustmentsGroupBox"));
        verticalLayout_2 = new QVBoxLayout(imageColorAdjustmentsGroupBox);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        adjustThermalImagePushButton = new QPushButton(imageColorAdjustmentsGroupBox);
        adjustThermalImagePushButton->setObjectName(QStringLiteral("adjustThermalImagePushButton"));

        verticalLayout_2->addWidget(adjustThermalImagePushButton);


        verticalLayout_4->addWidget(imageColorAdjustmentsGroupBox);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_7 = new QVBoxLayout(tab_2);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        groupBox = new QGroupBox(tab_2);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        formLayout = new QFormLayout(groupBox);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QStringLiteral("label_13"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_13);

        saveAllModalities = new QCheckBox(groupBox);
        saveAllModalities->setObjectName(QStringLiteral("saveAllModalities"));
        saveAllModalities->setChecked(true);

        formLayout->setWidget(0, QFormLayout::FieldRole, saveAllModalities);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        borderThickness = new QSpinBox(groupBox);
        borderThickness->setObjectName(QStringLiteral("borderThickness"));

        formLayout->setWidget(1, QFormLayout::FieldRole, borderThickness);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_7);

        btnColor = new QPushButton(groupBox);
        btnColor->setObjectName(QStringLiteral("btnColor"));

        formLayout->setWidget(2, QFormLayout::FieldRole, btnColor);

        label_14 = new QLabel(groupBox);
        label_14->setObjectName(QStringLiteral("label_14"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_14);

        overlayOpacitySpinBox = new QDoubleSpinBox(groupBox);
        overlayOpacitySpinBox->setObjectName(QStringLiteral("overlayOpacitySpinBox"));
        overlayOpacitySpinBox->setDecimals(0);
        overlayOpacitySpinBox->setMaximum(100);
        overlayOpacitySpinBox->setSingleStep(5);
        overlayOpacitySpinBox->setValue(30);

        formLayout->setWidget(3, QFormLayout::FieldRole, overlayOpacitySpinBox);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_8);

        noiseBlobSize = new QSpinBox(groupBox);
        noiseBlobSize->setObjectName(QStringLiteral("noiseBlobSize"));
        noiseBlobSize->setMaximum(1000000);
        noiseBlobSize->setSingleStep(5);
        noiseBlobSize->setValue(20);

        formLayout->setWidget(4, QFormLayout::FieldRole, noiseBlobSize);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_9);

        holeFillSize = new QSpinBox(groupBox);
        holeFillSize->setObjectName(QStringLiteral("holeFillSize"));
        holeFillSize->setMaximum(1000000);
        holeFillSize->setSingleStep(5);
        holeFillSize->setValue(20);

        formLayout->setWidget(5, QFormLayout::FieldRole, holeFillSize);

        label_12 = new QLabel(groupBox);
        label_12->setObjectName(QStringLiteral("label_12"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_12);

        autoBorderWhenDrawing = new QCheckBox(groupBox);
        autoBorderWhenDrawing->setObjectName(QStringLiteral("autoBorderWhenDrawing"));

        formLayout->setWidget(6, QFormLayout::FieldRole, autoBorderWhenDrawing);

        label_15 = new QLabel(groupBox);
        label_15->setObjectName(QStringLiteral("label_15"));

        formLayout->setWidget(7, QFormLayout::LabelRole, label_15);

        autoBorderWhenSwitching = new QCheckBox(groupBox);
        autoBorderWhenSwitching->setObjectName(QStringLiteral("autoBorderWhenSwitching"));

        formLayout->setWidget(7, QFormLayout::FieldRole, autoBorderWhenSwitching);


        verticalLayout_7->addWidget(groupBox);

        groupBox_8 = new QGroupBox(tab_2);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        verticalLayout_5 = new QVBoxLayout(groupBox_8);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        limitTagsCheckBox = new QCheckBox(groupBox_8);
        limitTagsCheckBox->setObjectName(QStringLiteral("limitTagsCheckBox"));

        verticalLayout_5->addWidget(limitTagsCheckBox);


        verticalLayout_7->addWidget(groupBox_8);

        verticalSpacer = new QSpacerItem(20, 75, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_3 = new QVBoxLayout(tab_3);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBox_2 = new QGroupBox(tab_3);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        formLayout_2 = new QFormLayout(groupBox_2);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_2);

        rgbPattern = new QLineEdit(groupBox_2);
        rgbPattern->setObjectName(QStringLiteral("rgbPattern"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, rgbPattern);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_3);

        depthPattern = new QLineEdit(groupBox_2);
        depthPattern->setObjectName(QStringLiteral("depthPattern"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, depthPattern);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_4);

        thermalPattern = new QLineEdit(groupBox_2);
        thermalPattern->setObjectName(QStringLiteral("thermalPattern"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, thermalPattern);


        verticalLayout_3->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(tab_3);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        formLayout_3 = new QFormLayout(groupBox_3);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_5);

        csvPath = new QLineEdit(groupBox_3);
        csvPath->setObjectName(QStringLiteral("csvPath"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, csvPath);

        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, label_6);

        rgbMaskFolder = new QLineEdit(groupBox_3);
        rgbMaskFolder->setObjectName(QStringLiteral("rgbMaskFolder"));

        formLayout_3->setWidget(1, QFormLayout::FieldRole, rgbMaskFolder);

        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout_3->setWidget(2, QFormLayout::LabelRole, label_10);

        depthMaskFolder = new QLineEdit(groupBox_3);
        depthMaskFolder->setObjectName(QStringLiteral("depthMaskFolder"));

        formLayout_3->setWidget(2, QFormLayout::FieldRole, depthMaskFolder);

        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout_3->setWidget(3, QFormLayout::LabelRole, label_11);

        thermalMaskFolder = new QLineEdit(groupBox_3);
        thermalMaskFolder->setObjectName(QStringLiteral("thermalMaskFolder"));

        formLayout_3->setWidget(3, QFormLayout::FieldRole, thermalMaskFolder);


        verticalLayout_3->addWidget(groupBox_3);

        tabWidget->addTab(tab_3, QString());

        verticalLayout_6->addWidget(tabWidget);

        buttonBox = new QDialogButtonBox(SettingsDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Save);

        verticalLayout_6->addWidget(buttonBox);


        retranslateUi(SettingsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SettingsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SettingsDialog, SLOT(reject()));

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
        SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "Settings", 0));
        groupBox_4->setTitle(QApplication::translate("SettingsDialog", "Modalities", 0));
        enableRgbCheckBox->setText(QApplication::translate("SettingsDialog", "Enable RGB", 0));
        enableDepthCheckBox->setText(QApplication::translate("SettingsDialog", "Enable Depth", 0));
        enableThermalCheckBox->setText(QApplication::translate("SettingsDialog", "Enable Thermal", 0));
        groupBox_5->setTitle(QApplication::translate("SettingsDialog", "Registration", 0));
        usePlanarHomRadioButton->setText(QApplication::translate("SettingsDialog", "Use planar homography", 0));
        useMultipleHomRadioButton->setText(QApplication::translate("SettingsDialog", "Use multiple homographies", 0));
        groupBox_6->setTitle(QApplication::translate("SettingsDialog", "Don't care mask", 0));
        label_16->setText(QApplication::translate("SettingsDialog", "Use global don't care mask:", 0));
        useDontCareMask->setText(QString());
        label_17->setText(QApplication::translate("SettingsDialog", "Overlay color:", 0));
        btnDontCareColor->setText(QString());
        label_18->setText(QApplication::translate("SettingsDialog", "Overlay opacity:", 0));
        dontCareOverlayOpacitySpinBox->setSuffix(QApplication::translate("SettingsDialog", " %", 0));
        imageColorAdjustmentsGroupBox->setTitle(QApplication::translate("SettingsDialog", "Image color adjustments", 0));
        adjustThermalImagePushButton->setText(QApplication::translate("SettingsDialog", "Adjust thermal image", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("SettingsDialog", "General", 0));
        groupBox->setTitle(QApplication::translate("SettingsDialog", "Annotations", 0));
        label_13->setText(QApplication::translate("SettingsDialog", "Save annotations for depth and thermal:", 0));
        saveAllModalities->setText(QString());
        label->setText(QApplication::translate("SettingsDialog", "Automatic border width:", 0));
        label_7->setText(QApplication::translate("SettingsDialog", "Overlay starting color:", 0));
        btnColor->setText(QString());
        label_14->setText(QApplication::translate("SettingsDialog", "Overlay opacity:", 0));
        overlayOpacitySpinBox->setSuffix(QApplication::translate("SettingsDialog", " %", 0));
        label_8->setText(QApplication::translate("SettingsDialog", "Noise removal blob size:", 0));
        label_9->setText(QApplication::translate("SettingsDialog", "Max. hole size for fill:", 0));
        label_12->setText(QApplication::translate("SettingsDialog", "Add annotation borders when drawing:", 0));
        autoBorderWhenDrawing->setText(QString());
        label_15->setText(QApplication::translate("SettingsDialog", "Add annotation borders when switching:", 0));
        autoBorderWhenSwitching->setText(QString());
        groupBox_8->setTitle(QApplication::translate("SettingsDialog", "Annotation properties", 0));
        limitTagsCheckBox->setText(QApplication::translate("SettingsDialog", "Limit annotation tags to suggested list", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("SettingsDialog", "Annotations", 0));
        groupBox_2->setTitle(QApplication::translate("SettingsDialog", "Image file patterns", 0));
        label_2->setText(QApplication::translate("SettingsDialog", "RGB images:", 0));
        label_3->setText(QApplication::translate("SettingsDialog", "Depth images:", 0));
        label_4->setText(QApplication::translate("SettingsDialog", "Thermal images:", 0));
        groupBox_3->setTitle(QApplication::translate("SettingsDialog", "Saving", 0));
        label_5->setText(QApplication::translate("SettingsDialog", "Output filename (csv):", 0));
        label_6->setText(QApplication::translate("SettingsDialog", "Folder for RGB masks:", 0));
        label_10->setText(QApplication::translate("SettingsDialog", "Folder for depth masks:", 0));
        label_11->setText(QApplication::translate("SettingsDialog", "Folder for thermal masks:", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("SettingsDialog", "File patterns", 0));
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDIALOG_H
