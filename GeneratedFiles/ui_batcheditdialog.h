/********************************************************************************
** Form generated from reading UI file 'batcheditdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BATCHEDITDIALOG_H
#define UI_BATCHEDITDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_BatchEditDialog
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QTableWidget *filterTable;
    QLabel *label_2;
    QTableWidget *annotationTable;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnPerformChanges;
    QPushButton *btnClose;

    void setupUi(QDialog *BatchEditDialog)
    {
        if (BatchEditDialog->objectName().isEmpty())
            BatchEditDialog->setObjectName(QStringLiteral("BatchEditDialog"));
        BatchEditDialog->resize(1208, 743);
        formLayout = new QFormLayout(BatchEditDialog);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(BatchEditDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        filterTable = new QTableWidget(BatchEditDialog);
        filterTable->setObjectName(QStringLiteral("filterTable"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(filterTable->sizePolicy().hasHeightForWidth());
        filterTable->setSizePolicy(sizePolicy);
        filterTable->setRowCount(0);

        formLayout->setWidget(0, QFormLayout::FieldRole, filterTable);

        label_2 = new QLabel(BatchEditDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        annotationTable = new QTableWidget(BatchEditDialog);
        annotationTable->setObjectName(QStringLiteral("annotationTable"));

        formLayout->setWidget(1, QFormLayout::FieldRole, annotationTable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btnPerformChanges = new QPushButton(BatchEditDialog);
        btnPerformChanges->setObjectName(QStringLiteral("btnPerformChanges"));

        horizontalLayout->addWidget(btnPerformChanges);

        btnClose = new QPushButton(BatchEditDialog);
        btnClose->setObjectName(QStringLiteral("btnClose"));

        horizontalLayout->addWidget(btnClose);


        formLayout->setLayout(2, QFormLayout::SpanningRole, horizontalLayout);


        retranslateUi(BatchEditDialog);

        QMetaObject::connectSlotsByName(BatchEditDialog);
    } // setupUi

    void retranslateUi(QDialog *BatchEditDialog)
    {
        BatchEditDialog->setWindowTitle(QApplication::translate("BatchEditDialog", "Batch edit annotations", 0));
        label->setText(QApplication::translate("BatchEditDialog", "Filter:", 0));
        label_2->setText(QApplication::translate("BatchEditDialog", "Annotations:", 0));
        btnPerformChanges->setText(QApplication::translate("BatchEditDialog", "Perform changes", 0));
        btnClose->setText(QApplication::translate("BatchEditDialog", "&Close", 0));
    } // retranslateUi

};

namespace Ui {
    class BatchEditDialog: public Ui_BatchEditDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BATCHEDITDIALOG_H
