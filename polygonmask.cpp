#include "polygonmask.h"

PolygonMask::PolygonMask(cv::Mat mask, int proximityThreshold)
{
	this->proximityThreshold = proximityThreshold;
	this->inputMask = mask;
	cv::Mat binaryImg;

	threshold(mask, binaryImg, 0, 255, cv::THRESH_BINARY);
	cv::findContours(binaryImg, contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

	if (!contours.empty())
	{
		currentContourIndex = 0;
	}
	else {
		currentContourIndex = -1;
	}

	moveOperationStarted = false;

	
	currentInnerIndex = 0;
}

PolygonMask::PolygonMask(cv::Mat mask, QString compactPolygon, int proximityThreshold)
{
	this->proximityThreshold = proximityThreshold;
	this->inputMask = mask;

	// Populate the contours from the string
	contours = getContoursFromCompactPolygon(compactPolygon);
	currentContourIndex = -1;

	if (contours.size() > 0)
	{
		currentContourIndex = 0;
	}

	currentInnerIndex = 0;
	moveOperationStarted = false;
}

std::vector<std::vector<cv::Point>> PolygonMask::getContoursFromCompactPolygon(QString compactPolygon)
{
	std::vector<std::vector<cv::Point>> contours;
	
	auto outerContours = compactPolygon.split(',');

	for (auto& outerContour : outerContours)
	{
		auto points = outerContour.split(' ');
		std::vector<cv::Point> contour;

		for (auto i = 0; i < points.size(); i = i + 2)
		{
			if (points.size() >(i + 1))
			{
				contour.push_back(cv::Point(points[i].toInt(), points[i + 1].toInt()));
			}
		}

		contours.push_back(contour);

	}

	return contours;
}


void PolygonMask::addPoint(cv::Point point)
{
	if (contours.size() > currentContourIndex && !contours.empty())
	{
		if (currentInnerIndex < 0 || currentInnerIndex >= contours[currentContourIndex].size())
		{
			currentInnerIndex = 0;
		}
		
		// Add to existing contour
		contours[currentContourIndex].insert(contours[currentContourIndex].begin() + currentInnerIndex + 1, point);
	} 
	else {
		// Create a entirely new contour
		contours.push_back(std::vector<cv::Point>{point});
		currentContourIndex = 0;
	}

	
	currentInnerIndex += 1;
}

void PolygonMask::removePoint(cv::Point point)
{
	// Find the point in the contour that is nearby 
	// the requested point
	int bestOuterIndex = -1;
	int bestInnerIndex = -1;

	getClosestContourPoint(point, bestOuterIndex, bestInnerIndex);

	if (bestOuterIndex >= 0)
	{
		// Now remove it
		currentContourIndex = bestOuterIndex;
		contours[bestOuterIndex].erase(contours[bestOuterIndex].begin() + bestInnerIndex);
		moveOperationStarted = false;
	}

}

void PolygonMask::removeCurrentPoint()
{
	if (contours.size() > currentContourIndex && !contours.empty() && contours[currentContourIndex].size() > currentInnerIndex)
	{
		contours[currentContourIndex].erase(contours[currentContourIndex].begin() + currentInnerIndex);
		currentInnerIndex--;
	}
}

void PolygonMask::movePoint(cv::Point point)
{
	if (!moveOperationStarted)
	{
		int bestOuterIndex = -1;
		int bestInnerIndex = -1;

 		getClosestContourPoint(point, bestOuterIndex, bestInnerIndex);

		if (bestOuterIndex >= 0 && bestInnerIndex >= 0)
		{
			moveOuterIndex = bestOuterIndex;
			currentContourIndex = bestOuterIndex;
			currentInnerIndex = bestInnerIndex;
			moveInnerIndex = bestInnerIndex;
			moveOperationStarted = true;
		}
	}

	if (moveOuterIndex >= 0 )
	{
		contours[moveOuterIndex][moveInnerIndex] = point;
	}
}

void PolygonMask::moveCurrentPoint(int vertical, int horizontal)
{
	if (contours.size() > currentContourIndex && contours[currentContourIndex].size() > currentInnerIndex) {
		// Draw current point
		cv::Point current = contours[currentContourIndex][currentInnerIndex];
		
		current.x += horizontal;
		current.y += vertical;

		contours[currentContourIndex][currentInnerIndex] = current;
	}

}

void PolygonMask::moveEntirePolygon(int vertical, int horizontal)
{
	for (auto& contour : contours)
	{
		for (auto& point : contour)
		{
			point.x += horizontal;
			point.y += vertical;
		}
	}
}

void PolygonMask::changeCurrentPoint(int direction)
{
	if (contours.size() > currentContourIndex && contours[currentContourIndex].size() > currentInnerIndex) {
		currentInnerIndex += direction;

		if (currentInnerIndex < 0)
		{
			// Wrap
			currentInnerIndex = contours[currentContourIndex].size() - 1;
		}

		if (currentInnerIndex >= contours[currentContourIndex].size())
		{
			// Wrap the other way
			currentInnerIndex = 0;
		}
	}
}

std::vector<std::vector<cv::Point>> PolygonMask::getContours()
{
	return contours;
}

cv::Rect PolygonMask::getBoundingBox()
{
	return getBoundingBox(contours);
}

cv::Rect PolygonMask::getBoundingBox(QString compactPolygon)
{
	std::vector<std::vector<cv::Point> > contours = getContoursFromCompactPolygon(compactPolygon);

	return getBoundingBox(contours);
}

cv::Rect PolygonMask::getBoundingBox(std::vector<std::vector<cv::Point>> polygon)
{
	cv::Point2i topLeft = cv::Point2i(std::numeric_limits<int>::max(), std::numeric_limits<int>::max()); // Default-place this in the lower right corner, the exact opposite of top-left
	cv::Point2i bottomRight = cv::Point2i(0, 0); // And the exact opposite

	for (auto& contour : polygon)
	{
		cv::Rect tmpRect = cv::boundingRect(contour);

		if (tmpRect.tl().x < topLeft.x)
		{
			topLeft.x = tmpRect.x;
		}

		if (tmpRect.tl().y < topLeft.y)
		{
			topLeft.y = tmpRect.y;
		}

		if (tmpRect.br().x > bottomRight.x)
		{
			bottomRight.x = tmpRect.br().x;
		}

		if (tmpRect.br().y > bottomRight.y)
		{
			bottomRight.y = tmpRect.br().y;
		}
	}

	return cv::Rect(topLeft, bottomRight);
}

void PolygonMask::stopMovePoint()
{
	moveOperationStarted = false;
}

QString PolygonMask::getCompactPolygon()
{
	QStringList compactPolygon;

	for (auto& outerContour : contours)
	{
		QStringList compactOuterContour;

		for (auto& innerContour : outerContour)
		{
			compactOuterContour << QString::number(innerContour.x);
			compactOuterContour << QString::number(innerContour.y);
		}

		compactPolygon.append(compactOuterContour.join(' '));
	}

	return compactPolygon.join(',');
}

cv::Mat PolygonMask::getMask(cv::Scalar maskColor)
{
	cv::Mat outputMask = cv::Mat::zeros(inputMask.size(), inputMask.type());

	cv::drawContours(outputMask, contours, -1, maskColor, -1);

	return outputMask;
}

cv::Mat PolygonMask::getOutline(cv::Scalar maskColor, int lineThickness)
{
	cv::Mat outputMask = cv::Mat::zeros(inputMask.size(), inputMask.type());
	cv::drawContours(outputMask, contours, -1, maskColor, lineThickness);	

	return outputMask;
}

void PolygonMask::drawPoints(cv::Mat &mask, cv::Scalar maskColor, int circleRadius, int lineThickness)
{
	assert(mask.size() == inputMask.size());
	
	if (moveOperationStarted && moveOuterIndex >= 0 && moveOuterIndex < contours.size() &&
		moveInnerIndex >= 0 && moveInnerIndex < contours[moveOuterIndex].size())
	{
		cv::Point currentPoint = contours[moveOuterIndex][moveInnerIndex];
		cv::drawMarker(mask, currentPoint, cv::Scalar(255,255,255), cv::MarkerTypes::MARKER_CROSS, 4, lineThickness);
	}
	else if (contours.size() > currentContourIndex && contours[currentContourIndex].size() > currentInnerIndex){
		// Draw current point
		cv::Point current = contours[currentContourIndex][currentInnerIndex];
		cv::drawMarker(mask, current, cv::Scalar(255, 255, 255), cv::MarkerTypes::MARKER_CROSS, 4, lineThickness);
	}

	
	for (auto& innerContour : contours)
	{
		for (auto& point : innerContour)
		{
			cv::drawMarker(mask, point, maskColor, cv::MarkerTypes::MARKER_CROSS, 2, lineThickness);
			//cv::circle(mask, point, circleRadius, maskColor, 1); 
		}
	}
}


void PolygonMask::getClosestContourPoint(cv::Point point, int& bestOuterIndex, int& bestInnerIndex)
{
	float bestProximity = proximityThreshold + 1;
	bestOuterIndex = -1;
	bestInnerIndex = -1;

	for (auto i = 0; i < contours.size(); ++i)
	{
		for (auto j = 0; j < contours[i].size(); ++j)
		{
			auto dist = cv::norm(contours[i][j] - point);

			if (dist < bestProximity)
			{
				bestOuterIndex = i;
				bestInnerIndex = j;
				bestProximity = dist;
			}
		}
	}
}
