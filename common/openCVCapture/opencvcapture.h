#ifndef OPENCVCAPTURE_H
#define OPENCVCAPTURE_H

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <math.h>


/**
 * Class to capture frames or single images using OpenCV.
 *
 * This class integrates OpenCV and Qt. It is a class that handles both the capture of
 * video and single images. It works well with OpenCVViewer, which can display OpenCV
 * images.
 *
 * Each time a new frame is captured, the signal frameCaptured() is emitted so the
 * image can be accessed and processed or displayed elsewhere. A simple usage would be
 * to connect OpenCVCapture's frameCaptured() signal with OpenCVViewer's setImage()
 * slot to show new frames as soon as they are obtained.
 */
class OpenCVCapture : public QObject
{
    Q_OBJECT

public:
	/**
	 * Constructor.
	 *
     * Creates a new OpenCVCapture. Be aware that the capture source must be
	 * set before any capture can commence. See setSource().
	 */
    OpenCVCapture();
    ~OpenCVCapture();

	/**
	 * Start a capture.
	 *
	 * When the capture source is set, use this method to begin capturing from
	 * a video source using OpenCV's VideoCapture. Whenever a new frame is
	 * captured, the signal frameCaptured() is emitted.
	 */
    virtual void startCapture();

	/**
	 * Start a capture while setting the source and frame rate.
	 *
	 * An overloaded version of startCapture() that allows you to set the source
	 * and frame rate while starting the capture.
	 *
	 * @param source the source identifier (see setSource()).
     * @param frameRate the desired frame rate. If frameRate is < 0 the program will attempt to determine the frame rate automatically from the input file. If frameRate is == 0, no timer will be started. Instead, use advanceCapture() to manually frab the frames.
	 */
    void startCapture(QString source, int frameRate = -1);

	/**
	 * Set the video source.
	 *
	 * Usually a file name of the video you want to process. If only an integer is
	 * provided, the source is the connected camera with that number.
	 */
	void setSource(QString source);

	/**
	 * Return the capture source.
	 *
	 * @return the string defining the current capture source.
	 */
	QString getSource();

	/**
	 * Set desired frame rate.
	 *
	 * @param frameRate the desired frame rate. If frameRate is < 0 the program will attempt to determine the frame rate automatically from the input file.
	 */
	void setFrameRate(int rate);

	/**
	 * Retrive the set frame rate.
	 *
	 * Note that this <b>does not</b> return the frame rate the camera or video source
     * uses internally, only the frame rate this object will try to obtain pictures
	 * with.
	 *
	 * @return the current frame rate of the capture timer. If frameRate is < 0 the program will attempt to determine the frame rate automatically from the input file.
	 */
	int getFrameRate();

	/**
	 * Stop the capture.
	 *
	 * This stops the internal capture timer and releases the VideoCapture object. The
	 * current image will stay in memory until overwritten when a capture is started again.
	 */
	void stopCapture();

	/**
	 * Pause the capture.
	 *
	 * Pause the internal capture timer.
	 */
	void pauseCapture();

	/**
	 * Restart the capture.
	 *
	 * Resume the internal capture timer. For video input, this resumes where the video
	 * was paused, for live camera input, this resumes at the current frame.
	 */
	void resumeCapture();

	/**
	 * Manually read next frame in a stream.
	 *
	 * If no timer is set, this function can be used to manually read the next frame.
	 * This functions has no effect unless the frame rate is set to 0.
	 */
	void advanceCapture();

	/**
	 * Manually grab next frame.
	 *
	 * Use to seek to a specific point in a video (when setProperty() cannot be used
     * due to bug 1419 in OpenCV). Remember that this does not retrieve the frame,
	 * so be sure to call advanceCapture() after calling this.
     * This function has no effect unless the frame rate is set to 0.
	 */
	void grab();

	/**
	 * Load a single image.
	 *
	 * Instead of using OpenCV's VideoCapture to capture a stream of images, this method
	 * uses imread() to load a single image. The image stays in memory until overwritten
     * by a new capture or a new image.
     *
     * @param loadFlag passed directly through to OpenCV's imread().
	 */
	void loadImage(int loadFlag = 1);

	/**
	 * Load a single image from the specified source.
	 *
	 * Overloaded version of loadImage(). Allows you to pass the file name in and load
	 * it in a single step.
	 *
     * @param source the name of the file to load. This will be set as the capture source.
     * @param loadFlag passed directly through to OpenCV's imread().
	 */
    void loadImage(QString source, int loadFlag = 1);

	/**
	 * Return the current image.
	 *
	 * This function returns the current image for further processing elsewhere. Note that
     * this is a shallow copy (as per OpenCV's memory management).
	 *
	 * @return a shallow copy of the moest recently capture image.
	 */
	cv::Mat getImage();

	/**
	 * Get a property
	 *
	 * Get any property from the capture device/file, as specified in
     * http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-get
	 *
	 * @param the desired property.
	 *
	 * @return the value of the requested property.
	 */
	double getCaptureProperty(int propId);

	/**
	 * Set a property
	 *
	 * Set any property from the capture device/file, as specified in
     * http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-set
	 *
	 * @param the desired property.
	 * @param the desired value.
	 *
	 * @return the result as returned by OpenCV.
	 */
	bool setCaptureProperty(int propId, double value);

	/**
	 * Tell whether a video capture is opened.
	 *
	 * @return true if a capture is opened (it may be paused), false if not.
	 */
	bool isOpened();

    /**
     * Tell whether a video capture is stopped.
     *
     * @return true if a capture is stopped (either from stopCapture() or from never having started), false if the capture is running or simply paused.
     */
    bool isStopped();

    /**
     * Tell whether a video capture is paused.
     *
     * @return true if a capture is paused, false if the capture is running or stopped.
     */
    bool isPaused();

    /**
     * Return the frame number of the current frame.
     *
     * @return 0-indexed number of the current frame.
     */
    int getFrameNumber();

    /**
     * Go to a specific frame.
     *
     * Jumps to a specific frame in the video. If frameNum is larger than the number of frames in the video, it jumps to the last frame. Handles keyframe-issues automatically.
     *
     * @param the frame to jump to.
     *
     * @return the frame number the capture is at after the jump.
     */
    int jumpToFrame(int frameNum);

protected:
	QTimer *timer; ///< Timer used for setting capture frame rate.
	cv::VideoCapture capture; ///< Capture object for video.
    cv::Mat image; ///< Holds the most recently captured image.
    cv::Mat firstFrame; ///< Holds the first frame to stop oni-files from looping.
    QString captureSource; ///< Specifies the source identifier/file name.
    int frameRate; ///< Holds the frame rate that controls the timer.
    int frameNumber;

protected slots:
	void captureFrame(); ///< Captures a new frame when the timer asks it to.

signals:
    void frameCaptured(cv::Mat image); ///< Emitted each time a new frame is captured.
};

#endif
