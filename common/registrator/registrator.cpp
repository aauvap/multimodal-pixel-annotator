// <copyright file="registrator.cpp" company="Aalborg University">
// Copyright (c) 2014 All Right Reserved, http://vap.aau.dk/
//
// This source is subject to the MIT Licence
// Copyright (c) <2014> <Aalborg University>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// </copyright>
// <author>Chris Bahnsen</author>
// <email>cb@create.aau.dk</email>
// <email2>chris.bahnsen@hotmail.com</email2>
// <date>2014-11-10</date>
// <summary>Contains the base functions for registering trimodal imagery by use of a calibVars.yml function</summary>

#include "registrator.h"

Registrator::Registrator()
{
    // Initialize variables where no sensible default exists
    stereoCalibParam.defaultDepth = 0;
    stereoCalibParam.depthCoeffA = 0;
    stereoCalibParam.depthCoeffB = 0;
    stereoCalibParam.HEIGHT = 0;
    stereoCalibParam.nbrHom = 0;
    stereoCalibParam.WIDTH = 0;

    settings.isInitialized = false;

}

float Registrator::backProjectPoint(float point, float focalLength, float principalPoint, float zCoord)
{
    float projectedPoint;

    projectedPoint = (point * zCoord - principalPoint * zCoord) / (focalLength);
    return projectedPoint;
}

float Registrator::forwardProjectPoint(float point, float focalLength, float principalPoint, float zCoord)
{
    float projectedPoint;

    projectedPoint = (point * focalLength) / zCoord + principalPoint;
    return projectedPoint;
}

float Registrator::lookUpDepth(cv::Mat depthImg, cv::Point2f dCoord, bool SCALE_TO_THEORETICAL)
{
    float depthInMm = 0;
    // Look up the 'vanilla' depth in the depth image


    if (!depthImg.empty() && (dCoord.y > 0 && dCoord.y < depthImg.rows
        && dCoord.x > 0 && dCoord.x < depthImg.cols)) {
        depthInMm = depthImg.at<unsigned short>(int(dCoord.y - 1), int(dCoord.x - 1))*0.1;
    }
    else {
        depthInMm = stereoCalibParam.defaultDepth;
    }

    if (SCALE_TO_THEORETICAL)
    {
        // Transfer the depth to the "theoretical" depth range as provided by the camera calibration
        depthInMm = stereoCalibParam.depthCoeffA*depthInMm + stereoCalibParam.depthCoeffB;
    }

    return depthInMm;
}

void Registrator::computeCorrespondingRgbPointFromDepth(std::vector<cv::Point2f> vecDCoord, std::vector<cv::Point2f> & vecRgbCoord)
{
    std::vector<cv::Point2f> vecDistRgbCoord, vecUndistRgbCoord, vecRecRgbCoord;
    cv::Point2f tmpPoint;
    cv::Point2f prevPoint;

    // Find the distorted rgb cv::Point
    for (auto i = 0; i < vecDCoord.size(); i++)
    {
        if ((vecDCoord[i].y > 0) && (vecDCoord[i].x > 0) && (vecDCoord[i].y <= stereoCalibParam.HEIGHT) && (vecDCoord[i].x <= stereoCalibParam.WIDTH)) {
            tmpPoint.x = stereoCalibParam.dToRgbCalX.at<short>(int(vecDCoord[i].y - 1), int(vecDCoord[i].x - 1)); // Look up the corresponding x-cv::Point in the depth image
            tmpPoint.y = stereoCalibParam.dToRgbCalY.at<short>(int(vecDCoord[i].y - 1), int(vecDCoord[i].x - 1)); // Look up the corresponding y-cv::Point in the depth image

            if ((tmpPoint.x == 0) || (tmpPoint.y == 480) || (tmpPoint.y == 0))
            {
                // If the cv::Point is inside the frame of the depth camera, look up neighbouring Points and perform bilinear interpolation
                cv::Point2f interpolPoint(0, 0), tmpLookupPoint;;
                int lookupCount = 0, xOffset, yOffset;

                for (int j = 0; j < 4; ++j) {

                    switch (j) {
                    case 0:
                        xOffset = -1;
                        yOffset = -1;
                        break;
                    case 1:
                        xOffset = +1;
                        yOffset = -1;
                        break;
                    case 2:
                        xOffset = +1;
                        yOffset = +1;
                        break;
                    case 3:
                        xOffset = -1;
                        yOffset = +1;
                        break;
                    }

                    if (((vecDCoord[i].y + yOffset) > 0) && ((vecDCoord[i].x + xOffset) > 0) && ((vecDCoord[i].y + yOffset) <= stereoCalibParam.HEIGHT)
                        && ((vecDCoord[i].x + xOffset) <= stereoCalibParam.WIDTH)) {
                        tmpLookupPoint.x = stereoCalibParam.dToRgbCalX.at<short>(int(vecDCoord[i].y - 1 + yOffset), int(vecDCoord[i].x - 1 + xOffset)); // Look up the corresponding x-cv::Point in the depth image
                        tmpLookupPoint.y = stereoCalibParam.dToRgbCalY.at<short>(int(vecDCoord[i].y - 1 + yOffset), int(vecDCoord[i].x - 1 + xOffset)); // Look up the corresponding y-cv::Point in the depth image

                        if ((tmpLookupPoint.x != 0) && (tmpLookupPoint.y != 480) && (tmpLookupPoint.y != 0)) {
                            interpolPoint.x = interpolPoint.x + tmpLookupPoint.x;
                            interpolPoint.y = interpolPoint.y + tmpLookupPoint.y;
                            lookupCount++;
                        }
                    }
                }

                if (lookupCount > 0) {
                    tmpPoint.x = interpolPoint.x / lookupCount;
                    tmpPoint.y = interpolPoint.y / lookupCount;
                }
                else if (this->settings.USE_PREV_DEPTH_POINT)
                {
                    tmpPoint = prevPoint;
                }
                else {
                    tmpPoint = cv::Point2f(0, 0);
                }
            }
            vecDistRgbCoord.push_back(tmpPoint);
        }
        else {
            tmpPoint.x = 1; tmpPoint.y = 1;
            vecDistRgbCoord.push_back(tmpPoint);
        }
    }

    if (settings.UNDISTORT_IMAGES) {
        undistortPoints(vecDistRgbCoord, vecUndistRgbCoord, stereoCalibParam.rgbCamMat, stereoCalibParam.rgbDistCoeff,
            cv::Mat::eye(3, 3, CV_32F), stereoCalibParam.rgbCamMat);
    }

    if (settings.UNDISTORT_IMAGES) {
        // If images are undistorted, but not rectified, use undistorted coordinates
        vecRgbCoord = vecUndistRgbCoord;
    }
    else {
        // Otherwise, use the distorted coordinates
        vecRgbCoord = vecDistRgbCoord;
    }

}

void Registrator::computeCorrespondingDepthPointFromRgb(std::vector<cv::Point2f> vecRgbCoord, std::vector<cv::Point2f> & vecDCoord)
{
    std::vector<cv::Point2f> vecDistRgbCoord, vecUndistRgbCoord;
    cv::Point2f tmpPoint;
    cv::Point2f prevPoint(0, 0);

    // Now, we may re-distort the Points so they correspond to the original image
    if (settings.UNDISTORT_IMAGES) {
        // If we need to undistort the cv::Point, do so:
        MyDistortPoints(vecRgbCoord, vecDistRgbCoord, stereoCalibParam.rgbCamMat, stereoCalibParam.rgbDistCoeff);
    }
    else {
        // Otherwise, the cv::Point is already distorted
        vecDistRgbCoord = vecRgbCoord;
    }

	if ((stereoCalibParam.WIDTH <= 0) || (stereoCalibParam.HEIGHT <= 0)) {
		return;
	}


    if ((stereoCalibParam.rgbToDCalX.cols == stereoCalibParam.WIDTH) && (stereoCalibParam.rgbToDCalX.rows == stereoCalibParam.HEIGHT))
    {
        for (auto i = 0; i < vecDistRgbCoord.size(); i++)
        {
            if ((vecDistRgbCoord[i].y > 0) && (vecDistRgbCoord[i].x > 0) && (vecDistRgbCoord[i].y <= stereoCalibParam.HEIGHT)
                && (vecDistRgbCoord[i].x <= stereoCalibParam.WIDTH)) {
                tmpPoint.x = stereoCalibParam.rgbToDCalX.at<uint16_t>(int(vecDistRgbCoord[i].y - 1), int(vecDistRgbCoord[i].x - 1)); // Look up the corresponding x-cv::Point in the depth image
                tmpPoint.y = stereoCalibParam.rgbToDCalY.at<uint16_t>(int(vecDistRgbCoord[i].y - 1), int(vecDistRgbCoord[i].x - 1)); // Look up the corresponding y-cv::Point in the depth image

                if ((tmpPoint.x == 0) || (tmpPoint.y == 480) || (tmpPoint.y == 0)) {
                    // If the cv::Point is inside the frame of the RGB camera, look up neighbouring Points and perform bilinear interpolation
                    cv::Point2f interpolPoint(0, 0), tmpLookupPoint;;
                    int lookupCount = 0, xOffset, yOffset;

                    for (int j = 0; j < 4; ++j) {

                        switch (j) {
                        case 0:
                            xOffset = -1;
                            yOffset = -1;
                            break;
                        case 1:
                            xOffset = +1;
                            yOffset = -1;
                            break;
                        case 2:
                            xOffset = +1;
                            yOffset = +1;
                            break;
                        case 3:
                            xOffset = -1;
                            yOffset = +1;
                            break;
                        }

                        if (((vecDistRgbCoord[i].y + yOffset) > 0) && ((vecDistRgbCoord[i].x + xOffset) > 0) && ((vecDistRgbCoord[i].y + yOffset) <= stereoCalibParam.HEIGHT)
                            && ((vecDistRgbCoord[i].x + xOffset) <= stereoCalibParam.WIDTH)) {
                            tmpLookupPoint.x = stereoCalibParam.rgbToDCalX.at<short>(int(vecDistRgbCoord[i].y - 1 + yOffset), int(vecDistRgbCoord[i].x - 1 + xOffset)); // Look up the corresponding x-cv::Point in the depth image
                            tmpLookupPoint.y = stereoCalibParam.rgbToDCalY.at<short>(int(vecDistRgbCoord[i].y - 1 + yOffset), int(vecDistRgbCoord[i].x - 1 + xOffset)); // Look up the corresponding y-cv::Point in the depth image

                            if ((tmpLookupPoint.x != 0) && (tmpLookupPoint.y != 480) && (tmpLookupPoint.y != 0)) {
                                interpolPoint.x = interpolPoint.x + tmpLookupPoint.x;
                                interpolPoint.y = interpolPoint.y + tmpLookupPoint.y;
                                lookupCount++;
                            }
                        }
                    }

                    if (lookupCount > 0) {
                        tmpPoint.x = interpolPoint.x / lookupCount;
                        tmpPoint.y = interpolPoint.y / lookupCount;
                    }
                    else if (this->settings.USE_PREV_DEPTH_POINT)
                    {
                        tmpPoint = prevPoint;
                    }
                    else {
                        tmpPoint = cv::Point2f(0, 0);
                    }
                }
                vecDCoord.push_back(tmpPoint);
            }
            else {
                if (this->settings.USE_PREV_DEPTH_POINT)
                {
                    // If the RGB coordinates are not within the camera frame, return the previous value
                    vecDCoord.push_back(prevPoint);
                }
                else {
                    vecDCoord.push_back(cv::Point2f(0, 0));
                }
            }

            prevPoint = tmpPoint;
        }
    }

}


void Registrator::computeCorrespondingThermalPointFromRgb(std::vector<cv::Point2f> vecRgbCoord, std::vector<cv::Point2f>& vecTCoord, std::vector<cv::Point2f> vecDCoord)
{
    std::vector<int> vecDepthInMm, bestHom;
    std::vector<double> minDist;
    std::vector<std::vector<double> > octantDistances;
    std::vector<std::vector<int> > octantIndices;
    std::vector<cv::Point3f> worldCoordPointVector;
    computeCorrespondingThermalPointFromRgb(vecRgbCoord, vecTCoord, vecDCoord, vecDepthInMm, minDist,
        bestHom, octantIndices, octantDistances, worldCoordPointVector);

}

void Registrator::computeCorrespondingThermalPointFromRgb(std::vector<cv::Point2f> vecRgbCoord, std::vector<cv::Point2f>& vecTCoord, std::vector<cv::Point2f> vecDCoord,
    std::vector<int> &bestHom)
{
    std::vector<int> vecDepthInMm;
    std::vector<double> minDist;
    vecDepthInMm.resize(0);
    std::vector<std::vector<double> > octantDistances;
    std::vector<std::vector<int> > octantIndices;
    std::vector<cv::Point3f> worldCoordPointVector;
    computeCorrespondingThermalPointFromRgb(vecRgbCoord, vecTCoord, vecDCoord, vecDepthInMm, minDist,
        bestHom, octantIndices, octantDistances, worldCoordPointVector);
}

void Registrator::computeCorrespondingThermalPointFromRgb(std::vector<cv::Point2f> vecRgbCoord, std::vector<cv::Point2f>& vecTCoord, std::vector<cv::Point2f> vecDCoord,
    std::vector<int> vecDepthInMm, std::vector<double>& minDist, std::vector<int> &bestHom, std::vector<std::vector<int> > &octantIndices,
    std::vector<std::vector<double> > &octantDistances, std::vector<cv::Point3f> &worldCoordPointVector)
{
    std::vector<cv::Point2f> vecUndistRgbCoord, vecRecRgbCoord, vecDistRgbCoord, vecRecTCoord, vecUndistTCoord;
    cv::Point2f tmpPoint;
    minDist.clear(); bestHom.clear();

    if (!settings.UNDISTORT_IMAGES && (stereoCalibParam.rgbCamMat.size() == cv::Size(3, 3))) // If the images are not undistorted, produce undistorted coordinates
    {
        undistortPoints(vecRgbCoord, vecUndistRgbCoord, stereoCalibParam.rgbCamMat, stereoCalibParam.rgbDistCoeff,
            cv::Mat::eye(3, 3, CV_32F), stereoCalibParam.rgbCamMat);
        vecDistRgbCoord = vecRgbCoord;
    }
    else {
        vecUndistRgbCoord = vecRgbCoord;
    }

	switch (settings.registrationMethod)
	{
	case TrimodalMultipleHom:
	{
		computeHomographyMapping(vecUndistRgbCoord, vecUndistTCoord, vecDCoord, vecDepthInMm, minDist, bestHom,
			octantIndices, octantDistances, worldCoordPointVector);
		break;
	}
	case BimodalMultipleHom:
	{
		computeHomographyMapping(vecUndistRgbCoord, vecUndistTCoord);
		break;
	}
	case BimodalSingleHom:
		if (!stereoCalibParam.homRgbToT.empty()) {
			perspectiveTransform(vecUndistRgbCoord, vecUndistTCoord, stereoCalibParam.homRgbToT);
		}
		break;
	}

    // If needed, distort the coordinates
    if (!settings.UNDISTORT_IMAGES && (stereoCalibParam.rgbCamMat.size() == cv::Size(3, 3))) // If the images are distorted, produce distorted coordinates
    {
        MyDistortPoints(vecUndistTCoord, vecTCoord, stereoCalibParam.tCamMat, stereoCalibParam.tDistCoeff);
    }
    else {
        vecTCoord = vecUndistTCoord;
    }

}

void Registrator::computeCorrespondingRgbPointFromThermal(std::vector<cv::Point2f> vecTCoord, std::vector<cv::Point2f>& vecRgbCoord)
{
    std::vector<double> minDist;
    std::vector<int> bestHom;
    std::vector<std::vector<int> > octantIndices;
    std::vector<std::vector<double> > octantDistances;
    std::vector<cv::Point3f> worldCoordPointVector;

    computeCorrespondingRgbPointFromThermal(vecTCoord, vecRgbCoord, minDist, bestHom, octantIndices, octantDistances,
        worldCoordPointVector);


}

void Registrator::computeCorrespondingRgbPointFromThermal(std::vector<cv::Point2f> vecTCoord, std::vector<cv::Point2f>& vecRgbCoord, std::vector<double>& minDist,
    std::vector<int> &bestHom, std::vector<std::vector<int> > &octantIndices, std::vector<std::vector<double>> &octantDistances)
{
    std::vector<cv::Point3f> worldCoordPointVector;
    computeCorrespondingRgbPointFromThermal(vecTCoord, vecRgbCoord, minDist, bestHom, octantIndices, octantDistances,
        worldCoordPointVector);
}

void Registrator::computeCorrespondingRgbPointFromThermal(std::vector<cv::Point2f> vecTCoord, std::vector<cv::Point2f>& vecRgbCoord, std::vector<double>& minDist,
    std::vector<int> &bestHom, std::vector<std::vector<int> > &octantIndices, std::vector<std::vector<double> > &octantDistances,
    std::vector<cv::Point3f> &worldCoordPointVector)
{
    std::vector<cv::Point2f> vecUndistTCoord, vecRecTCoord, vecDistTCoord, vecRecRgbCoord, vecUndistRgbCoord, vecDCoord;
    cv::Point2f tmpPoint;
    minDist.clear(); bestHom.clear();

    if (!settings.UNDISTORT_IMAGES) // If the images are not undistorted, produce undistorted coordinates
    {
        undistortPoints(vecTCoord, vecUndistTCoord, stereoCalibParam.tCamMat, stereoCalibParam.tDistCoeff,
            cv::Mat::eye(3, 3, CV_32F), stereoCalibParam.tCamMat);
        vecDistTCoord = vecTCoord;
    }
    else {
        vecUndistTCoord = vecTCoord;
    }


	switch (settings.registrationMethod)
	{
	case TrimodalMultipleHom:
	{
		std::vector<int> vecDepthInMm;
		computeHomographyMapping(vecUndistRgbCoord, vecUndistTCoord, vecDCoord, vecDepthInMm, minDist, bestHom,
			octantIndices, octantDistances, worldCoordPointVector);
		break;
	}
	case BimodalMultipleHom:
	{
		computeHomographyMapping(vecUndistRgbCoord, vecUndistTCoord);
		break;
	}
	case BimodalSingleHom:
	{
		if (!stereoCalibParam.homTToRgb.empty()) {
			perspectiveTransform(vecUndistTCoord, vecUndistRgbCoord, stereoCalibParam.homTToRgb);
		}

		break;
	}
	}

    // If needed, distort the coordinates
    if (!settings.UNDISTORT_IMAGES) // If the images are distorted, produce distorted coordinates
    {
        MyDistortPoints(vecUndistRgbCoord, vecRgbCoord, stereoCalibParam.rgbCamMat, stereoCalibParam.rgbDistCoeff);
    }
    else {
        vecRgbCoord = vecUndistRgbCoord;
    }

}

void Registrator::computeHomographies(std::vector<cv::Point2f> cam1Points, std::vector<cv::Point2f> cam2Points, int nbrHom, std::vector<cv::Mat>& homCam1Cam2,
    std::vector<cv::Mat>& homCam2Cam1, std::vector<cv::Point2f>& homCentersCam1, std::vector<cv::Point2f>& homCentersCam2)
{
    // Normalize data before K-means
    cv::Mat meanMat, stdDevMatX, stdDevMatY;
    std::vector<double> xVec, yVec;
    //cv::Mat normPoints(cam1Points.size(), 1, CV_32FC2);
    std::vector<cv::Point2f> normPoints;


    for (int i = 0; i < cam1Points.size(); i++)
    {
        // OpenCV doesn't handle the computation of the standard deviation on Matrices or vectors
        xVec.push_back(cam1Points[i].x);
        yVec.push_back(cam1Points[i].y);
    }

    meanStdDev(xVec, meanMat, stdDevMatX);
    meanStdDev(yVec, meanMat, stdDevMatY);

    cv::Point2f tmpPoint;

    for (int i = 0; i < cam1Points.size(); i++)
    {
        tmpPoint.x = xVec[i] / stdDevMatX.at<double>(0);
        tmpPoint.y = yVec[i] / stdDevMatY.at<double>(0);
        normPoints.push_back(tmpPoint);
    }

    // Find clusters
    cv::Mat centers(cam1Points.size(), 1, CV_32FC2), labels;

    cv::kmeans(normPoints, nbrHom, labels,
        cv::TermCriteria(CV_TERMCRIT_EPS, 10, 0.5),
        100, cv::KMEANS_RANDOM_CENTERS, centers);


    std::vector< std::vector<cv::Point2f> > clusteredCam1Points, clusteredCam2Points;
    cv::Point3f tmp3dPoint;
    int currentCluster;

    clusteredCam1Points.resize(nbrHom);
    clusteredCam2Points.resize(nbrHom);

    // Group Points into those clusters assigned by k-means
    for (int i = 0; i < cam1Points.size(); i++)
    {
        currentCluster = labels.at<int>(i);
        clusteredCam1Points[currentCluster].push_back(cam1Points[i]);
        clusteredCam2Points[currentCluster].push_back(cam2Points[i]);
    }

    // Compute planar homographies from the assigned clusters
    std::vector<cv::Point2f> tmpTPoints, tmpRgbPoints;
    std::vector<cv::Point3f> maskedPointCloudRgb;

    cv::Point2f tmpImageCoordRgb, tmpImageCoordT;
    cv::Mat tmpHom, invHom;

    homCentersCam1.clear();
    homCentersCam2.clear();

    for (int i = 0; i < nbrHom; i++)
    {
        // Compute the homographies and push them into an array
        std::vector<uchar> mask; cv::Point2f meanCam1, meanCam2; int count;

        homCam1Cam2.push_back(cv::findHomography(clusteredCam1Points[i], clusteredCam2Points[i], CV_RANSAC, 3, mask));
        invert(homCam1Cam2[i], invHom, cv::DECOMP_SVD);
        homCam2Cam1.push_back(invHom);

        count = 0; meanCam1.x = 0; meanCam1.y = 0; meanCam2.x = 0, meanCam2.y = 0;

        // Go through the masks and find the centre of the homography plane
        for (int k = 0; k < mask.size(); k++)
        {
            if (mask[k] != 0)
            {
                count++;
                meanCam1.x = meanCam1.x + (clusteredCam1Points[i][k].x - meanCam1.x) * 1 / (count); // Iterative mean
                meanCam1.y = meanCam1.y + (clusteredCam1Points[i][k].y - meanCam1.y) * 1 / (count);

                meanCam2.x = meanCam2.x + (clusteredCam2Points[i][k].x - meanCam2.x) * 1 / (count); // Iterative mean
                meanCam2.y = meanCam2.y + (clusteredCam2Points[i][k].y - meanCam2.y) * 1 / (count);
            }
        }

        homCentersCam1.push_back(meanCam1);
        homCentersCam2.push_back(meanCam2);
    }
}

void Registrator::computeHomographyMapping(std::vector<cv::Point2f>& cam1Points, std::vector<cv::Point2f>& cam2Points)
{
    // Use bilinear interpolator

    double sqDist, minDistTmp, maxDistTmp;
    //cv::Scalar sumOfWeights;

    cv::Point tmpIdx;
    int MAP_TYPE = 0;
    int nbrCoordinates;
    int nbrClusters = stereoCalibParam.nbrHom;
    std::vector<cv::Point2f> sourcePoints;

    // Are we mapping RGB or thermal Points?
    if (cam1Points.size() > 0)
    {
        MAP_TYPE = 1;
        nbrCoordinates = int(cam1Points.size());
        cam2Points.clear();
        sourcePoints = cam1Points;
    }
    else if (cam2Points.size() > 0)
    {
        MAP_TYPE = 2;
        nbrCoordinates = int(cam2Points.size());
        cam1Points.clear();
        sourcePoints = cam2Points;
    }
    else
    {
        MAP_TYPE = 0;
        std::cout << "Error in computeHomographyMapping - no coordinates to map!";
        return;
    }

    for (int i = 0; i < nbrCoordinates; i++)
    {
        // Find the euclidean distances from the cv::Point to the homographies

        int bestHomTmp = 0; // Reset parameters
        double bestDist = 1e6;

        std::vector<double> homDist, homWeights;
        int errorCounter = 0;

        for (int j = 0; j < nbrClusters; j++)
        {
            double mahalDist;
            int windowRadius;
            cv::Mat depthCenter(1, 3, CV_32F, cv::Scalar(0));
            // Compute Mahalanobis distance, squared
            if (MAP_TYPE == 1)
            {
                homDist.push_back(sqrt(pow(sourcePoints[i].x - stereoCalibParam.homCentersCam1[j].x, 2) +
                    pow(sourcePoints[i].y - stereoCalibParam.homCentersCam1[j].y, 2)));  // Euclidean					
            }
            else {
                homDist.push_back(sqrt(pow(sourcePoints[i].x - stereoCalibParam.homCentersCam2[j].x, 2) +
                    pow(sourcePoints[i].y - stereoCalibParam.homCentersCam2[j].y, 2)));  // Euclidean
            }

            sqDist = homDist[j];
            if (sqDist < bestDist)
            {
                bestDist = sqDist;
                bestHomTmp = j;
            }

        }

        /* Compute the corresponding cv::Point in the other modality
        This includes the weighting of multiple homographies
        */
        std::vector<cv::Point2f> tmpSourcePoint, tmpEstimatedPoint;
        cv::Point2f estimatedPoint = { 0,0 };

        tmpSourcePoint.push_back(sourcePoints[i]);

        std::vector<int> quadrantIndicesTmp; std::vector<double> quadrantDistancesTmp;


        if (MAP_TYPE == 1) {
            bilinearInterpolator(sourcePoints[i], stereoCalibParam.homCentersCam1, homDist, homWeights,
                quadrantIndicesTmp, quadrantDistancesTmp);

            weightedHomographyMapper(tmpSourcePoint, tmpEstimatedPoint, stereoCalibParam.homCam1Cam2, homWeights);

        }
        else {
            bilinearInterpolator(sourcePoints[i], stereoCalibParam.homCentersCam2, homDist, homWeights,
                quadrantIndicesTmp, quadrantDistancesTmp);

            weightedHomographyMapper(tmpSourcePoint, tmpEstimatedPoint, stereoCalibParam.homCam2Cam1, homWeights);
        }

        estimatedPoint = tmpEstimatedPoint[0];

        if (MAP_TYPE == 1) {
            cam2Points.push_back(estimatedPoint);
        }
        else {
            cam1Points.push_back(estimatedPoint);
        }
    }
}


void Registrator::computeHomographyMapping(std::vector<cv::Point2f>& vecUndistRgbCoord, std::vector<cv::Point2f>& vecUndistTCoord, std::vector<cv::Point2f> vecDCoord,
    std::vector<int> vecDepthInMm, std::vector<double>& minDist, std::vector<int> &bestHom, std::vector<std::vector<int> > &octantIndices,
    std::vector<std::vector<double> > &octantDistances, std::vector<cv::Point3f> &worldCoordPointVector)
{
    // Use trilinearInterpolator

    double depthInMm, sqDist;
    cv::Scalar sumOfWeights;
    std::vector<double> homDist, homWeights;
    std::vector<cv::Point2f> tmpUndistRgbPoint, tmpUndistTPoint, tmpEstimatedPoint;
    cv::Point2f estimatedPoint;
    cv::Point tmpIdx;
    int MAP_TYPE = 0;
    int nbrCoordinates;
    int nbrClusters = int(stereoCalibParam.homDepthCentersRgb.size());
    int count = 0;
    int prevDepthInMm = stereoCalibParam.defaultDepth;

    // Are we mapping RGB or thermal Points?
    if (vecUndistRgbCoord.size() > 0)
    {
        MAP_TYPE = 1;
        nbrCoordinates = int(vecUndistRgbCoord.size());
        vecUndistTCoord.clear();
    }
    else if (vecUndistTCoord.size() > 0)
    {
        MAP_TYPE = 2;
        nbrCoordinates = int(vecUndistTCoord.size());
        vecUndistRgbCoord.clear();
    }
    else
    {
        MAP_TYPE = 0;
        std::cerr << "Error in computeHomographyMapping - no coordinates to map!";
        return;
    }

    for (int i = 0; i < nbrCoordinates; i++)
    {
        if (vecDepthInMm.size() == 0) // Do we need to look up the depth?
        {
            if (vecDCoord.size() > 0)  // Are we providing any depth coordinates (as in the case of thermal coordinates)
            {
                if ((vecDCoord[i].y > 0) && (vecDCoord[i].x > 0) && (vecDCoord[i].y <= stereoCalibParam.HEIGHT)
                    && (vecDCoord[i].x <= stereoCalibParam.WIDTH))
                {
                    depthInMm = lookUpDepth(dImg, vecDCoord[i], true);

                    if (depthInMm > 7000)
                    { // Depth is undefined
                        depthInMm = prevDepthInMm;
                    }
                }
                else {
                    depthInMm = prevDepthInMm;
                }
            }
            else {
                depthInMm = prevDepthInMm;
            }
        }
        else
        {
            // If not, the depth has previously been determined
            depthInMm = float(vecDepthInMm[i]);
        }

        prevDepthInMm = int(depthInMm);

        // Find the euclidean distances from the cv::Point to the homographies
        cv::Mat worldCoordPoint(1, 3, CV_32F, cv::Scalar(0));
        cv::Point3f tmpWorldCoord;

        // First, map the image coordinates to world coordinates
        if (MAP_TYPE == 1) {
			if (stereoCalibParam.rgbCamMat.size() != cv::Size(3, 3))
			{
				return;
			}

            worldCoordPoint.at<float>(0, 0) = backProjectPoint(vecUndistRgbCoord[i].x, float(stereoCalibParam.rgbCamMat.at<double>(0, 0)),
                float(stereoCalibParam.rgbCamMat.at<double>(0, 2)), float(depthInMm));
            worldCoordPoint.at<float>(0, 1) = backProjectPoint(vecUndistRgbCoord[i].y, float(stereoCalibParam.rgbCamMat.at<double>(1, 1)),
                float(stereoCalibParam.rgbCamMat.at<double>(1, 2)), float(depthInMm));
            worldCoordPoint.at<float>(0, 2) = float(depthInMm);
        }
        else {
			if (stereoCalibParam.tCamMat.size() != cv::Size(3, 3))
			{
				return;
			}

            worldCoordPoint.at<float>(0, 0) = backProjectPoint(vecUndistTCoord[i].x, float(stereoCalibParam.tCamMat.at<double>(0, 0)),
                float(stereoCalibParam.tCamMat.at<double>(0, 2)), 1500);
            worldCoordPoint.at<float>(0, 1) = backProjectPoint(vecUndistTCoord[i].y, float(stereoCalibParam.tCamMat.at<double>(1, 1)),
                float(stereoCalibParam.tCamMat.at<double>(1, 2)), 1500);
            worldCoordPoint.at<float>(0, 2) = 1500;

        }

        tmpWorldCoord.x = worldCoordPoint.at<float>(0, 0);
        tmpWorldCoord.y = worldCoordPoint.at<float>(0, 1);
        tmpWorldCoord.z = worldCoordPoint.at<float>(0, 2);
        worldCoordPointVector.push_back(tmpWorldCoord);

        int bestHomTmp = 0; // Reset parameters
        double bestDist = 1e6;
        homDist.clear(); homWeights.clear();

        for (auto j = 0; j < nbrClusters; j++)
        {
            if (stereoCalibParam.homDepthCentersRgb[j].z >= 0) // Is the current cluster valid? (also applies for thermal clusters)
            {
                cv::Mat depthCenter(1, 3, CV_32F, cv::Scalar(0));
                // Compute Euclidean distance
                if (MAP_TYPE == 1)
                {
                    homDist.push_back(sqrt(pow(worldCoordPoint.at<float>(0, 0) - stereoCalibParam.homDepthCentersRgb[j].x, 2) +
                        pow(worldCoordPoint.at<float>(0, 1) - stereoCalibParam.homDepthCentersRgb[j].y, 2) +
                        pow(worldCoordPoint.at<float>(0, 2) - stereoCalibParam.homDepthCentersRgb[j].z, 2)));  // Euclidean					
                }
                else {
                    homDist.push_back(sqrt(pow(worldCoordPoint.at<float>(0, 1) - stereoCalibParam.homDepthCentersT[j].x, 2) +
                        pow(worldCoordPoint.at<float>(0, 1) - stereoCalibParam.homDepthCentersT[j].y, 2) +
                        pow(worldCoordPoint.at<float>(0, 2) - stereoCalibParam.homDepthCentersT[j].z, 2)));  // Euclidean
                }

                for (auto k = 0; k < settings.discardedHomographies.size(); ++k) {
                    if (j == settings.discardedHomographies[k]) {
                        homDist[j] = 1e12;
                    }
                }

                sqDist = homDist[j];
                if (sqDist < bestDist)
                {
                    bestDist = sqDist;
                    bestHomTmp = j;
                }

            }
            else {
                homDist.push_back(1e12);
            }
        }

        /* Compute the corresponding cv::Point in the other modality
        This includes the weighting of multiple homographies
        */
        tmpUndistRgbPoint.clear(); tmpUndistTPoint.clear();
        estimatedPoint.x = 0; estimatedPoint.y = 0;
        if (MAP_TYPE == 1) {
            tmpUndistRgbPoint.push_back(vecUndistRgbCoord[i]);
        }
        else {
            tmpUndistTPoint.push_back(vecUndistTCoord[i]);
        }

        std::vector<int> octantIndicesTmp; std::vector<double> octantDistancesTmp;

        if (MAP_TYPE == 1) {
            trilinearInterpolator(tmpWorldCoord, stereoCalibParam.homDepthCentersRgb, homDist, homWeights,
                octantIndicesTmp, octantDistancesTmp);

            weightedHomographyMapper(tmpUndistRgbPoint, tmpEstimatedPoint, stereoCalibParam.planarHom, homWeights);

        }
        else {
            trilinearInterpolator(tmpWorldCoord, stereoCalibParam.homDepthCentersT, homDist, homWeights,
                octantIndicesTmp, octantDistancesTmp);

            weightedHomographyMapper(tmpUndistTPoint, tmpEstimatedPoint, stereoCalibParam.planarHomInv, homWeights);
        }

        octantIndices.push_back(octantIndicesTmp);
        octantDistances.push_back(octantDistancesTmp);
        estimatedPoint = tmpEstimatedPoint[0];

        if (MAP_TYPE == 1) {
            vecUndistTCoord.push_back(estimatedPoint);
        }
        else {
            vecUndistRgbCoord.push_back(estimatedPoint);
        }

        minDist.push_back(bestDist);
        bestHom.push_back(bestHomTmp);
    }

    if (count > 0) {
        std::cout << std::endl;
    }
}

void Registrator::trilinearInterpolator(cv::Point3f inputPoint, std::vector<cv::Point3f> &sourcePoints, std::vector<double> &precomputedDistance, std::vector<double> &weights,
    std::vector<int> &nearestSrcPointInd, std::vector<double> &nearestSrcPointDist)
{
    /*	TrilinearHomographyInterpolator finds the nearest cv::Point for each quadrant in 3D space and calculates weights
    based on trilinear interpolation for the input 3D cv::Point. The function returns a list of weights of the Points
    used for the interpolation
    */

    /* Octant map:
    I:		+ + +
    II:		- + +
    III:	- - +
    IV:		+ - +
    V:		+ + -
    VI:		- + -
    VII:	- - -
    VIII:	+ - -
    */
    weights.clear();
    nearestSrcPointDist.clear();
    nearestSrcPointInd.clear();


    // Step 1: Label each sourcePoint according to the octant it belongs to
    std::vector<int> octantMap;

    for (auto i = 0; i < sourcePoints.size(); ++i)
    {

        if ((sourcePoints[i].x - inputPoint.x) > 0)
        {	// x is positive; We are either in the first, fourth, fifth, or eighth octant

            if ((sourcePoints[i].y - inputPoint.y) > 0)
            { // y is positive; We are either in the first or fifth octant

                if ((sourcePoints[i].z - inputPoint.z) > 0)
                { // z is positive; We are in the first octant
                    octantMap.push_back(1);
                }
                else {
                    // z is negative; We are in the fifth octant				
                    octantMap.push_back(5);
                }
            }
            else {
                // y is negative; We are either in the fourth or eighth octant

                if ((sourcePoints[i].z - inputPoint.z) > 0)
                { // z is positive; We are in the fourth octant
                    octantMap.push_back(4);
                }
                else {
                    // z is negative; We are in the eighth octant				
                    octantMap.push_back(8);
                }
            }
        }
        else {
            // x is negative; We are either in the second, third, sixth, or seventh octant

            if ((sourcePoints[i].y - inputPoint.y) > 0)
            { // y is positive; We are either in the second or sixth octant

                if ((sourcePoints[i].z - inputPoint.z) > 0)
                { // z is positive; We are in the second octant
                    octantMap.push_back(2);
                }
                else {
                    // z is negative; We are in the sixth octant				
                    octantMap.push_back(6);
                }
            }
            else {
                // y is negative; We are either in the third or seventh octant

                if ((sourcePoints[i].z - inputPoint.z) > 0)
                { // z is positive; We are in the third octant
                    octantMap.push_back(3);
                }
                else {
                    // z is negative; We are in the seventh octant				
                    octantMap.push_back(7);
                }
            }
        }
    }

    // Step 2: Find the nearest cv::Point for every octant

    for (int i = 0; i < 8; ++i)
    {
        nearestSrcPointInd.push_back(-1);
        nearestSrcPointDist.push_back(1e6);
    }

    int currentOctant;
    double currentDist;

    for (auto i = 0; i < sourcePoints.size(); ++i)
    {
        // Identify the current octant
        currentOctant = octantMap[i];

        // Identify the distance to the input cv::Point
        currentDist = precomputedDistance[i];

        if (nearestSrcPointDist[currentOctant - 1] > currentDist)
        {
            nearestSrcPointInd[currentOctant - 1] = i;
            nearestSrcPointDist[currentOctant - 1] = currentDist;
        }
    }

    // Step 3: Compute the relative weight of the eight surrounding Points
    std::vector<double> unNormWeights;
    double tmpWeight;
    double sumOfDistances = 0;

    for (auto i = 0; i < nearestSrcPointDist.size(); ++i)
    {
        if (nearestSrcPointDist[i] < 1e6)
            sumOfDistances += nearestSrcPointDist[i];
    }

    for (auto i = 0; i < nearestSrcPointDist.size(); ++i)
    {
        if (nearestSrcPointDist[i] < 1e6)
        {
            tmpWeight = sumOfDistances / nearestSrcPointDist[i];
            unNormWeights.push_back(tmpWeight);
        }
        else {
            unNormWeights.push_back(0.);
        }
    }

    // Step 4: Normalize the weights
    double sumOfWeights = 0;
    std::vector<double> tmpWeightsVec;

    for (auto i = 0; i < nearestSrcPointDist.size(); ++i)
    {
        sumOfWeights += unNormWeights[i];
    }

    for (auto i = 0; i < nearestSrcPointDist.size(); ++i)
    {
        tmpWeight = unNormWeights[i] / sumOfWeights;
        tmpWeightsVec.push_back(tmpWeight);

        nearestSrcPointDist[i] = tmpWeight;
    }

    // Step 5: Recreate the weights to make a weight for every distance in precomputedDistance, even if the weight is zero
    for (auto i = 0; i < precomputedDistance.size(); ++i)
    {
        weights.push_back(0.);
    }

    for (auto i = 0; i < tmpWeightsVec.size(); ++i)
    {
        if (nearestSrcPointInd[i] >= 0)
        {
            weights[nearestSrcPointInd[i]] = tmpWeightsVec[i];
        }
    }

}

void Registrator::bilinearInterpolator(cv::Point2f inputPoint, std::vector<cv::Point2f> &sourcePoints, std::vector<double> &precomputedDistance, std::vector<double> &weights,
    std::vector<int> &nearestSrcPointInd, std::vector<double> &nearestSrcPointDist)
{
    /*	BilinearHomographyInterpolator finds the nearest cv::Point for each quadrant in 3D space and calculates weights
    based on bilinear interpolation for the input 2D cv::Point. The function returns a list of weights of the Points
    used for the interpolation
    */

    /* Quadrant map:
    I:		+ +
    II:		- +
    III:	- -
    IV:		+ -
    */
    weights.clear();
    nearestSrcPointDist.clear();
    nearestSrcPointInd.clear();

    // If we have more than four sourcePoints, use bilinearInterpolation. Otherwise use a simple interpolation to avoid unlinearities

    if (sourcePoints.size() > 4)
    {
        // Step 1: Label each sourcePoint according to the quadrant it belongs to
        std::vector<int> quadrantMap;
        float distanceX, distanceY, distanceZ;

        for (auto i = 0; i < sourcePoints.size(); ++i)
        {
            if ((sourcePoints[i].x - inputPoint.x) > 0)
            {	// x is positive; We are either in the first or fourth oct

                if ((sourcePoints[i].y - inputPoint.y) > 0)
                { // y is positive; We are in the first quadrant

                    quadrantMap.push_back(1);
                }
                else {
                    // y is negative; We are in the fourth quadrant
                    quadrantMap.push_back(4);
                }
            }
            else {
                // x is negative; We are either in the second or third quadrant

                if ((sourcePoints[i].y - inputPoint.y) > 0)
                { // y is positive; We are in the second quadrant

                    quadrantMap.push_back(2);
                }
                else {
                    // y is negative; We are in the third quadrant
                    quadrantMap.push_back(3);
                }
            }
        }

        // Step 2: Find the nearest cv::Point for every quadrant

        for (int i = 0; i < 8; ++i)
        {
            nearestSrcPointInd.push_back(-1);
            nearestSrcPointDist.push_back(1e6);
        }

        int currentquadrant;
        double currentDist;

        for (auto i = 0; i < sourcePoints.size(); ++i)
        {
            // Identify the current quadrant
            currentquadrant = quadrantMap[i];

            // Identify the distance to the input cv::Point
            currentDist = precomputedDistance[i];

            if (nearestSrcPointDist[currentquadrant - 1] > currentDist)
            {
                nearestSrcPointInd[currentquadrant - 1] = i;
                nearestSrcPointDist[currentquadrant - 1] = currentDist;
            }
        }
    }
    else
    {
        // If we have four or less sourcePoints, compute the weights based on the distance from inputPoints and sourcePoints
        for (auto i = 0; i < sourcePoints.size(); ++i)
        {
            nearestSrcPointInd.push_back(i);
            nearestSrcPointDist.push_back(precomputedDistance[i]);
        }
    }

    // Step 3: Compute the relative weight of the four surrounding Points
    std::vector<double> unNormWeights;
    double tmpWeight;
    double sumOfDistances = 0;

    for (auto i = 0; i < nearestSrcPointDist.size(); ++i)
    {
        if (nearestSrcPointDist[i] < 1e6)
            sumOfDistances += nearestSrcPointDist[i];
    }

    for (auto i = 0; i < nearestSrcPointDist.size(); ++i)
    {
        if (nearestSrcPointDist[i] < 1e6)
        {
            tmpWeight = sumOfDistances / nearestSrcPointDist[i];
            unNormWeights.push_back(tmpWeight);
        }
        else {
            unNormWeights.push_back(0.);
        }
    }

    // Step 4: Normalize the weights
    double sumOfWeights = 0;
    std::vector<double> tmpWeightsVec;

    for (auto i = 0; i < nearestSrcPointDist.size(); ++i)
    {
        sumOfWeights += unNormWeights[i];
    }

    for (auto i = 0; i < nearestSrcPointDist.size(); ++i)
    {
        tmpWeight = unNormWeights[i] / sumOfWeights;
        tmpWeightsVec.push_back(tmpWeight);

        nearestSrcPointDist[i] = tmpWeight;
    }

    // Step 5: Recreate the weights to make a weight for every distance in precomputedDistance, even if the weight is zero
    for (auto i = 0; i < precomputedDistance.size(); ++i)
    {
        weights.push_back(0.);
    }

    for (auto i = 0; i < tmpWeightsVec.size(); ++i)
    {
        if (nearestSrcPointInd[i] >= 0)
        {
            weights[nearestSrcPointInd[i]] = tmpWeightsVec[i];
        }
    }

}

void Registrator::weightedHomographyMapper(std::vector<cv::Point2f> undistPoint, std::vector<cv::Point2f> &estimatedPoint, std::vector<cv::Mat> &homographies, std::vector<double> &homWeights)
{
    /* weightedHomographyMapper maps the undistPoint (undistorted cv::Point) based by a weighted sum of the provided homographies, which are weighted by homWeights
    */
    assert(homWeights.size() == homographies.size());
    std::vector<cv::Point2f> tmpEstimatedPoint; cv::Point2f tmpPoint;
    estimatedPoint.clear();
    estimatedPoint.push_back(tmpPoint);

    for (auto i = 0; i < homWeights.size(); ++i)
    {
        if (homWeights[i] > 0)
        {
			if (homographies[i].size() != cv::Size(3, 3))
			{
				return;
			}


            perspectiveTransform(undistPoint, tmpEstimatedPoint, homographies[i]);

            estimatedPoint[0].x = estimatedPoint[0].x + tmpEstimatedPoint[0].x * float(homWeights[i]);
            estimatedPoint[0].y = estimatedPoint[0].y + tmpEstimatedPoint[0].y * float(homWeights[i]);
        }
    }

}

void Registrator::depthOutlierRemovalLookup(std::vector<cv::Point2f> vecDCoord, std::vector<int> &vecDepthInMm)
{
    // depthOutlierRemovalLookup looks up depth for the provided coordinates of vecDCoord, discards garbage values
    // and makes constraints on the output depth in order to provide a smoothed depth lookup and removal of outlier values

    int depthInMm, depthNaNThreshold, validDepthMeasCount = 0, sumDepthInMm = 0;
    bool SCALE_TO_THEORETICAL = true;
    int avgDepthInMm;
    vecDepthInMm.clear();
    std::vector<int> rawDepthInMm;

    if (SCALE_TO_THEORETICAL) {
        depthNaNThreshold = int(stereoCalibParam.depthCoeffA * 6500 + stereoCalibParam.depthCoeffB);
    }
    else {
        depthNaNThreshold = 6500;
    }

    if (vecDCoord.size() > 0)
    {

        // Step 1: Look up the depth for the entire depth coordinate set
        for (auto i = 0; i < vecDCoord.size(); ++i)
        {
            if (vecDCoord.size() > 0)  // Are we providing any depth coordinates (as in the case of thermal coordinates)
            {
                if ((vecDCoord[i].y > 0) && (vecDCoord[i].x > 0) && (vecDCoord[i].y <= stereoCalibParam.HEIGHT)
                    && (vecDCoord[i].x <= stereoCalibParam.WIDTH))
                {
                    depthInMm = int(lookUpDepth(dImg, vecDCoord[i], SCALE_TO_THEORETICAL));


                    if (depthInMm > depthNaNThreshold)
                    { // Depth is undefined
                        depthInMm = depthNaNThreshold;
                    }
                    else {
                        // We have a valid depth measurement
                        sumDepthInMm += depthInMm;
                        ++validDepthMeasCount;
                    }
                }
                else {
                    // Depth is undefined
                    depthInMm = depthNaNThreshold;
                }
            }
            else {
                std::cerr << "No depth coordinates provided";
            }

            rawDepthInMm.push_back(depthInMm);
        }

        // Step 2: Filter outliers. Identify the outliers by perfoming K-means clustering and filter them 
        // according to a threshold. As we assume, that the human contours are in the foreground, this must
        // correspond to the minimum depth measurement cluster. Other depth measurements should be within a range
        // this cluster, otherwise they may be classified as outliers


        cv::Mat labels, centers;
        cv::Mat depthMat = cv::Mat::zeros(cv::Size(1, rawDepthInMm.size()), CV_32F);
        int nbrClusters;

        if (validDepthMeasCount < vecDCoord.size()) {
            // If there exists undefined depth in our dataset, we will need an extra cluster
            // to contain this
            nbrClusters = settings.nbrClustersForDepthOutlierRemoval;
        }
        else {
            // Otherwise, we will go for two clusters
            nbrClusters = settings.nbrClustersForDepthOutlierRemoval - 1;
        }

        if (vecDCoord.size() > nbrClusters) {
            for (auto i = 0; i < vecDCoord.size(); ++i) {
                depthMat.at<float>(i, 0) = float(rawDepthInMm[i]);
            }

            cv::kmeans(depthMat, nbrClusters, labels, cv::TermCriteria(CV_TERMCRIT_EPS, 10, 0.5), 100,
                cv::KMEANS_RANDOM_CENTERS, centers);

            int minAvg = depthNaNThreshold;

            for (int i = 0; i < nbrClusters; ++i) {
                // Find the lowest mean, or centre
                if (centers.at<float>(i, 0) < minAvg) {
                    minAvg = centers.at<float>(i, 0);
                }
            }
            avgDepthInMm = minAvg;

            std::vector<int> clustersToBeDiscarded;

            for (int i = 0; i < nbrClusters; ++i) {
                // If depths are more than depthProximityThreshold mm away from the human contour, they should be regarded as noise
                if (centers.at<float>(i, 0) >(minAvg + settings.depthProximityThreshold)) {
                    clustersToBeDiscarded.push_back(i);
                }
            }

            for (auto i = 0; i < vecDCoord.size(); ++i) {
                // Replace the noisy depth values by the average
                int currentCentre = labels.at<int>(i, 0);

                for (auto j = 0; j < clustersToBeDiscarded.size(); ++j) {
                    if (currentCentre == clustersToBeDiscarded[j]) {
                        rawDepthInMm[i] = minAvg;
                    }
                }
            }

            std::vector<int> labelsVec, centersVec; // For debug
            for (auto i = 0; i < vecDCoord.size(); ++i) {
                labelsVec.push_back(labels.at<int>(i, 0));
            }

            for (int i = 0; i < nbrClusters; ++i) {
                centersVec.push_back(int(centers.at<float>(i, 0)));
            }
        }
        else {
            // If we are operating on a very small dataset, calculate the mean manually
            if (validDepthMeasCount > 0) {
                avgDepthInMm = sumDepthInMm / validDepthMeasCount;
            }
            else {
                avgDepthInMm = stereoCalibParam.defaultDepth;
            }
        }

        // Step 3: Let the data through a non-causal moving-average filter
        int windowLength = 4;
        int filteredValue, validCounts;

        for (auto i = 0; i < vecDCoord.size(); ++i)
        {
            if (rawDepthInMm[i] != depthNaNThreshold) { // Extract the current value
                filteredValue = rawDepthInMm[i];
                validCounts = 1;
            }
            else {
                filteredValue = 0;
                validCounts = 0;
            }

            // Go backwards
            int j = i - 1;
            int currentStep = 0;

            while ((currentStep < (windowLength / 2)) && (j >= 0))
            {
                if (rawDepthInMm[j] != depthNaNThreshold) {
                    // If we have a valid value, add it to the filtered value
                    filteredValue += rawDepthInMm[j];
                    validCounts++;
                    currentStep++;
                }
                --j;
            }

            // Go forwards
            j = i + 1;
            currentStep = 0;


            while ((currentStep < (windowLength / 2)) && (j < vecDCoord.size()))
            {
                if (rawDepthInMm[j] != depthNaNThreshold) {
                    // If we have a valid value, add it to the filtered value
                    filteredValue += rawDepthInMm[j];
                    validCounts++;
                    currentStep++;
                }
                ++j;
            }

            // Compute the smoothed value and insert it into the depth std::vector
            if (validCounts > 0) {
                vecDepthInMm.push_back(filteredValue / validCounts);
            }
            else {
                vecDepthInMm.push_back(avgDepthInMm);
            }
        }
    }
}

void Registrator::MyDistortPoints(std::vector<cv::Point2f> src, std::vector<cv::Point2f> & dst,
    const cv::Mat & cameraMatrix, const cv::Mat & distortionMatrix)
{
    // Normalize Points before entering the distortion process
    cv::Mat zeroDist;
    zeroDist = cv::Mat::zeros(1, 5, CV_32F);
    std::vector<cv::Point2f> normalizedUndistPoints;
    undistortPoints(src, normalizedUndistPoints, cameraMatrix, zeroDist);

    src = normalizedUndistPoints;


    // Code thanks to http://stackoverflow.com/questions/10935882/opencv-camera-calibration-re-distort-Points-with-camera-intrinsics-extrinsics
    dst.clear();
    float fx = float(cameraMatrix.at<double>(0, 0));
    float fy = float(cameraMatrix.at<double>(1, 1));
    float ux = float(cameraMatrix.at<double>(0, 2));
    float uy = float(cameraMatrix.at<double>(1, 2));

    float k1 = float(distortionMatrix.at<double>(0, 0));
    float k2 = float(distortionMatrix.at<double>(0, 1));
    float p1 = float(distortionMatrix.at<double>(0, 2));
    float p2 = float(distortionMatrix.at<double>(0, 3));
    float k3 = 0;

    if (distortionMatrix.size().width > 4) {
        k3 = float(distortionMatrix.at<double>(0, 4));
    }

    //BOOST_FOREACH(const Point2d &p, src)
    for (unsigned int i = 0; i < src.size(); i++)
    {
        const cv::Point2f &p = src[i];
        float x = p.x;
        float y = p.y;
        float xCorrected, yCorrected;
        //Step 1 : correct distorsion
        {
            float r2 = x*x + y*y;
            //radial distorsion
            xCorrected = x * (1 + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2);
            yCorrected = y * (1 + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2);

            //tangential distorsion
            //The "Learning OpenCV" book is wrong here !!!
            //False equations from the "Learning OpenCv" book
            //xCorrected = xCorrected + (2. * p1 * y + p2 * (r2 + 2. * x * x)); 
            //yCorrected = yCorrected + (p1 * (r2 + 2. * y * y) + 2. * p2 * x);
            //Correct formulae found at : http://www.vision.caltech.edu/bouguetj/calib_doc/htmls/parameters.html
            xCorrected = xCorrected + (2 * p1 * x * y + p2 * (r2 + 2 * x * x));
            yCorrected = yCorrected + (p1 * (r2 + 2 * y * y) + 2 * p2 * x * y);
        }
        //Step 2 : ideal coordinates => actual coordinates
        {
            xCorrected = xCorrected * float(fx) + float(ux);
            yCorrected = yCorrected * float(fy) + float(uy);
        }
        dst.push_back(cv::Point2d(xCorrected, yCorrected));
    }

}

void Registrator::setImageWidth(int width)
{
    if (width > 0) {
        stereoCalibParam.WIDTH = width;
    }
}

void Registrator::setImageHeight(int height)
{
    if (height > 0) {
        stereoCalibParam.HEIGHT = height;
    }
}

bool Registrator::getIsInitialized()
{
    return settings.isInitialized;
}



void Registrator::loadMinCalibrationVars(std::string calFile)
{
    cv::FileStorage fsStereo;

    try {
        fsStereo.open(calFile, cv::FileStorage::READ);
    }
    catch (cv::Exception e) {
        QString errorMsg = "Exception happened opening file " + QString::fromStdString(calFile)
            + "\n\nException:\n" + QString::fromStdString(e.err) + "\n\nMessage: \n" + QString::fromStdString(e.msg);
        QMessageBox::critical(NULL, "Error opening calibration variables", errorMsg);
    }


    if (fsStereo.isOpened())
    { // If the file exists, we may read the stereo camera calibration parameters

        fsStereo["registrationMethod"] >> settings.registrationMethod;

        if (settings.registrationMethod == TrimodalMultipleHom)
        {
            // Depth to RGB registration maps
            fsStereo["rgbToDCalX"] >> stereoCalibParam.rgbToDCalX;
            fsStereo["rgbToDCalY"] >> stereoCalibParam.rgbToDCalY;
            fsStereo["dToRgbCalX"] >> stereoCalibParam.dToRgbCalX;
            fsStereo["dToRgbCalY"] >> stereoCalibParam.dToRgbCalY;

            // Homographies
            fsStereo["planarHom"] >> stereoCalibParam.planarHom;
            fsStereo["planarHomInv"] >> stereoCalibParam.planarHomInv;

            // 3D representations of the "centres" of the homographies
            fsStereo["homDepthCentersRgb"] >> stereoCalibParam.homDepthCentersRgb;
            fsStereo["homDepthCentersT"] >> stereoCalibParam.homDepthCentersT;

            // Misc parameters
            fsStereo["depthCoeffA"] >> stereoCalibParam.depthCoeffA;
            fsStereo["depthCoeffB"] >> stereoCalibParam.depthCoeffB;
            fsStereo["defaultDepth"] >> stereoCalibParam.defaultDepth;
            fsStereo["WIDTH"] >> stereoCalibParam.WIDTH;
            fsStereo["HEIGHT"] >> stereoCalibParam.HEIGHT;

            // Flags and settings
            fsStereo["UNDISTORT_IMAGES"] >> settings.UNDISTORT_IMAGES;
            fsStereo["depthProximityThreshold"] >> settings.depthProximityThreshold;
            fsStereo["nbrClustersForDepthOutlierRemoval"] >> settings.nbrClustersForDepthOutlierRemoval;

            if (settings.nbrClustersForDepthOutlierRemoval == 0)
            {
                settings.nbrClustersForDepthOutlierRemoval = 3;
            }

            fsStereo["discardedHomographies"] >> settings.discardedHomographies;
        }
        else if (settings.registrationMethod == BimodalMultipleHom)
        {
            fsStereo["homCam1Cam2"] >> stereoCalibParam.homCam1Cam2;
            fsStereo["homCam2Cam1"] >> stereoCalibParam.homCam2Cam1;
            fsStereo["nbrHom"] >> stereoCalibParam.nbrHom;
            fsStereo["homCentersCam1"] >> stereoCalibParam.homCentersCam1;
            fsStereo["homCentersCam2"] >> stereoCalibParam.homCentersCam2;
        }

        // Intrinsic camera parameters and distortion parameters
        fsStereo["cam1CamMat"] >> stereoCalibParam.rgbCamMat;
        fsStereo["cam2CamMat"] >> stereoCalibParam.tCamMat;

        if (stereoCalibParam.rgbCamMat.empty()) {
            stereoCalibParam.rgbCamMat = cv::Mat::eye(cv::Size(3, 3), CV_32FC1);
        }

        if (stereoCalibParam.tCamMat.empty()) {
            stereoCalibParam.tCamMat = cv::Mat::eye(cv::Size(3, 3), CV_32FC1);
        }

        fsStereo["cam1DistCoeff"] >> stereoCalibParam.rgbDistCoeff;
        fsStereo["cam2DistCoeff"] >> stereoCalibParam.tDistCoeff;

        bool distortionParametersLoaded = true;

        // Check the retrieved parameters. We should have at least 4 distortion parameters 
        // and at most 14 in order to use the OpenCV undistortion functions
        if (stereoCalibParam.rgbDistCoeff.size().area() < 4 || stereoCalibParam.rgbDistCoeff.size().area() > 14) {
            distortionParametersLoaded = false;

            // Choose sensible defaults
            stereoCalibParam.rgbDistCoeff = cv::Mat::zeros(cv::Size(1, 4), CV_32FC1);
        }

        if (stereoCalibParam.tDistCoeff.size().area() < 4 || stereoCalibParam.tDistCoeff.size().area() > 14) {
            distortionParametersLoaded = false;

            // Choose sensible defaults
            stereoCalibParam.tDistCoeff = cv::Mat::zeros(cv::Size(1, 4), CV_32FC1);
        }

        if (distortionParametersLoaded) {
            // If we have loaded the distortion parameters from the configuration file successfully, we 
            // should indicate, that we want to undistort the input coordinates, hence are 
            // the input images distorted
            settings.UNDISTORT_IMAGES = false;
        }
        else {
            // If not, we assume that we have loaded undistorted images or that we don't want to bother 
            // with rectifying the lens distortion
            settings.UNDISTORT_IMAGES = true;
        }

        settings.isInitialized = true;
    }
    fsStereo.release();
}

void Registrator::loadHomography(std::string calFile)
{
    cv::FileStorage fs;

    try {
        fs.open(calFile, cv::FileStorage::READ);
    }
    catch (cv::Exception e) {
        QString errorMsg = "Exception happened opening file " + QString::fromStdString(calFile)
            + "\n\nException:\n" + QString::fromStdString(e.err) + "\n\nMessage: \n" + QString::fromStdString(e.msg);
        QMessageBox::critical(NULL, "Error opening calibration variables", errorMsg);
    }

    settings.registrationMethod = BimodalSingleHom;

	if (fs.isOpened()) {
		try {
			fs["homRgbToT"] >> stereoCalibParam.homRgbToT;
			fs["homTToRgb"] >> stereoCalibParam.homTToRgb;
		} catch (cv::Exception e){
			// Try to load the homographies as a vector of matrices
			std::vector<cv::Mat> homRgbToT, homTToRgb;

			try {
				fs["homRgbToT"] >> homRgbToT;
				fs["homTToRgb"] >> homTToRgb;

				if (!homRgbToT.empty())
				{
					stereoCalibParam.homRgbToT = homRgbToT[0];
				} 
				if (!homTToRgb.empty())
				{
					stereoCalibParam.homTToRgb = homTToRgb[0];
				}
			}
			catch (cv::Exception e) {
				qDebug() << e.what();
			}
		}

        // Check the acquired homographies
        if ((stereoCalibParam.homRgbToT.cols != 3) || (stereoCalibParam.homRgbToT.rows != 3) ||
            (stereoCalibParam.homTToRgb.cols != 3) || (stereoCalibParam.homTToRgb.rows != 3)) {
            QString errorMsg = QString("Could not load homographies from file. The homographies must be 3x3 matrices in .yml-format.\n\n"
                "Dimensions of loaded matrices:\nhomRgbToT: %1x%2\nhomTToRgb: %3x%3").arg(stereoCalibParam.homRgbToT.rows).arg(
                    stereoCalibParam.homRgbToT.cols).arg(stereoCalibParam.homTToRgb.rows).arg(stereoCalibParam.homTToRgb.cols);
            QMessageBox::critical(NULL, "Unable to acquire homographies from file", errorMsg);
        }
    }


}

int Registrator::drawRegisteredContours(cv::Mat &rgbContourImage, cv::Mat& depthContourImage, cv::Mat& thermalContourImage, cv::Mat depthImg, int mapType, 
    bool preserveColors)
{
    // This functions draws the contours in rgbContourImage in a registered fashion in depthContourImage and thermalContourImage.
    // Note that in the current version, this does not handle contours inside holes in contours - although one-level holes in
    // contours are handled just fine. This is due to CV_RETR_CCOMP. For complete support, CV_RETR_TREE should be used.

    std::vector< std::vector<cv::Point> > contourPoints;
    std::vector<cv::Vec4i> hierarchy;
    cv::Mat workImg, sourceImg;

    switch (mapType) {
    case MAP_FROM_RGB:
    {
        workImg = rgbContourImage.clone();
        sourceImg = rgbContourImage;
        break;
    }
    case MAP_FROM_DEPTH:
    {
        workImg = depthContourImage.clone();
        sourceImg = depthContourImage;
        break;
    }
    case MAP_FROM_THERMAL:
    {
        workImg = thermalContourImage.clone();
        sourceImg = thermalContourImage;
        break;
    }
    default:
        workImg = rgbContourImage.clone();
        sourceImg = rgbContourImage;
    }

    cv::Mat erodedContourImage = cv::Mat::zeros(workImg.size(), workImg.type());
    cv::Mat gradX, gradY, grad;
    cv::Mat absGradX, absGradY;

    // Find the contours to draw:
    cv::findContours(workImg, contourPoints, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    // Use the Sobel operator to find gradients of the image in order to shrink the contours
    cv::Sobel(workImg, gradX, CV_32F, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
    cv::Sobel(workImg, gradY, CV_32F, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);


    // Draw them, one at a time:
    for (int i = 0; i < contourPoints.size(); i++) {

        // Shrink the contour to enhance the depth lookup. First, we need to compute the direction
        // of the intensity gradient from the Sobel derivatives
        std::vector<float> gradAngle;
        std::vector<cv::Point> erodedContourPoints;
        int erosionSize = 5;
        int successCount = 0;
        cv::Point2f prevPoint = contourPoints[i][0];

        for (auto j = 0; j < contourPoints[i].size(); ++j) {
            float tmpAngle;
            tmpAngle = cv::fastAtan2(gradX.at<float>(contourPoints[i][j]), gradY.at<float>(contourPoints[i][j]));
            gradAngle.push_back(tmpAngle);

            cv::Point tmpErodedPoint;
            tmpErodedPoint.x = contourPoints[i][j].x - cos(tmpAngle)*erosionSize;
            tmpErodedPoint.y = contourPoints[i][j].y + sin(tmpAngle)*erosionSize;

            // Check if the eroded cv::Point is inside the mask
            if ((tmpErodedPoint.x < sourceImg.cols) && (tmpErodedPoint.y < sourceImg.rows) &&
                (tmpErodedPoint.x >= 0) && (tmpErodedPoint.y >= 0) &&
                (sourceImg.at<uchar>(tmpErodedPoint) != 0)) {
                erodedContourPoints.push_back(tmpErodedPoint);
                prevPoint = tmpErodedPoint;
                successCount++;
            }
            else {
                // Otherwise, use the previous cv::Point
                erodedContourPoints.push_back(prevPoint);
            }
        }

        std::vector<cv::Point> rgbContour, dContour, tContour;
        getRegisteredContours(contourPoints[i], erodedContourPoints, rgbContour, dContour, tContour, depthImg, mapType);

        std::vector< std::vector<cv::Point> > drawPoints;
        cv::Scalar color;
        if (preserveColors) {
            color = cv::Scalar::all(sourceImg.at<uchar>(contourPoints[i][0]));
        }
        else {
            color = cv::Scalar::all(255); // Draw white as default.
        }
        if (hierarchy[i][3] != -1) { // Has parent. Thus, is a hole. Draw black.
            color = cv::Scalar::all(0);
        }

        if ((mapType == MAP_FROM_RGB || mapType == MAP_FROM_DEPTH) && !thermalContourImage.empty()) {

            // Check if we managed to produce drawContoursvalid thermal points
            if (tContour.empty()) {
				// Something went wrong. Indicate this to the caller 
				// by returning an empty thermal image
				thermalContourImage = cv::Mat();
			}
			else {
				drawPoints.clear();
				drawPoints.push_back(tContour);
				cv::drawContours(thermalContourImage, drawPoints, 0, color, CV_FILLED);
			}
        }

        if ((mapType == MAP_FROM_RGB || mapType == MAP_FROM_THERMAL) && !depthContourImage.empty()) {
            // Check if we managed to produce valid depth points
            if (dContour.empty()) {
				// Something went wrong. Indicate this to the caller 
				// by returning an empty depth image
				depthContourImage = cv::Mat();
			} else {
				drawPoints.clear();
				drawPoints.push_back(dContour);
				cv::drawContours(depthContourImage, drawPoints, 0, color, CV_FILLED);
			}
        }

        if ((mapType == MAP_FROM_THERMAL || mapType == MAP_FROM_DEPTH) && !rgbContourImage.empty()) {
            if (rgbContour.empty()) {
				// Something went wrong. Indicate this to the caller 
                // by returning an empty rgb image
				rgbContourImage = cv::Mat();
			}
			else {
				drawPoints.clear();
				drawPoints.push_back(rgbContour);
				cv::drawContours(rgbContourImage, drawPoints, 0, color, CV_FILLED);
			}
        }
    }

	return 0;
}


void Registrator::getRegisteredContours(std::vector<cv::Point> contour, std::vector<cv::Point> erodedContour, std::vector<cv::Point>& rgbContour, 
    std::vector<cv::Point>& dContour, std::vector<cv::Point>& tContour, cv::Mat depthImg, int mapType)
{
    std::vector<cv::Point2f> rgbCoords, erodedRgbCoords, dCoords, erodedDCoords, tCoords, sourceCoords, erodedSourceCoords;
    depthImg.copyTo(this->dImg); // Required for the registration of RGB to thermal

    for (int j = 0; j < contour.size(); j++) {
        sourceCoords.push_back(cv::Point(contour[j].x, contour[j].y));
    }

    for (int j = 0; j < erodedContour.size(); j++) {
        erodedSourceCoords.push_back(cv::Point(erodedContour[j].x, erodedContour[j].y));
    }

    switch (settings.registrationMethod)
    {
    case TrimodalMultipleHom:
    {
        // Use advanced mapping supporting both RGB, depth, and thermal

        if (mapType == MAP_FROM_RGB || mapType == MAP_FROM_DEPTH) {

            if (mapType == MAP_FROM_RGB) {
                computeCorrespondingDepthPointFromRgb(erodedSourceCoords, erodedDCoords);
                computeCorrespondingDepthPointFromRgb(sourceCoords, dCoords);

                if (dCoords.size() > 0) {
                    for (int j = static_cast<int>(dCoords.size()) - 1; j >= 0; j--) {

                        if ((dCoords[j].x == 0) && (dCoords[j].y == 0)) {
                            // Depth coordinate is not valid. Remove corresponding rgbCoord and erodedDCoord
                            sourceCoords.erase(sourceCoords.begin() + j);
                            erodedDCoords.erase(erodedDCoords.begin() + j);
                        }
                        else {
                            // Depth coordinate is valid. Insert from beginning
                            dContour.insert(dContour.begin(), (cv::Point(dCoords[j].x, dCoords[j].y)));
                        }
                    }
                }
            }
            else if (mapType == MAP_FROM_DEPTH) {
                computeCorrespondingRgbPointFromDepth(sourceCoords, rgbCoords);
                erodedDCoords = erodedSourceCoords;
            }

            std::vector<int> homInd, vecDepthInMm;
            std::vector<double> minDist;
            std::vector<std::vector<double>> octantDistances;
            std::vector<std::vector<int>> octantIndices;
            std::vector<cv::Point3f> worldCoordPointVector;

            this->depthOutlierRemovalLookup(erodedDCoords, vecDepthInMm); // Use the coordinates of the eroded mask to look up the depth

            if (sourceCoords.size() > 0) {
                computeCorrespondingThermalPointFromRgb(sourceCoords, tCoords, dCoords, vecDepthInMm, minDist,
                    homInd, octantIndices, octantDistances, worldCoordPointVector);
            }
        }
        else if (mapType == MAP_FROM_THERMAL) {
            computeCorrespondingRgbPointFromThermal(sourceCoords, rgbCoords);
            computeCorrespondingDepthPointFromRgb(rgbCoords, dCoords);
        }


        break;
    }
    case BimodalMultipleHom:
    {
        // Use advanced mapping using both RGB and Thermal
        std::vector<cv::Point2f> dCoords; // No  depth is provided

        if (mapType == MAP_FROM_RGB) {
            computeCorrespondingThermalPointFromRgb(sourceCoords, tCoords, dCoords);
        }
        else if (mapType == MAP_FROM_THERMAL) {
            computeCorrespondingRgbPointFromThermal(sourceCoords, rgbCoords);
        }

        break;
    }
    case BimodalSingleHom:
    {
        // Use a single, planar homography for the RGB -> thermal mapping

        if (mapType == MAP_FROM_RGB) {
			if (!stereoCalibParam.homRgbToT.empty()) {
				perspectiveTransform(sourceCoords, tCoords, stereoCalibParam.homRgbToT);
			}            
        }
        else if (mapType == MAP_FROM_THERMAL) {
			if (!stereoCalibParam.homTToRgb.empty()) {
				perspectiveTransform(sourceCoords, rgbCoords, stereoCalibParam.homTToRgb);
			}
        }
        
        break;
    }
    }

	if ((stereoCalibParam.WIDTH <= 0) || (stereoCalibParam.HEIGHT <= 0)) {
		rgbCoords.clear();
		tCoords.clear();
		dCoords.clear();
		return;
	}

  
    for (auto i = 0; i < rgbCoords.size(); ++i) {
        cv::Point2f adjustedCoord = rgbCoords[i];

        if (adjustedCoord.x < 0) {
            adjustedCoord.x = 0;
        }
        if (adjustedCoord.y < 0) {
            adjustedCoord.y = 0;
        }
        if (adjustedCoord.x >= stereoCalibParam.WIDTH) {
            adjustedCoord.x = stereoCalibParam.WIDTH - 1;
        }
        if (adjustedCoord.y >= stereoCalibParam.HEIGHT) {
            adjustedCoord.y = stereoCalibParam.HEIGHT - 1;
        }

        rgbContour.push_back(cv::Point(adjustedCoord.x+0.5, adjustedCoord.y+0.5));
    }

    for (auto j = 0; j < tCoords.size(); j++) {
        cv::Point2f adjustedCoord = tCoords[j];

        if (adjustedCoord.x < 0) {
            adjustedCoord.x = 0;
        }
        if (adjustedCoord.y < 0) {
            adjustedCoord.y = 0;
        }
        if (adjustedCoord.x >= stereoCalibParam.WIDTH) {
            adjustedCoord.x = stereoCalibParam.WIDTH - 1;
        }
        if (adjustedCoord.y >= stereoCalibParam.HEIGHT) {
            adjustedCoord.y = stereoCalibParam.HEIGHT - 1;
        }

        tContour.push_back(cv::Point(adjustedCoord.x+0.5, adjustedCoord.y+0.5));
    }

    if (dContour.empty()) {
        for (auto i = 0; i < dCoords.size(); ++i) {
            cv::Point2f adjustedCoord = dCoords[i];

            if (adjustedCoord.x < 0) {
                adjustedCoord.x = 0;
            }
            if (adjustedCoord.y < 0) {
                adjustedCoord.y = 0;
            }
            if (adjustedCoord.x >= stereoCalibParam.WIDTH) {
                adjustedCoord.x = stereoCalibParam.WIDTH - 1;
            }
            if (adjustedCoord.y >= stereoCalibParam.HEIGHT) {
                adjustedCoord.y = stereoCalibParam.HEIGHT - 1;
            }

            dContour.push_back(cv::Point(adjustedCoord.x+0.5, adjustedCoord.y+0.5));
        }
    }
}


void Registrator::vecOfVecToCatVec(std::vector<std::vector<cv::Point2f> > vecOfVec, std::vector<cv::Point2f>& catVec)
{
    for (auto i = 0; i < vecOfVec.size(); ++i)
    {
        catVec.insert(catVec.end(), vecOfVec[i].begin(), vecOfVec[i].end());
    }
}

void Registrator::vecOfVecToCatVec(std::vector<std::vector<double> > vecOfVec, std::vector<double>& catVec)
{
    for (auto i = 0; i < vecOfVec.size(); i++)
    {
        catVec.insert(catVec.end(), vecOfVec[i].begin(), vecOfVec[i].end());
    }
}

void Registrator::vecOfVecToCatVec(std::vector<std::vector<int> > vecOfVec, std::vector<int>& catVec)
{
    for (auto i = 0; i < vecOfVec.size(); i++)
    {
        catVec.insert(catVec.end(), vecOfVec[i].begin(), vecOfVec[i].end());
    }
}

void Registrator::vecOfVecToCatVec3D(std::vector<std::vector<cv::Point3f> > vecOfVec, std::vector<cv::Point3f>& catVec)
{
    for (auto i = 0; i < vecOfVec.size(); i++)
    {
        catVec.insert(catVec.end(), vecOfVec[i].begin(), vecOfVec[i].end());
    }
}

void Registrator::toggleUndistortion(bool undistort)
{
    this->settings.UNDISTORT_IMAGES = undistort;
}

void Registrator::setDiscardedHomographies(std::vector<int> discardedHomographies)
{
    this->settings.discardedHomographies = discardedHomographies;
}

void Registrator::setUsePrevDepthPoint(bool value)
{
    this->settings.USE_PREV_DEPTH_POINT = value;
}
