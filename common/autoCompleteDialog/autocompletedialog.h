#ifndef STARTTRACKDIALOG_H
#define STARTTRACKDIALOG_H

#include <QDialog>
#include <QListWidgetItem>
#include <QRegExp>
#include <QMessageBox>

namespace Ui {
	class AutoCompleteDialog;
}

class AutoCompleteDialog : public QDialog
{
    Q_OBJECT

public:
	explicit AutoCompleteDialog(QWidget *parent = 0, QStringList wordList = QStringList(), QString title = QString("Dialog"));
	~AutoCompleteDialog();
	QString getWord();

private:
	Ui::AutoCompleteDialog *ui;
	QString word;
	QStringList wordList;

private slots:
	void on_wordList_itemSelectionChanged();
	void on_wordEdit_textEdited(QString text);

public slots:
	void accept();
};

#endif // STARTTRACKDIALOG_H
