HEADERS       *= openCVViewer/opencvviewer.h
SOURCES       *= openCVViewer/opencvviewer.cpp

QT += core gui widgets

!win32{
    LIBS *= -lopencv_core \
			-lopencv_highgui \
			-lopencv_imgproc
}

win32{
    INCLUDEPATH += "C:/OpenCV/release/install/include"
    LIBS += C:/OpenCV/release/bin/*.dll
}

INCLUDEPATH *= $${VPATH}openCVViewer/
