HEADERS       *= autoCompleteDialog/autocompletedialog.h
SOURCES       *= autoCompleteDialog/autocompletedialog.cpp
FORMS         *= autoCompleteDialog/autocompletedialog.ui


QT += core gui widgets

!win32{
    LIBS *= -lopencv_core \
			-lopencv_highgui \
			-lopencv_imgproc
}

win32{
    INCLUDEPATH += "C:/OpenCV/release/install/include"
    LIBS += C:/OpenCV/release/bin/*.dll
}

INCLUDEPATH *= $${VPATH}autoCompleteDialog/
