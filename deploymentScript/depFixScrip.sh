#! /bin/bash

#LIB_DIR=/Users/markpp/Downloads/opencv-3.3.0/build/lib
#APP_DIR=/Users/markpp/Desktop/code/VAPprojects/build-pixelAnnotator-Desktop_Qt_5_6_0_clang_64bit-Debug/pixelAnnotator.app
LIB_DIR=/usr/local/opt/opencv/lib
APP_DIR=/Users/chris/code/build-pixelAnnotator-Desktop_Qt_5_10_1_clang_64bit-Release/pixelAnnotator.app

# This script supposes here that you are in the parent directory of the app.

# check to what libs your app is "explicitly" linked to with otool -L $APP_DIR/Contents/MacOS/Qtfits_openmp
# If originally linking with LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc, you'd need to copy them in the .app with their dependencies.
# However, opencv has the path of symbolic links, ending with 3.0. , so we need to copy the actual files, i.e., with the whole version number 3.0.0. 
cp $LIB_DIR/libopencv_core.3.3.0.dylib $APP_DIR/Contents/Frameworks/libopencv_core.3.3.0.dylib
cp $LIB_DIR/libopencv_imgproc.3.3.0.dylib $APP_DIR/Contents/Frameworks/libopencv_imgproc.3.3.0.dylib
cp $LIB_DIR/libopencv_videoio.3.3.0.dylib $APP_DIR/Contents/Frameworks/libopencv_videoio.3.3.0.dylib
cp $LIB_DIR/libopencv_calib3d.3.3.0.dylib $APP_DIR/Contents/Frameworks/libopencv_calib3d.3.3.0.dylib
cp $LIB_DIR/libopencv_imgcodecs.3.3.0.dylib $APP_DIR/Contents/Frameworks/libopencv_imgcodecs.3.3.0.dylib
cp $LIB_DIR/libopencv_highgui.3.3.0.dylib $APP_DIR/Contents/Frameworks/libopencv_highgui.3.3.0.dylib
cp $LIB_DIR/libopencv_features2d.3.3.0.dylib $APP_DIR/Contents/Frameworks/libopencv_features2d.3.3.0.dylib

# Extra dependencies exist: otool -L libopencv_* will show you all the dependencies that you need to copy.
# Here we will also need to add imgcodecs and videoio to the .app
#cp $LIB_DIR/libopencv_imgcodecs.3.0.0.dylib $APP_DIR/Contents/Frameworks/libopencv_imgcodecs.3.0.0.dylib
#cp $LIB_DIR/libopencv_videoio.3.0.0.dylib $APP_DIR/Contents/Frameworks/libopencv_videoio.3.0.0.dylib


# With otool -L $APP_DIR/Contents/MacOS/yourApp , look at the .lib files with a wrong path. With the following result:
#   lib/libopencv_core.3.0.dylib (compatibility version 3.0.0, current version 3.0.0)
#   lib/libopencv_highgui.3.0.dylib (compatibility version 3.0.0, current version 3.0.0)
#   lib/libopencv_imgproc.3.0.dylib (compatibility version 3.0.0, current version 3.0.0)

# we need to fix the 3 libraries core, highgui and imgproc which are given here with an incorrect relative path. 
# Note that again, opencv kept the basename of the symbolic links (ending with 3.0.dylib), not the basename of the actual files (ending with 3.0.0.dylib)
# So we have to keep things consistent with what we did above. 

install_name_tool -change lib/libopencv_core.3.3.dylib @executable_path/../Frameworks/libopencv_core.3.3.0.dylib $APP_DIR/Contents/MacOS/pixelAnnotator
install_name_tool -change lib/libopencv_imgproc.3.3.dylib @executable_path/../Frameworks/libopencv_imgproc.3.3.0.dylib $APP_DIR/Contents/MacOS/pixelAnnotator
install_name_tool -change lib/libopencv_videoio.3.3.dylib @executable_path/../Frameworks/libopencv_videoio.3.3.0.dylib $APP_DIR/Contents/MacOS/pixelAnnotator
install_name_tool -change lib/libopencv_calib3d.3.3.dylib @executable_path/../Frameworks/libopencv_calib3d.3.3.0.dylib $APP_DIR/Contents/MacOS/pixelAnnotator
install_name_tool -change lib/libopencv_imgcodecs.3.3.dylib @executable_path/../Frameworks/libopencv_imgcodecs.3.3.0.dylib $APP_DIR/Contents/MacOS/pixelAnnotator
install_name_tool -change lib/libopencv_highgui.3.3.dylib @executable_path/../Frameworks/libopencv_highgui.3.3.0.dylib $APP_DIR/Contents/MacOS/pixelAnnotator
install_name_tool -change lib/libopencv_features2d.3.3.dylib @executable_path/../Frameworks/libopencv_features2d.3.3.0.dylib $APP_DIR/Contents/MacOS/pixelAnnotator

# Now we need to fix the path to all the dependencies of each library file.
cd $APP_DIR/Contents/Frameworks

# Change the IDs
for i in libopencv*.dylib; do install_name_tool -id @executable_path/../Frameworks/$i $i; done

# Again, with otool -L libopencv_* , you will see that you need to change their paths so that your copied libraries are pointed to within the app.
# for each opencv lib that is not properly referenced, change it from the absolute or wrong-relative path to @executable_path/../Frameworks/

# fix libopencv_highgui
install_name_tool -change $LIB_DIR/libopencv_imgcodecs.3.3.dylib @executable_path/../Frameworks/libopencv_imgcodecs.3.3.0.dylib libopencv_highgui.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_core.3.3.dylib @executable_path/../Frameworks/libopencv_core.3.3.0.dylib libopencv_highgui.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_imgproc.3.3.dylib @executable_path/../Frameworks/libopencv_imgproc.3.3.0.dylib libopencv_highgui.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_videoio.3.3.dylib @executable_path/../Frameworks/libopencv_videoio.3.3.0.dylib libopencv_highgui.3.3.0.dylib
# fix libopencv_imgproc 
install_name_tool -change $LIB_DIR/libopencv_core.3.3.dylib @executable_path/../Frameworks/libopencv_core.3.3.0.dylib libopencv_imgproc.3.3.0.dylib

# fix libopencv_videoio
install_name_tool -change $LIB_DIR/libopencv_imgcodecs.3.3.dylib @executable_path/../Frameworks/libopencv_imgcodecs.3.3.0.dylib libopencv_videoio.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_imgproc.3.3.dylib @executable_path/../Frameworks/libopencv_imgproc.3.3.0.dylib libopencv_videoio.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_core.3.3.dylib @executable_path/../Frameworks/libopencv_core.3.3.0.dylib libopencv_videoio.3.3.0.dylib

# fix libopencv_imgcodecs
install_name_tool -change $LIB_DIR/libopencv_imgproc.3.3.dylib @executable_path/../Frameworks/libopencv_imgproc.3.3.0.dylib libopencv_imgcodecs.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_core.3.3.dylib @executable_path/../Frameworks/libopencv_core.3.3.0.dylib libopencv_imgcodecs.3.3.0.dylib

# fix libopencv_calib3d
install_name_tool -change $LIB_DIR/libopencv_imgcodecs.3.3.dylib @executable_path/../Frameworks/libopencv_imgcodecs.3.3.0.dylib libopencv_calib3d.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_core.3.3.dylib @executable_path/../Frameworks/libopencv_core.3.3.0.dylib libopencv_calib3d.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_imgproc.3.3.dylib @executable_path/../Frameworks/libopencv_imgproc.3.3.0.dylib libopencv_calib3d.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_videoio.3.3.dylib @executable_path/../Frameworks/libopencv_videoio.3.3.0.dylib libopencv_calib3d.3.3.0.dylib
install_name_tool -change $LIB_DIR/libopencv_features2d.3.3.dylib @executable_path/../Frameworks/libopencv_features2d.3.3.0.dylib libopencv_calib3d.3.3.0.dylib

#install_name_tool -change 
#@executable_path/../Frameworks/QtWidgets.framework/Versions/5/QtWidgets
