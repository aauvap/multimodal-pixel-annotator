#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "adjustpicturedialog.h"

SettingsDialog::SettingsDialog(QWidget *parent, cv::Mat thermalImg) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    ui->saveAllModalities->setChecked(settings.value("saveAllModalities", true).toBool());
    ui->borderThickness->setValue(settings.value("borderThickness").toInt());
    overlayColor = settings.value("overlayColor").value<QColor>();
    ui->overlayOpacitySpinBox->setValue(settings.value("overlayOpacity", 0.3).toDouble() * 100);
    ui->btnColor->setStyleSheet(QString("QPushButton {"
                                        "background-color: rgb(%1, %2, %3);"
                                        "border-style: inset;"
                                        "border-width: 2px;"
                                    "}"
                                    "QPushButton:pressed {"
                                        "border-style: outset;"
                                        "}").arg(overlayColor.red()).arg(overlayColor.green()).arg(overlayColor.blue()));

    dontCareOverlayColor = settings.value("dontCareOverlayColor", QColor(255,255,255)).value<QColor>();
    ui->btnDontCareColor->setStyleSheet(QString("QPushButton {"
        "background-color: rgb(%1, %2, %3);"
        "border-style: inset;"
        "border-width: 2px;"
        "}"
        "QPushButton:pressed {"
        "border-style: outset;"
        "}").arg(dontCareOverlayColor.red()).arg(dontCareOverlayColor.green()).arg(dontCareOverlayColor.blue()));
    ui->dontCareOverlayOpacitySpinBox->setValue(settings.value("dontCareOverlayOpacity", 0.3).toDouble() * 100);
    ui->useDontCareMask->setChecked(settings.value("useMask").toBool());

    ui->noiseBlobSize->setValue(settings.value("noiseBlobSize", 20).toInt());
    ui->holeFillSize->setValue(settings.value("holeFillSize", 20).toInt());

    ui->autoBorderWhenSwitching->setChecked(settings.value("autoBorderWhenSwitching").toBool());
    ui->autoBorderWhenDrawing->setChecked(settings.value("autoBorderWhenDrawing").toBool());

    ui->rgbPattern->setText(settings.value("rgbPattern").toString());
    ui->depthPattern->setText(settings.value("depthPattern").toString());
    ui->thermalPattern->setText(settings.value("thermalPattern").toString());
    ui->csvPath->setText(settings.value("csvPath").toString());
    ui->rgbMaskFolder->setText(settings.value("rgbMaskFolder").toString());
    ui->depthMaskFolder->setText(settings.value("depthMaskFolder").toString());
    ui->thermalMaskFolder->setText(settings.value("thermalMaskFolder").toString());

    ui->usePlanarHomRadioButton->setChecked(settings.value("usePlanarHom").toBool());
    ui->useMultipleHomRadioButton->setChecked(settings.value("useMultipleHom").toBool());

    //ui->enableRgbCheckBox->setChecked(settings.value("enableRgb").toBool()); // Currently not available for deactivation
    ui->enableDepthCheckBox->setChecked(settings.value("enableDepth").toBool());
    ui->enableThermalCheckBox->setChecked(settings.value("enableThermal").toBool());

    ui->imageColorAdjustmentsGroupBox->setVisible(settings.value("enableThermal").toBool());
	ui->limitTagsCheckBox->setChecked(settings.value("limitTagsToSuggested", 0).toBool());

    this->thermalImg = thermalImg;
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::accept()
{
    settings.setValue("saveAllModalities", ui->saveAllModalities->isChecked());
    settings.setValue("borderThickness", ui->borderThickness->value());
    settings.setValue("overlayColor", overlayColor);
    settings.setValue("overlayOpacity", ui->overlayOpacitySpinBox->value() / 100.0);
    settings.setValue("noiseBlobSize", ui->noiseBlobSize->value());
    settings.setValue("holeFillSize", ui->holeFillSize->value());
    settings.setValue("rgbPattern", ui->rgbPattern->text());
    settings.setValue("depthPattern", ui->depthPattern->text());
    settings.setValue("thermalPattern", ui->thermalPattern->text());
    settings.setValue("csvPath", ui->csvPath->text());
    settings.setValue("rgbMaskFolder", ui->rgbMaskFolder->text());
    settings.setValue("depthMaskFolder", ui->depthMaskFolder->text());
    settings.setValue("thermalMaskFolder", ui->thermalMaskFolder->text());

    settings.setValue("usePlanarHom", ui->usePlanarHomRadioButton->isChecked());
    settings.setValue("useMultipleHom", ui->useMultipleHomRadioButton->isChecked());

    //settings.setValue("enableRGB", ui->enableRgbCheckBox->isChecked());
    settings.setValue("enableDepth", ui->enableDepthCheckBox->isChecked());
    settings.setValue("enableThermal", ui->enableThermalCheckBox->isChecked());

    settings.setValue("autoBorderWhenSwitching", ui->autoBorderWhenSwitching->isChecked());
    settings.setValue("autoBorderWhenDrawing", ui->autoBorderWhenDrawing->isChecked());
    
    settings.setValue("useMask", ui->useDontCareMask->isChecked());
    settings.setValue("dontCareOverlayColor", dontCareOverlayColor);
    settings.setValue("dontCareOverlayOpacity", ui->dontCareOverlayOpacitySpinBox->value() / 100.0);
	settings.setValue("limitTagsToSuggested", ui->limitTagsCheckBox->isChecked());

    done(QDialog::Accepted);
}

void SettingsDialog::on_btnColor_clicked()
{
    QColor chosenColor;
    chosenColor = QColorDialog::getColor(overlayColor, this, QString("Choose overlay starting color (only hue is used)"));

    if(!chosenColor.isValid()) {
        return;
    }

    chosenColor.setHsv(chosenColor.hue(), 255, 255);

    overlayColor = chosenColor;

    ui->btnColor->setStyleSheet(QString("QPushButton {"
                                        "background-color: rgb(%1, %2, %3);"
                                        "border-style: inset;"
                                        "border-width: 2px;"
                                    "}"
                                    "QPushButton:pressed {"
                                        "border-style: outset;"
                                        "}").arg(overlayColor.red()).arg(overlayColor.green()).arg(overlayColor.blue()));
}

void SettingsDialog::on_btnDontCareColor_clicked()
{
    QColor chosenColor;
    chosenColor = QColorDialog::getColor(dontCareOverlayColor, this, QString("Choose overlay color of don't care mask (only hue is used)"));

    if (!chosenColor.isValid()) {
        return;
    }

   
    chosenColor.setHsv(chosenColor.hue(), 255, 255);

    dontCareOverlayColor = chosenColor;

    ui->btnDontCareColor->setStyleSheet(QString("QPushButton {"
        "background-color: rgb(%1, %2, %3);"
        "border-style: inset;"
        "border-width: 2px;"
        "}"
        "QPushButton:pressed {"
        "border-style: outset;"
        "}").arg(dontCareOverlayColor.red()).arg(dontCareOverlayColor.green()).arg(dontCareOverlayColor.blue()));
}

void SettingsDialog::on_adjustThermalImagePushButton_clicked()
{
    AdjustPictureDialog d(this, thermalImg);

    d.exec();
}
