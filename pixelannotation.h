#ifndef PIXELANNOTATION_H
#define PIXELANNOTATION_H

#include <QStringList>
#include <QDebug>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

enum BoxStatus {
	ACTIVE,
	LASTFRAMEREACHED
};

class PixelAnnotation
{
public:
	PixelAnnotation(int id, const QString& tag = QString(), 
		const QStringList& metaDataNames = QStringList(), 
		const QStringList& metaDataTypes = QStringList(),
		const QString& compactPolygon = QString());

    int id() const;
    void id(int id);
    cv::Mat getMask();
    cv::Mat getMaskNoDontCare();
    cv::Mat getMaskOnlyDontCare();
	cv::Point getUpperLeftCorner() { return upperLeftCorner; }

	void setCompactPolygon(const QString& compactPolygon);
	QString getCompactPolygon() const;
	void resetCompactPolygon() { compactPolygon.clear(); };

    void setMask(cv::Mat mask, bool addDontCare = false, int borderThickness = 3, int borderColor = 170);
    void setDontCareMask(cv::Mat dontCareMask);

    static void generateDontCareFromMask(cv::Mat mask, cv::Mat &onlyDontCare, int borderThickness = 3, int borderColor = 170);

    bool isFinished() const { return activeStatus == LASTFRAMEREACHED ? true : false; };
	void setIsFinished(bool isFinished) {
		activeStatus = isFinished ?
            LASTFRAMEREACHED : ACTIVE;
	};

    int getMetaDataSize() const;
    QStringList getMetaData() const;
    QString getMetaData(int metaDataId) const;
    void setMetaData(int metaDataId, QString value);
    void setMetaData(int metaDataId, bool value);
    void setMetaData(QStringList values);
    void toggleMetaData(int metaDataId);

	static PixelAnnotation csvToAnnotation(const QString& csv,
		int csvMetaDataColumnIndex,
		const QStringList& metaDataNames,
		const QStringList& metaDataTypes);

	static QString annotationToCsv(const PixelAnnotation& annotation,
		const QStringList& startList);


    QString getMetaDataName(int metaDataId) const;
    QStringList getMetaDataNames() const;
    QString getMetaDataType(int metaDataId) const;

	QString getTag() const { return theTag; }
	void setTag(const QString& tag);

	cv::Rect getBoundingBox();

private:
	void findUpperLeftCorner(cv::Mat mask);

    int theId;
	QString theTag;
	QString compactPolygon;

    cv::Mat maskNoDontCare;
    cv::Mat maskOnlyDontCare;

	cv::Point upperLeftCorner;

    QStringList metaDataNames;
    QStringList metaDataTypes;
    QStringList metaDataValues;

	BoxStatus activeStatus;

};

#endif // PIXELANNOTATION_H
