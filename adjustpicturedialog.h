#ifndef ADJUSTPICTUREDIALOG_H
#define ADJUSTPICTUREDIALOG_H

#include <QtWidgets/QDialog>
#include <QSettings>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include "common/openCVViewer/opencvviewer.h"

namespace Ui {
class AdjustPictureDialog;
}

class AdjustPictureDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AdjustPictureDialog(QWidget *parent, cv::Mat thermalImg);
    ~AdjustPictureDialog();

    void accept();

private slots:
    void on_brightnessSlider_valueChanged();
    void on_contrastSlider_valueChanged();
    void on_contrastSpinBox_valueChanged();
    void on_brightnessSpinBox_valueChanged();

private:
    Ui::AdjustPictureDialog *ui;
    QSettings settings;
    void transformThermalImg();
    cv::Mat thermalImg;

};

#endif // !ADJUSTPICTUREDIALOG_H

