#ifndef BATCHEDITDIALOG_H
#define BATCHEDITDIALOG_H

#include <QtWidgets/QDialog>
#include <pixelannotation.h>

namespace Ui {
class BatchEditDialog;
}

class BatchEditDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit BatchEditDialog(QList<PixelAnnotation> annotations, QStringList baseDataNames, QStringList metaDataNames, QStringList metaDataTypes, QWidget *parent = 0);
    ~BatchEditDialog();
    
private slots:
    void on_btnClose_clicked();

    void on_btnPerformChanges_clicked();

private:
    Ui::BatchEditDialog *ui;
    QList<PixelAnnotation> annotations;
    QStringList baseDataNames;
    QStringList metaDataNames;
    QStringList metaDataTypes;
};

#endif // BATCHEDITDIALOG_H
