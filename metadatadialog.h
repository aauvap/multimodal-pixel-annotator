#ifndef METADATADIALOG_H
#define METADATADIALOG_H

#include <QtWidgets/QDialog>
#include <QStringList>
#include <QtWidgets/QMessageBox>

namespace Ui {
	class MetaDataDialog;
}

class MetaDataDialog : public QDialog
{
    Q_OBJECT

public:
	explicit MetaDataDialog(QWidget *parent, QStringList &wordList, QStringList &metaDataTypes, const QString &title, const QString &label);
	~MetaDataDialog();

	void accept();
	QStringList getNames();

private:
	Ui::MetaDataDialog *ui;
	QStringList metaDataNames;
	QStringList metaDataTypes;
	int maxNames;
};

#endif // METADATADIALOG_H
