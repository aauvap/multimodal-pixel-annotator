Multimodal Pixel Annotator is a tool for pixel-level annotation of masks in up to three different image modalities (RGB, thermal, depth).

The annotator is built in C++ with QT and OpenCV and may be compiled under Windows, macOS, and Linux. Pre-compiled binaries are available for these platforms in the [downloads section](https://bitbucket.org/aauvap/multimodal-pixel-annotator/downloads). The binaries may not always reflect the latest features and bug-fixes, however. 

The annotator is free to use under the MIT License. 

For bounding-box level annotations, please visit our [Bounding Box Annotator](https://bitbucket.org/aauvap/bounding-box-annotator/).

## How to use ##

#### Getting started ####

1. [Download the application binaries](https://bitbucket.org/aauvap/multimodal-pixel-annotator/downloads) for your platform or [compile the application](https://bitbucket.org/aauvap/multimodal-pixel-annotator/overview#markdown-header-compilation-and-set-up).
2. Download the [sample annotations](https://bitbucket.org/aauvap/multimodal-pixel-annotator/downloads/sampleAnnotations.zip)
3. Launch Pixel Annotator
4. In the menu, go to 'File -> Settings'
    a. Make sure that 'Enable Thermal' is checked, and 'Enable Depth' is unchecked. 
    b. In 'Registration', make sure that 'Use multiple homographies' is selected.
    c. Select the 'File patterns' tab, and make sure that the 'Image file patterns' are configured like this:

* RGB images      | \*cam1\*.png
* Thermal images  | \*cam2\*.png

5. Go to 'File -> Open folder', and open the sampleAnnotations folder
6. If prompted, select the 'mask.png' as the don't care mask
7. Feel free to explore the program

#### Configuring the program ####

1. Make sure that you have enabled the right modalities in 'File -> Settings'. If you have imagery of the same scene from two different views, you may use the thermal modality as the second view. 
2. We search for corresponding image files using [QRegularExpressions](http://doc.qt.io/qt-5/qregularexpression.html). Adjust the search paths to fit your project in 'File -> Settings -> File patterns -> Image file patterns'
3. Each annotation mask may be accompanied by  meta data. Before opening an annotation folder for the first time, set the meta data tags in 'File -> Edit meta data fields...'

### Keyboard shortcuts - see [wiki](https://bitbucket.org/aauvap/multimodal-pixel-annotator/wiki/Introduction%20guide.md#markdown-header-the-taskbar) ###


### Compilation and set-up - see [wiki](https://bitbucket.org/aauvap/multimodal-pixel-annotator/wiki/Compilation%20and%20set-up.md#!compilation-and-set-up) ##


### Contribution guidelines ###

Please test the tool and see if it fits your purpose. If not, open an [issue](https://bitbucket.org/aauvap/multimodal-pixel-annotator/issues?status=new&status=open).



### Who do I talk to? ###
* Chris H. Bahnsen at cb@create.aau.dk