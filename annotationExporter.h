#ifndef ANNOTATIONEXPORTER
#define ANNOTATIONEXPORTER

#include <QApplication>
#include <QComboBox>
#include <QDebug>
#include <QDateEdit>
#include <QDialog>
#include <QDirIterator>
#include <QFileDialog>
#include <QFileInfo>
#include <QFormLayout>
#include <QGroupBox>
#include <QHash>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QList>
#include <QMessageBox>
#include <QPushButton>
#include <QProgressBar>
#include <QRadioButton>
#include <QSet>
#include <QSpinBox>
#include <QToolButton>
#include <QTextStream>
#include <QTimer>
#include <QTimeEdit>
#include <QThread>

#include <fstream>
#include <iomanip>

#include <opencv2/opencv.hpp>
#include "pixelannotation.h"
#include "polygonmask.h"
#include "json.hpp"

using json = nlohmann::json;

/**
Identifiers for the four images in the workspace.

The program handles 3 modalities and a mask. These identifiers are used as indexes in the data structures which hold the images, imageviewers, etc.
*/
struct Image { enum e { RGB, Depth, Thermal, MaskNoDontCare, MaskOnlyDontCare }; };


class AnnotationExporter : public QDialog
{
	Q_OBJECT;

public:
	AnnotationExporter(QWidget* parent, const QStringList& annotationList,
		int imageWidth,
		int imageHeight,
		const QString& annotationDir,
		int csvMetaDataColumnIndex,
		const QStringList& metaDataNames,
		const QStringList& metaDataTypes,
		const QHash<int, QStringList>& fileLists,
		const QString& maskNoDontCareDirPath, 
		int numImages);

	public slots:
	void startConversionButtonClicked();
	void fileLocationPushButtonClicked();
	void createNewFileRadioButtonToggled();
	void filePathButtonsChecked();

private:
	void forceCoordinatesWithinImageFrame(double& xULC, double& yULC, double& xLRC, double& yLRC);

	QGroupBox* annotationFormatGroupBox;
	QComboBox* objectCategoriesComboBox;
	QPushButton* startConversionButton;
	QProgressBar* progressBar;
	QLabel* progressLabel;

	QRadioButton* createNewFileRadioButton;

	QGroupBox* fileLocationGroupBox;
	QPushButton* fileLocationPushButton;
	QLineEdit* fileLocationPath;	
	

	QHash<QString, QString> objectCategories;
	QString annotationDir;

	QStringList annotationList;
	int imageWidth;
	int imageHeight;
	int csvMetaDataColumnIndex;
	QStringList metaDataNames;
	QStringList metaDataTypes;
	QHash<int, QStringList> fileLists;
	int numImages;
	QString maskNoDontCareDirPath;

	QLineEdit* sampleFilePath;
	QList<QPushButton*> filePathList;
};



#endif // !ANNOTATIONEXPORTER

