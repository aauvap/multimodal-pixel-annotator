#include "annotationExporter.h"


AnnotationExporter::AnnotationExporter(QWidget * parent, 
	const QStringList & annotationList, 
	int imageWidth, 
	int imageHeight, 
	const QString & annotationDir, 
	int csvMetaDataColumnIndex, 
	const QStringList & metaDataNames, 
	const QStringList & metaDataTypes, 
	const QHash<int, QStringList>& fileLists,
	const QString & maskNoDontCareDirPath,
	int numImages)
	: QDialog(parent)
{
	setWindowTitle(tr("Export annotations to COCO format"));

	this->annotationList = annotationList;
	this->annotationList.sort();
	this->imageHeight = imageHeight;
	this->imageWidth = imageWidth;
	this->annotationDir = annotationDir;
	this->csvMetaDataColumnIndex = csvMetaDataColumnIndex;
	this->metaDataNames = metaDataNames;
	this->metaDataTypes = metaDataTypes;
	this->fileLists = fileLists;
	this->numImages = numImages;
	this->maskNoDontCareDirPath = maskNoDontCareDirPath;

	if (fileLists.empty() || (numImages == 0) || annotationList.empty())
	{
		return;
	}

	// Output file options
	QGroupBox* outputFileGroupBox = new QGroupBox(tr("Output options"));
	QVBoxLayout* outputFileLayout = new QVBoxLayout();

	createNewFileRadioButton = new QRadioButton(tr("Create new file or overwrite existing"));
	createNewFileRadioButton->toggle();
	connect(createNewFileRadioButton, SIGNAL(toggled(bool)), this, SLOT(createNewFileRadioButtonToggled()));

	outputFileLayout->addWidget(createNewFileRadioButton);

	QRadioButton* appendToExistingRadioButton = new QRadioButton(tr("Append to existing annotation file"));
	outputFileLayout->addWidget(appendToExistingRadioButton);
	outputFileGroupBox->setLayout(outputFileLayout);

	// File location
	QGroupBox* fileLocationGroupBox = new QGroupBox(tr("File location"));

	QHBoxLayout* fileLocationLayout = new QHBoxLayout();
	fileLocationPath = new QLineEdit();
	fileLocationLayout->addWidget(fileLocationPath);
	fileLocationPushButton = new QPushButton(tr("Set file path"));
	connect(fileLocationPushButton, SIGNAL(clicked(bool)), this, SLOT(fileLocationPushButtonClicked()));

	fileLocationLayout->addWidget(fileLocationPushButton);
	fileLocationGroupBox->setLayout(fileLocationLayout);

	// Export format
	annotationFormatGroupBox = new QGroupBox(tr("Export format"));
	QFormLayout* annotationFormatLayout = new QFormLayout();
	outputFileLayout->addLayout(fileLocationLayout);



	//// File formats
	//fileFormatComboBox = new QComboBox();

	//fileFormats.append("MSCOCO");

	//for (auto& fileFormat : fileFormats)
	//{
	//	fileFormatComboBox->addItem(fileFormat);
	//}

	//annotationFormatLayout->addRow(tr("Overall file format"), fileFormatComboBox);

	// Object categories
	objectCategoriesComboBox = new QComboBox();

	// Open the available csv-files in the "categoryLists" folder
	QStringList filters;
	filters << "*.txt" << "*.csv";

	QDirIterator it("./categoryLists", filters, QDir::NoFilter, QDirIterator::NoIteratorFlags);

	while (it.hasNext())
	{
		QString filePath = it.next();
		QString baseName = it.fileInfo().baseName();

		objectCategories.insert(baseName, filePath);
		objectCategoriesComboBox->addItem(baseName);
	}

	annotationFormatLayout->addRow(tr("Object categories"), objectCategoriesComboBox);

	QHBoxLayout* startConversionLayout = new QHBoxLayout();
	startConversionLayout->addStretch();

	startConversionButton = new QPushButton(tr("Start conversion"));
	startConversionButton->setDisabled(true);
	connect(startConversionButton, SIGNAL(clicked(bool)), this, SLOT(startConversionButtonClicked()));
	startConversionLayout->addWidget(startConversionButton);

	// Image file path
	QGroupBox* imageFilePathGroupBox = new QGroupBox(tr("Image file paths"));
	QVBoxLayout* imageFilePathLayout = new QVBoxLayout();

	QHBoxLayout* fileNameLayout = new QHBoxLayout();

	QString fileName = fileLists[Image::RGB][0];
	QDir dir(fileName);

	// Add file paths first
	QPushButton* fileButton = new QPushButton(tr("File name"));
	fileButton->setCheckable(false);
	fileButton->setEnabled(false);
	fileButton->setFlat(true);
	filePathList.push_front(fileButton);
	dir.cdUp();

	do {
		QPushButton* dirButton = new QPushButton(dir.dirName());
		dirButton->setCheckable(true);
		dirButton->setChecked(true);
		dirButton->setFlat(true);
		connect(dirButton, SIGNAL(toggled(bool)), this, SLOT(filePathButtonsChecked()));


		filePathList.push_front(dirButton);
		qDebug() << dir.dirName();

	} while (dir.exists() && dir.cdUp() && !dir.dirName().contains(".."));

	sampleFilePath = new QLineEdit();
	sampleFilePath->setDisabled(true);

	// Now pupulate list
	for (auto& button : filePathList)
	{
		fileNameLayout->addWidget(button);

		if (!button->text().contains(tr("File name")))
		{
			fileNameLayout->addWidget(new QLabel("/"));
			sampleFilePath->setText(sampleFilePath->text().append(button->text()));
			sampleFilePath->setText(sampleFilePath->text().append("/"));
		}
		else {
			sampleFilePath->setText(sampleFilePath->text().append(QFileInfo(fileName).fileName()));
		}
	}

	imageFilePathLayout->addLayout(fileNameLayout);
	imageFilePathLayout->addWidget(new QLabel(tr("Resulting image path in JSON file")));
	imageFilePathLayout->addWidget(sampleFilePath);

	imageFilePathGroupBox->setLayout(imageFilePathLayout);

	// Overall layout
	annotationFormatGroupBox->setLayout(annotationFormatLayout);

	QVBoxLayout* overallLayout = new QVBoxLayout();
	overallLayout->addWidget(outputFileGroupBox);
	overallLayout->addWidget(fileLocationGroupBox);
	overallLayout->addWidget(annotationFormatGroupBox);
	overallLayout->addWidget(imageFilePathGroupBox);
	progressBar = new QProgressBar();
	progressBar->hide();
	progressLabel = new QLabel();
	progressLabel->hide();
	overallLayout->addWidget(progressBar);
	overallLayout->addWidget(progressLabel);
	overallLayout->addLayout(startConversionLayout);

	setLayout(overallLayout);
}



void AnnotationExporter::fileLocationPushButtonClicked()
{
	QDir d(annotationDir);
	QString path;

	if (createNewFileRadioButton->isChecked())
	{
		path = QFileDialog::getSaveFileName(this, tr("Save annotation file to"), d.absoluteFilePath(d.dirName() + "-export.json"), tr("JavaScript Object Notation (*.json)"));
	}
	else {
		path = QFileDialog::getOpenFileName(this, tr("Append annotations to"), d.path(), tr("JavaScript Object Notation (*.json)"));

		json j;

		// Inspect file for custom information
		if (QFile::exists(path))
		{
			progressBar->show();
			progressBar->setMinimum(0);
			progressBar->setMaximum(0);
			progressLabel->show();
			progressLabel->setText("Inspecting annotation file...");
			QApplication::processEvents();


			std::ifstream appendInput(path.toStdString());
			appendInput >> j;

			auto customIt = j.find("custom");

			if (customIt != j.end())
			{
				auto filenameformattingIt = j["custom"].find("filenameformatting");

				if (filenameformattingIt != j["custom"].end())
				{
					std::vector<bool> fileNameFormatting = j["custom"]["filenameformatting"];
					auto index = fileNameFormatting.size() - 1;

					QListIterator<QPushButton*> listIt(filePathList);
					listIt.toBack();

					while (listIt.hasPrevious())
					{
						auto button = listIt.previous();

						if (index < fileNameFormatting.size() && index >= 0)
						{
							// Set button checked according to the saved value from JSON file
							button->setChecked(fileNameFormatting[index]);
						}
						else {
							button->setChecked(false);
						}

						index--;
					}
					filePathButtonsChecked();
				}
			}

			progressBar->hide();
			progressLabel->hide();
			QApplication::processEvents();

		}
	}

	if (!path.isEmpty())
	{
		fileLocationPath->setText(path);
		startConversionButton->setDisabled(false);
	}
}

void AnnotationExporter::createNewFileRadioButtonToggled()
{
	if (createNewFileRadioButton->isChecked())
	{
		annotationFormatGroupBox->setEnabled(true);
	}
	else {
		// The "append to existing annotation file" option assumes that we will re-use the existing object category list
		// There is thus no need to set it again
		annotationFormatGroupBox->setEnabled(false);
	}
}

void AnnotationExporter::filePathButtonsChecked()
{
	sampleFilePath->setText("");

	for (auto& button : filePathList)
	{
		if (button->isChecked())
		{
			if (!button->text().contains(tr("File name")))
			{
				sampleFilePath->setText(sampleFilePath->text().append(button->text()));
				sampleFilePath->setText(sampleFilePath->text().append("/"));
			}
		}
	}

				
	sampleFilePath->setText(sampleFilePath->text().append(QFileInfo(fileLists[Image::RGB][0]).fileName()));				
}

void AnnotationExporter::forceCoordinatesWithinImageFrame(double & xULC, double & yULC, double & xLRC, double & yLRC)
{
	xULC = std::max(0., std::min(xULC, double(imageWidth - 1)));
	yULC = std::max(0., std::min(yULC, double(imageHeight - 1)));
	xLRC = std::max(0., std::min(xLRC, double(imageWidth - 1)));
	yLRC = std::max(0., std::min(yLRC, double(imageHeight - 1)));
}

void AnnotationExporter::startConversionButtonClicked()
{
	startConversionButton->setEnabled(false);

	progressBar->setMaximum(numImages+10);
	progressBar->show();
	progressLabel->setText(tr("Reading category labels..."));
	progressLabel->show();

	json j;

	if (!createNewFileRadioButton->isChecked())
	{
		// If the append button is checked (and create new file button is unchecked), read from existing JSON file
		if (QFile::exists(fileLocationPath->text()))
		{
			std::ifstream appendInput(fileLocationPath->text().toStdString());
			appendInput >> j;

		} else {
			progressBar->hide();
			progressLabel->setText(tr("The provided annotation file does exist. Aborting."));
			return;
		}
	}

	QMap<QString, int> objectIdCategoryPairs;

	if (createNewFileRadioButton->isChecked())
	{
		// Get object category format if the createNewFileRadioButton is checked
		QFile objectCatFile(objectCategories[objectCategoriesComboBox->currentText()]);
		objectCatFile.open(QFile::ReadOnly);

		if (objectCatFile.isOpen())
		{
			QTextStream stream;
			stream.setDevice(&objectCatFile);

			while (!stream.atEnd()) {
				QString line = stream.readLine();
				QStringList split = line.split(";", QString::SkipEmptyParts);

				if (split.size() >= 2)
				{
					// Take care if the category contains multiple category names
					QStringList categoryNameSplit = split[1].split(",");

					if (categoryNameSplit.size() > 1)
					{
						for (auto& categoryName : categoryNameSplit)
						{
							QString cleanedCategoryName = categoryName.remove("'").remove("\"").simplified().toLower(); // ImageNet categories look like this: 'tench, Tinca tinca'

							if (cleanedCategoryName.size() > 1)
							{
								objectIdCategoryPairs[cleanedCategoryName] = split[0].toInt();
							}
						}
					}
					else {
						QString cleanedCategoryName = split[1].remove("'").simplified().toLower();
						objectIdCategoryPairs[cleanedCategoryName] = split[0].toInt(); // The simplified operator removes whitespace from start and end
					}

				}
			}
		}

		objectCatFile.close();
		j["categories"] = {};

		// Iterate through all object categories and include them in the JSON-file

		// But first, make sure that the items are inserted alphabetically
		QMap<int, QString> objectCategoriesByid;

		QMapIterator<QString, int> i(objectIdCategoryPairs);
		while (i.hasNext())
		{
			i.next();
			auto idIterator = objectCategoriesByid.find(i.value());

			if (idIterator == objectCategoriesByid.end())
			{
				// The id does not exist in the dictionary - just put it in
				objectCategoriesByid[i.value()] = i.key();
			}
			else {
				// The key does exist - append the category label
				objectCategoriesByid[i.value()].append(';' + i.key());
			}
		}

		// And then insert them in the JSON object
		QMapIterator<int, QString> iMap(objectCategoriesByid);
		while (iMap.hasNext())
		{
			iMap.next();
			QStringList categoryLabels = iMap.value().split(";");

			if (categoryLabels.size() >= 1)
			{
				j["categories"].push_back({ { "id", iMap.key() },{ "name", categoryLabels[0].toStdString() },{ "supercategory", "None" } });
			}
			
			categoryLabels.removeFirst();
			
			// Append the rest of the categories, if any	
			if (!categoryLabels.empty())
			{
				j["categories"].back()["other_names"] = {};

				for (auto& catLabel : categoryLabels)
				{
					j["categories"].back()["other_names"].push_back(catLabel.toStdString());
				}
			}
		}

	}
	else {
		// If the "append to file" button was checked, look in the provided JSON-files for categories
		// Iterate through the existing JSON categories and append them to the map
		auto it = j.find("categories");

		if (it != j.end())
		{
			for (auto& category : j["categories"])
			{
				auto nameIt = category.find("name");
				auto idIt = category.find("id");

				if ((nameIt != category.end()) && (idIt != category.end()))
				{
					objectIdCategoryPairs[QString::fromStdString(category["name"])] = static_cast<int>(category["id"]);
				}

				// Append the rest of the categories, if any	
				auto otherNamesIt = category.find("other_names");

				if (otherNamesIt != category.end())
				{
					for (auto& otherName : category["other_names"])
					{
						objectIdCategoryPairs[QString::fromStdString(otherName)] = static_cast<int>(category["id"]);
					}
				}
			}
		}
		else {
			progressBar->hide();
			progressLabel->setText(tr("The provided annotation file does not contain any categories. Aborting..."));
			return;
		}
	}


	progressLabel->setText(tr("Exporting annotations to:\n%1").arg(fileLocationPath->text()));
	progressLabel->show();
	QApplication::processEvents();

	QSet<QString> fileNames;
	QSet<QString> nonMatchedCategories;


	// Currently, we have hardcoded this to the MSCOCO format
	if (!objectIdCategoryPairs.empty())
	{
		bool firstLine = true;
		QString lastFileName;
		QFile convertedAnnotationFile;
		int lineNumber = 0;
		int imageNumber = 0;
		int annotationNumber = 0;

		if (!createNewFileRadioButton->isChecked())
		{
			// The "append to" button is checked - get the last image number and annotation id from the existing file
			auto imageIt = j.find("images");

			if (imageIt != j.end())
			{
				for (auto& image : j["images"])
				{
					auto imageIdIt = image.find("id");

					if (imageIdIt != image.end())
					{
						int imageId = static_cast<int>(image["id"]);

						if (imageId > imageNumber)
						{
							imageNumber = imageId + 1;
						}
					}
				}
			}

			auto annoationIt = j.find("annotations");

			if (annoationIt != j.end())
			{
				for (auto& annotation : j["annotations"])
				{
					auto annotationIdIt = annotation.find("id");

					if (annotationIdIt != annotation.end())
					{
						int annotationId = static_cast<int>(annotation["id"]);

						if (annotationId > annotationNumber)
						{
							annotationNumber = annotationId + 1;
						}
					}
				}
			}


		}
		else {
			j["images"] = {};
			j["annotations"] = {};
		}


		QDir maskNoDontCareDir(maskNoDontCareDirPath);

		for (auto imageIdx = 0; imageIdx < numImages; ++imageIdx)
		{
			QRegExp extractFilename("([^/]+)\\.[^\\.]+$");
			extractFilename.indexIn(fileLists[Image::RGB][imageIdx]);
			QString maskFile = extractFilename.cap(1);
			maskFile += ".png";			

			// Format the imageName according to the configured file paths
			QString path = QDir::cleanPath(fileLists[Image::RGB][imageIdx]);
			QStringList formattedPath(QFileInfo(path).fileName());
			auto filePosIndex = filePathList.size() - 1;

			// The file name should always be encoded as-is. Now, we must decide which folders
			// should go inside. We start with pos constEnd() - 2 as the last entry in the list 
			// (at pos constEnd() - 1) refers to the file name which we have already added to 
			// our listB

			for (auto it = filePathList.constEnd() - 2; it != filePathList.constBegin(); it--)
			{
				if (!QFileInfo(path).isRoot())
				{
					path = QFileInfo(path).path();

					auto button = *it;
					
					if (button->isChecked())
					{
						formattedPath.push_front(QFileInfo(path).fileName());
					}
				}
			}

			j["images"].push_back({ { "id", imageNumber }, { "width", imageWidth }, { "height", imageHeight },
			{ "file_name", formattedPath.join("/").toStdString() }, { "license", 0 } });

			if (maskFile.length() > 0) {
				cv::Mat maskNoDontCare = cv::imread(maskNoDontCareDir.absoluteFilePath(maskFile).toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
				/*cv::imshow("Mask for image" + maskFile.toStdString(), maskNoDontCare);*/

				QStringList existingAnnotations = annotationList.filter(maskFile);
				for (int i = 0; i < existingAnnotations.size(); i++) {
					PixelAnnotation a = PixelAnnotation::csvToAnnotation(existingAnnotations[i], csvMetaDataColumnIndex, metaDataNames, metaDataTypes);

					if (maskNoDontCare.empty() && a.getCompactPolygon().isEmpty()) {
						// No mask exist
						break;
					}

					// Get the object category ID
					int categoryNumber = -1;

					if (objectIdCategoryPairs.contains(a.getTag()))
					{
						categoryNumber = objectIdCategoryPairs[a.getTag()];
					}
					else if (objectIdCategoryPairs.contains(a.getTag().toLower()))
					{
						categoryNumber = objectIdCategoryPairs[a.getTag().toLower()];
					}
					else
					{
						nonMatchedCategories.insert(a.getTag());
					}

					std::vector<std::vector<cv::Point> > contours;
					cv::Rect cvBbox;

					// Get the object mask
					if (!maskNoDontCare.empty() && a.getCompactPolygon().isEmpty())
					{
						cv::Mat annotationOnly, dontCareOnly;
						cv::Mat(maskNoDontCare.size(), maskNoDontCare.type(), cv::Scalar(150)).copyTo(annotationOnly, maskNoDontCare == a.id());
						/*cv::imshow("Mask for ID:" + QString::number(a.id()).toStdString(), annotationOnly);
						cv::waitKey(0);*/
						a.setMask(annotationOnly, false); // Don't draw don't care zone here

						cvBbox = a.getBoundingBox();

						// Get raw polygon from here 
						PolygonMask polygon(a.getMask(), 10);
						contours = polygon.getContours();
					
						// Optional: Enable run length encoding
					}
					else if (!a.getCompactPolygon().isEmpty())
					{
 						contours = PolygonMask::getContoursFromCompactPolygon(a.getCompactPolygon());
						cvBbox = PolygonMask::getBoundingBox(a.getCompactPolygon());
					}

					// Format contours to fit into COCO format - flatten the inner contours
					std::vector<std::vector<int> > cocoContours;
					float area = 0.;

					for (auto& outerContour : contours)
					{
						std::vector<int> flattenedInnerContour;

						// Calculate area using contours
						area += cv::contourArea(outerContour);

						for (auto& innerContour : outerContour)
						{
							flattenedInnerContour.push_back(innerContour.x);
							flattenedInnerContour.push_back(innerContour.y);
						}

						cocoContours.push_back(flattenedInnerContour);
					}
										

					std::vector<int> bbox = { cvBbox.x, cvBbox.y, cvBbox.width, cvBbox.height };
					int objectId = a.id();

					j["annotations"].push_back({ {"id", annotationNumber},
											   {"image_id", imageNumber},
											   {"category_id", categoryNumber},
											   {"object_id", objectId},
											   {"area", area},
											   {"bbox", bbox},
											   {"iscrowd", 0},
											   {"segmentation", cocoContours} });

					for (int i = 0; i < metaDataNames.size(); ++i)
					{
						auto it = j["annotations"].back().find(metaDataNames[i].toStdString());

						if (it == j["annotations"].back().end())
						{
							// A key containing the meta data tag was not found. 
							// Thus, we can safely write this meta data tag to the 
							// annotation object
							std::string value = a.getMetaData(i).toStdString();
							j["annotations"].back()[metaDataNames[i].toStdString()] = value;
						}
					}

					annotationNumber++;
				}


			}

			imageNumber++;
			progressBar->setValue(imageNumber);
			QApplication::processEvents();
		}
	}

	// Make a "custom" category that contains information on the file path formatting
	j["custom"] = {};

	std::vector<bool> fileNameFormatting;

	for (auto& button : filePathList)
	{
		fileNameFormatting.push_back(button->isChecked());
	}

	j["custom"]["filenameformatting"] = fileNameFormatting;

	progressLabel->setText(tr("Saving file..."));
	QApplication::processEvents();

	std::ofstream out(fileLocationPath->text().toStdString());
	out << std::setw(4) << j << std::endl;
	progressBar->setValue(progressBar->maximum());

	startConversionButton->setEnabled(true);

	if (!nonMatchedCategories.empty())
	{
		QString message;

		if (nonMatchedCategories.size() == 1)
		{
			message.append(tr("The tag \"%1\" was not found in the %2 category list. Thus, annotations contaiting this tag have all been assigned the number -1.").arg(nonMatchedCategories.values().join("")).arg(objectCategoriesComboBox->currentText()));
		}
		else {
			message.append(tr("The tags listed below were not found in the %1 category list. Thus, annotations containing these tags have all been assigned the number -1.\n").arg(objectCategoriesComboBox->currentText()));

			for (auto& category : nonMatchedCategories)
			{
				message.append(QString("\n%1").arg(category));
			}
		}


		QMessageBox::information(this, tr("Tags not found in category list"), message);
	}

	progressBar->hide();
	progressLabel->setText(tr("Finished exporting annotations to:\n%1").arg(fileLocationPath->text()));

}
