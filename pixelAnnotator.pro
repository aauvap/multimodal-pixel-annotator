#-------------------------------------------------
#
# Project created by QtCreator 2012-12-18T08:30:21
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = pixelAnnotator
TEMPLATE = app
CONFIG   += app_bundle\
            c++11

SOURCES +=  main.cpp\
            pixelannotator.cpp \
            boxannotation.cpp \
            pixelannotation.cpp \
            metadatadialog.cpp \
            settingsdialog.cpp \
            batcheditdialog.cpp \
            adjustpicturedialog.cpp \
            polygonmask.cpp

HEADERS  += pixelannotator.h \
            boxannotation.h \
            pixelannotation.h \
            metadatadialog.h \
            settingsdialog.h \
            batcheditdialog.h \
            adjustpicturedialog.h \
            polygonmask.h



FORMS    += pixelannotator.ui \
            metadatadialog.ui \
            settingsdialog.ui \
            batcheditdialog.ui \
            adjustpicturedialog.ui

!win32{
    LIBS += -L/usr/local/lib \
        -lopencv_core \
        -lopencv_imgproc \
        -lopencv_videoio \
        -lopencv_calib3d \
        -lopencv_imgcodecs \
        -lopencv_highgui

    INCLUDEPATH += /usr/local/include
}

win32{
    LIBS += -LC:\Tools\opencv3\build\install\x64\vc14\lib -lopencv_core310 -lopencv_highgui310 -lopencv_imgproc310 -lopencv_imgcodecs310

    INCLUDEPATH += C:\Tools\opencv3\build\install\include
    DEPENDPATH += C:\Tools\opencv3\build\install\include
}

VPATH = common/

include($${VPATH}opencvcapture.pri)
include($${VPATH}opencvviewer.pri)
include($${VPATH}cvutilities.pri)
include($${VPATH}registrator.pri)
include($${VPATH}autocompletedialog.pri)

RESOURCES += \
    ressources.qrc
