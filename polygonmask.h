#ifndef POLYGONMASK_H
#define POLYGONMASK_H

#include <opencv2/opencv.hpp>
#include <QString>
#include <QStringList>

class PolygonMask
{
public:
	PolygonMask(cv::Mat mask, int proximityThreshold);
	PolygonMask(cv::Mat mask, QString compactPolygon, int proximityThreshold);

	void addPoint(cv::Point point);
	void removePoint(cv::Point point);
	void removeCurrentPoint();
	void movePoint(cv::Point point);
	void stopMovePoint();

	QString getCompactPolygon();
	cv::Mat getMask(cv::Scalar maskColor);
	cv::Mat getOutline(cv::Scalar maskColor, int lineThickness);
	void drawPoints(cv::Mat &mask, cv::Scalar maskColor, int circleRadius, int lineThickness);
	void moveCurrentPoint(int vertical, int horizontal);
	void moveEntirePolygon(int vertical, int horizontal);

	void changeCurrentPoint(int direction);

	std::vector<std::vector<cv::Point> > getContours();
	cv::Rect getBoundingBox();
	static cv::Rect getBoundingBox(QString compactPolygon);
	static cv::Rect getBoundingBox(std::vector<std::vector<cv::Point> > polygon);


	static std::vector<std::vector<cv::Point> > getContoursFromCompactPolygon(QString compactPolygon);

private:
	void getClosestContourPoint(cv::Point point, int &bestOuterIndex, int &bestInnerIndex);

	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;

	int currentContourIndex;
	int proximityThreshold;

	int currentInnerIndex;

	cv::Mat inputMask;

	bool moveOperationStarted;
	int moveOuterIndex;
	int moveInnerIndex;
};

#endif // ! POLYGONMASK_H

