#include "adjustpicturedialog.h"
#include "ui_adjustpicturedialog.h"

AdjustPictureDialog::AdjustPictureDialog(QWidget *parent, cv::Mat thermalImg) :
    QDialog(parent),
    ui(new Ui::AdjustPictureDialog)
{
    ui->setupUi(this);

    ui->openCVView->setImage(thermalImg);
    this->thermalImg = thermalImg;

    ui->contrastSpinBox->setValue(settings.value("thermalContrast", 1).toDouble());
    ui->brightnessSpinBox->setValue(settings.value("thermalBrightness", 0).toDouble());
}

AdjustPictureDialog::~AdjustPictureDialog()
{
    delete ui;
}

void AdjustPictureDialog::accept()
{
    settings.setValue("thermalContrast", ui->contrastSpinBox->value());
    settings.setValue("thermalBrightness", ui->brightnessSpinBox->value());

    done(QDialog::Accepted);
}

void AdjustPictureDialog::on_contrastSpinBox_valueChanged()
{
    double alpha = ui->contrastSpinBox->value();
    int sliderValue = alpha * 20 + 10;
    ui->contrastSlider->setValue(sliderValue);
}

void AdjustPictureDialog::on_brightnessSpinBox_valueChanged()
{
    ui->brightnessSlider->setValue(ui->brightnessSpinBox->value());
}

void AdjustPictureDialog::on_brightnessSlider_valueChanged()
{
    transformThermalImg();
}

void AdjustPictureDialog::on_contrastSlider_valueChanged()
{
    transformThermalImg();
}

void AdjustPictureDialog::transformThermalImg()
{
    cv::Mat image = thermalImg.clone();

    double alpha = (static_cast<double>(ui->contrastSlider->value()) - 10.) / 20.;

    ui->contrastSpinBox->blockSignals(true);
    ui->contrastSpinBox->setValue(alpha);
    ui->contrastSpinBox->blockSignals(false);

    ui->brightnessSpinBox->blockSignals(true);
    int beta = ui->brightnessSlider->value();
    ui->brightnessSpinBox->setValue(beta);
    ui->brightnessSpinBox->blockSignals(false);


    for (auto y = 0; y < image.rows; ++y) {
        for (auto x = 0; x < image.cols; ++x) {
            int newVal = cv::saturate_cast<uchar>(alpha*(thermalImg.at<cv::Vec3b>(y, x)[0]) + beta);
            image.at<cv::Vec3b>(y, x)[0] = newVal;
            image.at<cv::Vec3b>(y, x)[1] = newVal;
            image.at<cv::Vec3b>(y, x)[2] = newVal;
        }
    }

    ui->openCVView->setImage(image);
}