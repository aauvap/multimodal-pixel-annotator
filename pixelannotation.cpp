#include "pixelannotation.h"

PixelAnnotation::PixelAnnotation(int id, const QString & tag, const QStringList & metaDataNames, const QStringList & metaDataTypes, const QString& compactPolygon)
{
	assert(metaDataNames.size() == metaDataTypes.size());

	this->compactPolygon = compactPolygon;
	this->theId = id;
	this->theTag = tag;

	for (int i = 0; i < (int)metaDataNames.size(); i++) {
		if (metaDataTypes[i] == "bool") {
			metaDataValues.push_back("false");
		}
		else {
			metaDataValues.push_back(QString());
		}
	}
	this->metaDataNames = metaDataNames;
	this->metaDataTypes = metaDataTypes;
}

int PixelAnnotation::id() const
{
    return theId;
}

void PixelAnnotation::id(int id)
{
    theId = id;
}

cv::Mat PixelAnnotation::getMask()
{
    // Return the combined mask and don't care mask
    cv::Mat returnMask = maskNoDontCare.clone();

    if (!maskOnlyDontCare.empty()) {
        returnMask = returnMask + maskNoDontCare;
    }

    return returnMask;
}

cv::Mat PixelAnnotation::getMaskNoDontCare()
{
    return maskNoDontCare;
}

cv::Mat PixelAnnotation::getMaskOnlyDontCare()
{
    return maskOnlyDontCare;
}

void PixelAnnotation::setCompactPolygon(const QString & compactPolygon)
{
	this->compactPolygon = compactPolygon;
}

QString PixelAnnotation::getCompactPolygon() const
{
	return this->compactPolygon;
}

void PixelAnnotation::setMask(cv::Mat mask, bool addDontCare, int borderThickness, int borderColor)
{
    maskNoDontCare = mask.clone();
	findUpperLeftCorner(mask);

    // Update the don't care mask if needed
    if (addDontCare) {
        cv::Mat input = (mask.clone() == cv::GC_FGD) + (mask.clone() == cv::GC_PR_FGD);
        maskOnlyDontCare = cv::Mat::zeros(mask.size(), mask.type());
		
        generateDontCareFromMask(input, maskOnlyDontCare, borderThickness, borderColor);
    }
}

void PixelAnnotation::generateDontCareFromMask(cv::Mat mask, cv::Mat &onlyDontCare, 
	int borderThickness, int borderColor)
{
    cv::Mat input = mask.clone();
    if (countNonZero(input) == 0) {
        return;
    }

    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    onlyDontCare = cv::Mat::zeros(mask.size(), mask.type());

    cv::findContours(input, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    
    if (!hierarchy.empty()) {
        for (int i = 0; i >= 0; i = hierarchy[i][0]) {
            drawContours(onlyDontCare, contours, i, cv::Scalar(borderColor), borderThickness, 8, hierarchy);
        }
    }
}


void PixelAnnotation::setDontCareMask(cv::Mat dontCareMask)
{
    maskOnlyDontCare = dontCareMask.clone();
}

int PixelAnnotation::getMetaDataSize() const
{
    return metaDataValues.size();
}

QStringList PixelAnnotation::getMetaData() const
{
    return metaDataValues;
}

QString PixelAnnotation::getMetaData(int metaDataId) const
{
    return metaDataValues[metaDataId];
}

void PixelAnnotation::setMetaData(int metaDataId, QString value)
{
    assert(metaDataTypes[metaDataId] == "string" || (metaDataTypes[metaDataId] == "bool" && (value == "false" || value == "true")));

    metaDataValues[metaDataId] = value;
}

void PixelAnnotation::setMetaData(int metaDataId, bool value)
{
    assert(metaDataTypes[metaDataId] == "bool");
    if(value) {
        metaDataValues[metaDataId] = "True";
    } else {
        metaDataValues[metaDataId] = "False";
    }
}

void PixelAnnotation::setMetaData(QStringList values)
{
    assert(values.size() == metaDataValues.size());
    metaDataValues = values;
}

void PixelAnnotation::toggleMetaData(int metaDataId)
{
    if(metaDataTypes[metaDataId] == "bool") {
        if(metaDataValues[metaDataId].toLower() != "true") {
            metaDataValues[metaDataId] = "True";
        } else {
            metaDataValues[metaDataId] = "False";
        }
    }
}

PixelAnnotation PixelAnnotation::csvToAnnotation(const QString & csv, 
												 int csvMetaDataColumnIndex,
												 const QStringList& metaDataNames,
												 const QStringList& metaDataTypes)
{
	QStringList fields = csv.split(";");
	int annotationIdStartIndex = fields.indexOf(QRegExp("^\\d\\d\\d$"));
	int annotationIdEndIndex = fields.lastIndexOf(QRegExp("^\\d\\d\\d$"));

	if (annotationIdStartIndex >= 0 && annotationIdEndIndex >= 0) {
		QString tag;

		if (fields.size() > csvMetaDataColumnIndex - 1)
		{
			tag = fields[csvMetaDataColumnIndex - 1];
		}

		PixelAnnotation annotation(fields[annotationIdStartIndex].toInt(), tag, metaDataNames, metaDataTypes);

		QStringList metaDataList;

		if (fields.size() > csvMetaDataColumnIndex)
		{
			metaDataList = fields[csvMetaDataColumnIndex].split(',');
		}

		if (metaDataList.count() < metaDataNames.count()) {
			// The meta data list is not the same size as the template.
			// Add empty entries so it fits

			while (metaDataList.count() < metaDataNames.count()) {
				metaDataList.append("");
			}
		}
		else if (metaDataList.count() > metaDataNames.count()) {
			// The list is too big. Discard the last elements

			while (metaDataList.count() > metaDataNames.count()) {
				metaDataList.removeLast();
			}
		}

		annotation.setMetaData(metaDataList);


		if (fields.size() > csvMetaDataColumnIndex + 1)
		{
			bool isFinished = fields[csvMetaDataColumnIndex + 1].toInt() > 0 ? true : false;
			annotation.setIsFinished(isFinished);
		}

		if (fields.size() > csvMetaDataColumnIndex + 2)
		{
			QString compactPolygon = fields[csvMetaDataColumnIndex + 2];
			qDebug() << "Annotation ID: " << annotation.id() << " - Polygon from CSV: " << compactPolygon;
			annotation.setCompactPolygon(compactPolygon);
		}

		return annotation;
	}
	else {
		// Return empty annotation
		PixelAnnotation annotation(0);
		return annotation;
	}
}

QString PixelAnnotation::annotationToCsv(const PixelAnnotation & annotation, const QStringList& startList)
{
	QStringList line = startList;

	line << QString("%1").arg(annotation.id());
	line << annotation.getTag();

	QString combinedLine = line.join(';');
	combinedLine.append(';' + annotation.getMetaData().join(","));
	combinedLine.append(';');
	QString isFinished = annotation.isFinished() ? "1" : "0";
	combinedLine.append(isFinished);
	combinedLine.append(';');
	combinedLine.append(annotation.getCompactPolygon());
	qDebug() << "Annotation id: " << annotation.id() << " - Polygon to CSV: " << annotation.getCompactPolygon();


	return combinedLine;
	
}

QStringList PixelAnnotation::getMetaDataNames() const
{
    return metaDataNames;
}

QString PixelAnnotation::getMetaDataName(int metaDataId) const
{
    return metaDataNames[metaDataId];
}

QString PixelAnnotation::getMetaDataType(int metaDataId) const
{
    return metaDataTypes[metaDataId];
}

void PixelAnnotation::setTag(const QString & tag)
{
	theTag = tag;
}

cv::Rect PixelAnnotation::getBoundingBox()
{
	cv::Mat binaryImg;

	if (maskNoDontCare.empty())
	{
		return cv::Rect();
	}

	threshold(maskNoDontCare, binaryImg, 0, 255, cv::THRESH_BINARY);
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;

	cv::findContours(binaryImg, contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
	
	cv::Rect boundRect;
	cv::Point2i topLeft = cv::Point2i(binaryImg.cols, binaryImg.rows); // Default-place this in the lower right corner, the exact opposite of top-left
	cv::Point2i bottomRight = cv::Point2i(0, 0); // And the exact opposite

	for (auto& contour : contours)
	{
		cv::Rect tmpRect = cv::boundingRect(contour);

		if (tmpRect.tl().x < topLeft.x)
		{
			topLeft.x = tmpRect.x;
		}

		if (tmpRect.tl().y < topLeft.y)
		{
			topLeft.y = tmpRect.y;
		}

		if (tmpRect.br().x > bottomRight.x)
		{
			bottomRight.x = tmpRect.br().x;
		}

		if (tmpRect.br().y > bottomRight.y)
		{
			bottomRight.y = tmpRect.br().y;
		}
	}

	if (topLeft.x != binaryImg.cols)
	{
		boundRect = cv::Rect(topLeft, bottomRight);
	}

	if (boundRect.area() != 0)
	{
		qDebug() << "Bounding rect for ID " << QString::number(theId) << " at (x, y, w, h)" << boundRect.x << ", " << boundRect.y << ", " << boundRect.width << ", " << boundRect.height;

		return boundRect;
	}
	else {
		qDebug() << "No countour found with ID " << QString::number(theId);

		return cv::Rect();
	}
}

void PixelAnnotation::findUpperLeftCorner(cv::Mat mask)
{
	cv::Mat input = mask.clone();

	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;

	cv::findContours(input, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	upperLeftCorner = cv::Point(input.cols, input.rows);

	// Update the upper-left corner
	for (auto& contour : contours)
	{
		for (auto& point : contour)
		{
			if (point.x < upperLeftCorner.x)
			{
				upperLeftCorner.x = point.x;
			}

			if (point.y < upperLeftCorner.y)
			{
				upperLeftCorner.y = point.y;
			}
		}
	}

	// Make some additional space for the text in the y direction
	if (upperLeftCorner.y > 5)
	{
		upperLeftCorner.y -= 5;
	}
	else {
		upperLeftCorner.y = 1;
	}

}
