#include "metadatadialog.h"
#include "ui_metadatadialog.h"

MetaDataDialog::MetaDataDialog(QWidget *parent, QStringList &wordList, QStringList &metaDataTypes,
	const QString &title, const QString &label) :
	QDialog(parent),
	ui(new Ui::MetaDataDialog)
{
	ui->setupUi(this);
	ui->label->setText(label);
	setWindowTitle(title);

	for (int i = 0; i < (int)wordList.size(); i++) {
		ui->metaDataNames->appendPlainText(wordList[i]);
	}

}

MetaDataDialog::~MetaDataDialog()
{
    delete ui;
}

void MetaDataDialog::accept()
{
	metaDataNames = ui->metaDataNames->toPlainText().split('\n', QString::SkipEmptyParts);
	for(int i = 0; i < (int)metaDataNames.size(); i++) {
		metaDataNames[i] = metaDataNames[i].trimmed();
	}

	done(QDialog::Accepted);
}

QStringList MetaDataDialog::getNames()
{
	return metaDataNames;
}
