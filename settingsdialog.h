#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QtWidgets/QDialog>
#include <QSettings>
#include <QRegExp>
#include <QtWidgets/QColorDialog>

#include <opencv2/core.hpp>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SettingsDialog(QWidget *parent, cv::Mat thermalImg);
    ~SettingsDialog();
    
    void accept();
private slots:
    void on_btnColor_clicked();
    void on_btnDontCareColor_clicked();
    void on_adjustThermalImagePushButton_clicked();

private:
    Ui::SettingsDialog *ui;
    QSettings settings;
    QColor overlayColor, dontCareOverlayColor;

    cv::Mat thermalImg;

};

#endif // SETTINGSDIALOG_H
