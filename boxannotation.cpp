#include "boxannotation.h"

using namespace cv;
using namespace std;

BoxAnnotation::BoxAnnotation(QStringList metaDataNames)
{
	aSet = bSet = false;

	initMetaData(metaDataNames);
}

BoxAnnotation::BoxAnnotation(std::vector<cv::Point> corners, QStringList metaDataNames, QString tag)
{
	assert(corners.size() == 2);
	initPoints(corners[0], corners[1]);
	initTag(tag);
	initMetaData(metaDataNames);
}

BoxAnnotation::BoxAnnotation(cv::Point a, cv::Point b, QStringList metaDataNames, QString tag)
{
	initPoints(a, b);
	initTag(tag);
	initMetaData(metaDataNames);
}

void BoxAnnotation::initPoints(cv::Point a, cv::Point b)
{
	pointA = a;
	pointB = b;

	aSet = bSet = true;
	realign();
}

void BoxAnnotation::initMetaData(QStringList metaDataNames)
{
	for(int i = 0; i < (int)metaDataNames.size(); i++) {
		metaDataValues.push_back(false);
	}
	this->metaDataNames = metaDataNames;
}

void BoxAnnotation::initTag(QString tag)
{
	if(tag.length() > 0) {
		this->theTag = tag;
	}
}

cv::Point BoxAnnotation::a()
{
	return pointA;
}

cv::Point BoxAnnotation::b()
{
	return pointB;
}

QString BoxAnnotation::aAsString()
{
	return QString("%1,%2").arg(pointA.x).arg(pointA.y);
}

QString BoxAnnotation::bAsString()
{
	return QString("%1,%2").arg(pointB.x).arg(pointB.y);
}

cv::Rect BoxAnnotation::box()
{
	return theBox;
}

void BoxAnnotation::setA(cv::Point position)
{
	pointA = position;
	aSet = true;
	realign();
}

void BoxAnnotation::setB(cv::Point position)
{
	pointB = position;
	bSet = true;
	realign();
}

bool BoxAnnotation::isFull()
{
	return (aSet && bSet);
}

void BoxAnnotation::realign()
{
    if(!isFull()) {
        return;
    }
    Point corner;
    corner.x = min(pointA.x, pointB.x);
    corner.y = min(pointA.y, pointB.y);
    /*pointB.x = max(pointA.x, pointB.x);
    pointB.y = max(pointA.y, pointB.y);
    pointA = corner;*/

    /*theBox.x = pointA.x;
    theBox.y = pointA.y;
    theBox.width = pointB.x-pointA.x;
    theBox.height = pointB.y-pointA.y;*/

    theBox.x = corner.x;
    theBox.y = corner.y;
    theBox.width = max(pointA.x, pointB.x)-corner.x;
    theBox.height = max(pointA.y, pointB.y)-corner.y;
}

void BoxAnnotation::moveUp(int distance)
{
	if(!isFull()) {
		return;
	}
	pointA.y -= distance;
	pointB.y -= distance;
    realign();
}

void BoxAnnotation::moveDown(int distance)
{
	if(!isFull()) {
		return;
	}
	pointA.y += distance;
	pointB.y += distance;
    realign();
}

void BoxAnnotation::moveLeft(int distance)
{
	if(!isFull()) {
		return;
	}
	pointA.x -= distance;
	pointB.x -= distance;
    realign();
}

void BoxAnnotation::moveRight(int distance)
{
	if(!isFull()) {
		return;
	}
	pointA.x += distance;
	pointB.x += distance;
    realign();
}

void BoxAnnotation::shrinkVertical(int distance)
{
	if(!isFull()) {
		return;
	}
	if(pointB.y-distance > pointA.y) {
		pointB.y -= distance;
	}
    realign();
}

void BoxAnnotation::shrinkHorizontal(int distance)
{
	if(!isFull()) {
		return;
	}
	if(pointB.x-distance > pointA.x) {
		pointB.x -= distance;
	}
    realign();
}

void BoxAnnotation::growVertical(int distance)
{
	if(!isFull()) {
		return;
	}
	pointB.y += distance;
    realign();
}

void BoxAnnotation::growHorizontal(int distance)
{
	if(!isFull()) {
		return;
	}
	pointB.x += distance;
    realign();
}

QString BoxAnnotation::tag()
{
	return theTag;
}

void BoxAnnotation::setTag(QString tag)
{
	this->theTag = tag;
}

bool BoxAnnotation::getMetaData(int metaDataId)
{
	assert(metaDataId < (int)metaDataValues.size());
	return metaDataValues[metaDataId];
}

void BoxAnnotation::setMetaData(int metaDataId, bool value)
{
	assert(metaDataId < (int)metaDataValues.size());
	metaDataValues[metaDataId] = value;
}

QString BoxAnnotation::getMetaDataName(int metaDataId)
{
	assert(metaDataId < (int)metaDataValues.size());
	return metaDataNames[metaDataId];
}

int BoxAnnotation::getMetaDataSize()
{
	return metaDataValues.size();
}
