#include "batcheditdialog.h"
#include "ui_batcheditdialog.h"

BatchEditDialog::BatchEditDialog(QList<PixelAnnotation> annotations, QStringList baseDataNames, QStringList metaDataNames, QStringList metaDataTypes, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BatchEditDialog)
{
    ui->setupUi(this);
    this->annotations = annotations;
    this->baseDataNames = baseDataNames;
    this->metaDataNames = metaDataNames;
    this->metaDataTypes = metaDataTypes;

    if (this->annotations.isEmpty())
    {
        QStringList k("<no annotations>");
        PixelAnnotation empAnn(0, QString(), k, k);

        annotations.append(empAnn);
    }

    ui->annotationTable->setRowCount(annotations.size());

    ui->annotationTable->setColumnCount(annotations[0].getMetaDataNames().size());
    ui->annotationTable->setHorizontalHeaderLabels(annotations[0].getMetaDataNames());
    ui->annotationTable->resizeColumnsToContents();

}

BatchEditDialog::~BatchEditDialog()
{
    delete ui;
}

void BatchEditDialog::on_btnClose_clicked()
{
    done(QDialog::Rejected);
}

void BatchEditDialog::on_btnPerformChanges_clicked()
{
    done(QDialog::Accepted);
}
