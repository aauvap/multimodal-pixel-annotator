/**
 * \mainpage
 * Pixel annotator is a program to annotate large amounts of multi-modal video data with pixel-accuracy.
 *
 * A userguide can be found at XX, as these docs are mostly interesting to developers.
 *
 * Now, if you are really a developer, this is a very good place to start. Before you start make sure you have the necessary dependencies in places:
 * \li Qt 5.6+
 * \li OpenCV 3.1+
 * \li cvUtilities (should come with this source code in the common/ directory)
 *
 * The best place to dive into the code is by looking at the PixelAnnotator class. That is the main class of the project, and it handles all UI events.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMdiSubWindow>
#include <QtWidgets/QActionGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QFileDialog>
#include <QInputDialog>
#include <QProgressDialog>

#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "common/openCVViewer/opencvviewer.h"
#include "common/cvUtilities/cvutilities.h"
#include "common/registrator/registrator.h"
#include "common/Qt-Color-Widgets/hue_slider.hpp"
#include "common/autoCompleteDialog/autocompletedialog.h"
#include "annotationExporter.h"
#include "boxannotation.h"
#include "pixelannotation.h"
#include "metadatadialog.h"
#include "settingsdialog.h"
#include "batcheditdialog.h"
#include "polygonmask.h"

namespace Ui {
class MainWindow;
}

/**
  Used to keep track of the function-state.

  Functions are defined as something that happens immediately when a button is pressed, as opposed to tools, which are selectable and can hold their state.

  \enum Function::e
  */
struct Function {
    /** Placeholder name - in reality the struct (Function) is used directly */
    enum e {
        newAnnotation, ///< Create a new, empty annotation.
        save, ///< Save all annotations in this image.
        undoAll, ///< Undo all changes to this frame.
        deleteAnnotations, ///< Delete the selected annotation(s).
        autoBorder, ///< Add a don't-care border around the selected annotation(s).
        moveMaskUp, // Move mask in the negative y-direction
        moveMaskDown, // Move mask in the positive y-direction
        moveMaskLeft, // Move mask in the negative x-direction
        moveMaskRight, // Move mask in the positive x-direction
        removeNoise, ///< Remove small specks from mask.
        fillHoles, ///< Auto-fill holes.
        zoomIn, ///< Zoom all views in.
        zoomInExtra, ///< Zoom all views in (extra shortcut to enable numpad +).
        zoomOut, ///< Zoom all views out.
        zoomOutExtra, ///< Zoom all views out (extra shortcut to enable numpad -).
        zoomFit, ///< Fit image in all windows.
        zoomActual, ///< Zoom to actual pixel size in all windows.
		zoomCenter, ///> Center the image on the current annotation(s)
        previousImage, ///< Go to previous image.
        nextImage, ///< Go to next image.
        jumpToImage, ///< Go to a specific image, given in an input box in the interface.
		retainPreviousAnnotation, ///< Retain annotations when opening previous frame
		retainNextAnnotation, ///< Retain annotations when opening next frame
        repairRegistration,
		showDontCareMask, ///< Show don't care mask
		displayTag, ///< Display annotation tag on top of bounding box
		showBorder, ///< Show don't care border
    }; };

/**
  Used to keep track of tool-state.

  Tools are selectable, so this is part of the program's state, as opposed to functions, which describe something that happens instantly. This is basically the different tools the user can select in the program.
  */
struct Tool { enum e {none, selector, grabCut, addToGrabCut, subtractFromGrabCut, addToMask, subtractFromMask, addToPolygon, subtractFromPolygon, movePointInPolygon}; };

/**
  Keeps track of under-the-hood state.

  Holds whether a drawing or box-drawing is in progress.
  */
struct State { enum e {none, boxInProgress, drawingInProgress, busy}; };

/**
  Identifiers for the four images in the workspace.

  The program handles 3 modalities and a mask. These identifiers are used as indexes in the data structures which hold the images, imageviewers, etc.
  */
//struct Image { enum e {RGB, Depth, Thermal, MaskNoDontCare, MaskOnlyDontCare}; }; % Declared in annotationExporter.h

/**
  Defines the color of the don't-care border.
  */
struct Color { enum e {unknown = 170}; };

/**
 * The main class of the program, start here.
 *
 * Defines the general flow of the program and the handling of UI events.
 * The program contains a working area with 4 windows (see Image), 3 for each modality and one for the mask. On the top is a toolbar with a combination of functions (Function) and tools (Tool). To the right is a dock with a list of annotations and a table with properties of the current annotation (see updateCurrentAnnotationsPane()).
 */
class PixelAnnotator : public QMainWindow
{
    Q_OBJECT
    
public:
    /**
      Creates the program and initializes all variables and tools.

      This function has 5 main functions:
      \li Load settings saved earlier (see also SettingsDialog and on_action_Settings_triggered())
      \li Load meta data settings
      \li Load assets (icons, basically)
      \li Build the user interface contained by the main MDI area. This means creating the relevant subwindows, putting scroll areas and image viewers inside them, and connecting mouse events in the windows to their respective handlers.
      \li Build the toolbar, including setting up shortcuts.
      \callgraph
      */
    explicit PixelAnnotator(QWidget *parent = 0);
    ~PixelAnnotator();
    
private slots:
	void on_actionExport_annotations_triggered();

    bool eventFilter(QObject *object, QEvent *event);
    void keyPressEvent(QKeyEvent *e); ///< Handles the few keypresses which are not implemented as action-shortcuts (see PixelAnnotator()). Mainly using Ctrl to switch between add and subtract actions.
    void keyReleaseEvent(QKeyEvent *e); ///< Switches between add and subtract when Ctrl is released.
    void imageMouseHandler(int image, QMouseEvent* event);
    void RGBMouseHandler(QMouseEvent* event);
    void DepthMouseHandler(QMouseEvent* event);
    void ThermalMouseHandler(QMouseEvent* event);
    void MaskMouseHandler(QMouseEvent* event);
    void scrollHandler(QWheelEvent* event);
    void setBrushSize(QString text);
    void drawAndUpdate();
	void enableAddToPolygon();
	void enableSubtractFromPolygon();
	void enableMovePointInPolygon();

	void on_actionReset_layout_triggered();

	bool changeTag(int annotationIndex);
    void newAnnotation();
    void saveAnnotations();
    void undoAll();
    void deleteSelectedAnnotations();
    void deleteAnnotations(QList<int> deleteIds);
    void deleteById(int id);
    void autoBorder();
    void removeNoise();
    void fillHoles();
    void enableSelector();
    void enableGrabCut();
    void enableAddToGrabCut();
    void enableSubtractFromGrabCut();
    void enableAddToMask();
    void enableSubtractFromMask();
    void zoomIn();
    void zoomOut();
    void zoomFit();
    void zoomActual();
    void moveMaskUp();
    void moveMaskDown();
    void moveMaskLeft();
    void moveMaskRight();

    void nextImage();
    void previousImage();
    void jumpToImage();

    void setMetaDataOptions(QStringList lines);
    void updateCurrentAnnotationsPane();
	void updateCurrentAnnotationsPropertiesPane();

    void placeholderSlot();

    void repairRegistration();

	void on_actionExport_to_bounding_box_annotations_triggered();
	void on_actionEdit_suggested_tags_triggered();

    void on_actionTile_triggered();
    void on_action_Quit_triggered();
    void on_action_Edit_meta_data_fields_triggered();
    void on_annotationProperties_cellDoubleClicked(int row, int column);
    void on_annotationProperties_cellChanged(int row, int column);
    void on_annotationList_itemSelectionChanged();
    void on_action_About_triggered();
    void on_actionReport_bug_triggered();
    void on_action_Settings_triggered();
    void on_action_Open_folder_triggered();

    void on_actionLoad_configuration_triggered();
    void on_actionSave_configuration_triggered();
    void on_overlayColorSlider_valueChanged();

	void centerCurrentAnnotation();

private:
	
	void addPolygonMask();
	void toggleIsFinishedState();
    void switchAddSubtract();
    void zoom(int direction = 1, float magnitude = 0.25);
    void loadImage(int index, bool saveChanges = true, bool showAnnotations = true);
    void updateControlButtons();
    
	QString annotationToCsv(int frameNumber, const PixelAnnotation &annotation);

    QString maskFilename(int index, int modality);
    void drawMask(QList<PixelAnnotation> annotations, cv::Mat& combinedMask, cv::Mat& maskNoDontCare, cv::Mat& maskOnlyDontCare);
    void saveCSVFile();
    bool idInList(int id, QList<PixelAnnotation> list);
    void setCurrentAnnotations(QSet<int> annotations, bool save = true);
    bool getNewId(int& newId);
	QStringList getExistingAnnotations(QString maskDir, QString imageFileName, int id);
    QString getModalityName(int modality);
    int getModalityIndex(int modality);
    void initModalityWindows();
    void clearWindows();
    void moveMask(int xShift, int yShift, bool moveSinglePoint = false);
	void insertAnnotationInList(int frameNumber, const PixelAnnotation &annotation);
	void findFutureAnnotationsWithSameId(std::map<int, int>& samedIdTagAnnotationsTable, int annotationId, int futureRange);

	void readSettings(QString prefix);
	void writeSettings(QString prefix);

    void openConfiguration(QString dir = "", QString calibPath = "", QString maskPath = "");

    Ui::MainWindow *ui;
    QList<OpenCVViewer*> viewers;
    QList<QMdiSubWindow*> workWindows;
    QList<QScrollArea*> scrollAreas;

    QList<cv::Mat> images;
    QList<cv::Mat> displayImages;
    QList<cv::Mat> dontCareMasks;

    Registrator registrator;
    Registrator oldRegistrator;
    QHash<int, QAction*> functionActions;
    QHash<int, QAction*> toolActions;
    QActionGroup* toolGroup;
    QComboBox* brushSizeSelector;
    QSpinBox* imageSelector;
    QLabel* fileNumber;
    //QLabel* progressIndicator;
    BoxAnnotation currentBox; // Used for the GrabCut rectangle while selection is in progress.
    QSet<int> currentAnnotations; // Contains IDs for the currently selected annotations.
    int currentImage;
    int numImages;
    int previousSelection;
    QList<PixelAnnotation> annotations; // Contains the annotations for the current image.
    QHash<int, cv::Mat> externalMasks;

    QFile outFile;
    QStringList outFileLines;
	QStringList replaceInTrack, noReplace;


    State::e state;
    Tool::e tool;
    bool overlayMaskEnabled;
    int brushSize;
    bool changesSaved;
    cv::Point previousPaintPoint, scrollStart;

    QCursor* addCursor;
    QCursor* subtractCursor;
    QCursor* addGrabCutCursor;
    QCursor* subtractGrabCutCursor;
    QCursor* handCursor;
	QCursor* addPolygonCursor;
	QCursor* subtractPolygonCursor;
	QCursor* movePolygonCursor;

    // Settings
    QStringList baseDataNames;
    QStringList metaDataNames;
    QStringList metaDataTypes;
	QStringList suggestedTags;

    bool saveAllModalities;
    int borderThickness;
    bool autoBorderWhenDrawing, autoBorderWhenSwitching;
    QHash<int, QString> filePatterns;
    QString csvPath;
    QHash<int, QString> maskFolders;
    QColor overlayColor;
    float overlayOpacity;
    int noiseBlobSize;
    int holeFillSize;
    int csvMetaDataColumnIndex;
	int csvTagColumnIndex;

	std::shared_ptr<PolygonMask> polygonMask;

    QString maskPath, calibPath;

    QHash<int, bool> enabledModalities;

    QHash<int, QStringList> fileLists;
    QDir baseDir;
};

class KeyPressEater : public QObject
{
    Q_OBJECT

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};


#endif // MAINWINDOW_H
