#ifndef BOXANNOTATION_H
#define BOXANNOTATION_H

#include <vector>
#include <opencv2/core/core.hpp>
#include <QString>
#include <QStringList>

class BoxAnnotation
{
public:
	BoxAnnotation(QStringList metaDataNames = QStringList());
	BoxAnnotation(std::vector<cv::Point> corners, QStringList metaDataNames = QStringList(), QString tag = QString());
	BoxAnnotation(cv::Point a, cv::Point b, QStringList metaDataNames = QStringList(), QString tag = QString());

	cv::Point a();
	cv::Point b();
	QString aAsString();
	QString bAsString();
	cv::Rect box();
	void setA(cv::Point position);
	void setB(cv::Point position);
	bool isFull();
	void moveUp(int distance);
	void moveDown(int distance);
	void moveLeft(int distance);
	void moveRight(int distance);
	void shrinkVertical(int distance);
	void shrinkHorizontal(int distance);
	void growVertical(int distance);
	void growHorizontal(int distance);
	QString tag();
	void setTag(QString tag);
	bool getMetaData(int metaDataId);
	void setMetaData(int metaDataId, bool value);
	QString getMetaDataName(int metaDataId);
	int getMetaDataSize();

protected:
	void initPoints(cv::Point a, cv::Point b);
	void initMetaData(QStringList metaDataNames);
	void initTag(QString tag);
	void realign();

	cv::Point pointA, pointB;
	cv::Rect theBox;
	QString theTag;
	bool aSet, bSet;
	bool aligned;
	QStringList metaDataNames;
	std::vector<bool> metaDataValues;
};

#endif // BOXANNOTATION_H
